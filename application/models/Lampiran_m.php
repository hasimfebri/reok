<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lampiran_m extends CI_Model{
	function lampimport($post) {
		$id = $post['periode'];
		$this->db->select('month(bulan) as angka, monthname(bulan) as nama,count(nama_pegawai) as pgw,sum(iuran_1) as satu,sum(iuran_4) as empat');
		$this->db->from("import_satker");
		$this->db->where("periode_id_periode",$id);
		$this->db->group_by("bulan"); 
		$data = $this->db->get();
		return $data;
	}
	function bayarsatu($post) {
		$id = $post['periode'];
		$this->db->select('month(bulan) as angka, monthname(bulan) as nama,sum(nominal) as nom');
		$this->db->from("satup");
		$this->db->where("periode_id_periode",$id);
		$this->db->group_by("bulan"); 
		$data = $this->db->get();
		return $data;
	}
	function bayarempat($post) {
		$id = $post['periode'];
		$this->db->select('month(bulan) as angka, monthname(bulan) as nama,sum(nominal) as nom');
		$this->db->from("empatp");
		$this->db->where("periode_id_periode",$id);
		$this->db->group_by("bulan"); 
		$data = $this->db->get();
		return $data;
	}
	function lampimportbpjs($post) {
		$id = $post['periode'];
		$this->db->select('month(bulan) as angka, monthname(bulan) as nama,count(nama_pegawai) as pgw,sum(iuran_1) as satu,sum(iuran_4) as empat');
		$this->db->from("import_bpjs");
		$this->db->where("periode_id_periode",$id);
		$this->db->group_by("bulan"); 
		$data = $this->db->get();
		return $data;
	}
	function fikssatu($post) {
		$id = $post['periode'];
		$this->db->select('month(bulan) as angka, monthname(bulan) as nama,sum(nominal) as nom');
		$this->db->from("fikssatu");
		$this->db->where("periode_id_periode",$id);
		$this->db->group_by("bulan"); 
		$data = $this->db->get();
		return $data;
	}
	function fiksempat($post) {
		$id = $post['periode'];
		$this->db->select('month(bulan) as angka, monthname(bulan) as nama,sum(nominal) as nom');
		$this->db->from("fiksempat");
		$this->db->where("periode_id_periode",$id);
		$this->db->group_by("bulan"); 
		$data = $this->db->get();
		return $data;
	}
	function totaltagihan($post){
		$id = $post['periode'];
		$this->db->select('month(bulan) as angka, sum(iuran_1) as satu,sum(iuran_4) as empat');
		$this->db->from("import_bpjs");
		$this->db->where("periode_id_periode",$id);
		$this->db->group_by("periode_id_periode"); 
		$data = $this->db->get();
		return $data;
	}
	function totalsatu($post){
		$id = $post['periode'];
		$this->db->select('month(bulan) as angka, sum(nominal) as satu');
		$this->db->from("fikssatu");
		$this->db->where("periode_id_periode",$id);
		$this->db->group_by("periode_id_periode"); 
		$data = $this->db->get();
		return $data;
	}	
	function totalempat($post){
		$id = $post['periode'];
		$this->db->select('month(bulan) as angka, sum(nominal) as empat');
		$this->db->from("fiksempat");
		$this->db->where("periode_id_periode",$id);
		$this->db->group_by("periode_id_periode"); 
		$data = $this->db->get();
		return $data;
	}
	function getIuaran($id){
	    $this->db->from('iuran_month');
	    $this->db->where('periode_id_periode',$id);
	    return $this->db->get();
	    
	}
	function getAlldata($id){
		$sql = "SELECT *,actual_iuran_1 - iuran_1 as kurang1, actual_iuran_4 - iuran_4 as kurang4, date_format(bulan,'%m') as bulan FROM `iuran_month` WHERE periode_id_periode = '".$id."' order by date_format(bulan,'%m')";
		return $this->db->query($sql);
	}
}
