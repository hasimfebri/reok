<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sk_m extends CI_Model{
	public function get(){
		$idperode = $this->session->idperiod;
		$this->db->select('*');
		$this->db->from('sk');
		$this->db->join('periode','periode_id_periode = id_periode');
		$this->db->join('satker','satker_username = username');
		$this->db->where('periode_id_periode',$idperode );
		$query = $this->db->get();
		return $query;
	}
	public function add($post){
		$idperode = $this->session->idperiod;
		$params['periode_id_periode'] = $idperode;
		$params['sk'] = $post['userfile1'] ;
		$this->db->insert('sk',$params);
	}
	public function del(){
		$user=$this->session->username;
		$cbg = $this->fungsi->user_login()->username;		
		$this->db->where('satker',$cbg);
		$this->db->delete('sk');

	}			
}