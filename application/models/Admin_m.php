<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_m extends CI_Model{

	function getById($id) {
		$this->db->from("periode");
		$this->db->where("satker_username",$id);
		$this->db->order_by("tahun", "desc");
    	$this->db->limit(1);
		$data = $this->db->get();
		return $data;
	}
	function getId(){
		$this->db->from("satker");
		$this->db->where("username",$this->session->username);
		return $this->db->get();
	}
	function edit($post){
		$params['nama'] = $post['namasatker'];
		$params['nama_kepala'] = $post['namakepalasatker'];
		$params['nama_admin'] = $post['namaadmin'];
		$params['no_hp_satker'] = $post['hpsatker'];
		$params['no_hp_admin'] = $post['hpadmin'];
		$params['alamat'] = $post['alamat'];
		$params['kota'] = $post['kota'];
		$this->db->where("username",$post['username']);
		$this->db->update("satker",$params);
	}
	public function get($id=null,$table=null,$join=null){
		$sql = "SELECT * from bpjs a 
				inner join cabang b on a.cabang = b.id 
				where a.hapus = 0";
		if($id!=null){
			$sql .= " and a.username = '".$id."' ";
		}
		return $this->db->query($sql);

		// $this->db->from('bpjs');
		// $this->db->join('cabang','cabang ')
		// if($id!=null){
		// 	$this->db->where('username',$id);
		// }
		// $this->db->where('hapus',0);		
		// $query = $this->db->get();
		// return $query;
	}
	public function getCabang($id=null){
		$sql = "SELECT * from cabang where hapus_data = 0";
		if($id!=null){
			$sql .= " and id = '".$id."' ";
		}
		return $this->db->query($sql);
	}
	public function add($post){
		$options = [
		    'cost' => 10,
		];
		$params['username'] = $post['username'];
		$params['nama'] = $post['nama'];
		$params['password'] = password_hash($post['password'], PASSWORD_DEFAULT, $options);
		$this->db->insert('bpjs',$params);
	}
	// public function editadmin($post){
	// 	$options = [
	// 	    'cost' => 10,
	// 	];
	// 	if(!empty($post['password'])){
	// 	$params['password'] = password_hash($post['password'], PASSWORD_DEFAULT, $options);
	// 	}
	// 	$params['username'] = $post['username'];
	// 	$params['nama'] = $post['nama'];
	// 	$this->db->where('username',$post['usernameasli']);
	// 	$this->db->update('bpjs',$params);
	// 		}
	public function updateqr($post,$img){
		$params['qr'] = $img;
		$this->db->where('username',$post['username']);
		$this->db->update('satker',$params);
	}
	public function del($id){
		$params['hapus'] = 1;
		$this->db->where('username',$id);
		$this->db->update('satker',$params);
	}
	function editadmin($data,$id){
		$this->db->where('username',$id);
		return $this->db->update('bpjs',$data);
	}
	function addadmin($data,$id){
		return $this->db->insert('bpjs',$data);
	}
}