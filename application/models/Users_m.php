<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users_m extends CI_Model{
	public function login($post){
		$this->db->select('*');
		$this->db->from('bpjs');
		$this->db->where('username',$post['username']);
		$this->db->where('hapus',0);
		$query = $this->db->get();
		return $query;
	}
	public function loginuser($post){
		$this->db->select('*');
		$this->db->from('satker');
		$this->db->join('periode', 'satker.username = periode.satker_username');
		$this->db->where('username',$post['username']);
		$this->db->where('hapus',0);
		$this->db->order_by("tahun", "desc");
    	$this->db->limit(1);		
		$query = $this->db->get();
		return $query;
	}
	public function getuser($id=null){
		$this->db->select('*');
		$this->db->from('satker');
		$this->db->join('periode', 'satker.username = periode.satker_username');
		if($id!=null){
			$this->db->where('username',$id);
		}
		$this->db->order_by("tahun", "desc");
    	$this->db->limit(1);
		$query = $this->db->get();
		return $query;
	}
	public function getadmin($id=null){
		$this->db->select('*');
		$this->db->from('bpjs');
		if($id!=null){
			$this->db->where('username',$id);
		}
		$query = $this->db->get();
		return $query;
	}
	public function addperiod($id){
		$params['tahun'] = date("Y");
		$params['satker_username'] = $id;
		$this->db->insert('periode',$params);
	}
	public function gantipass($post){
		$options = [
		    'cost' => 10,
		];
		$params['password'] = password_hash($post['passwordbaru'], PASSWORD_DEFAULT, $options);
		$username = $this->session->username;
		$this->db->where('username',$username);
		$this->db->update('bpjs',$params);
	}
	public function gantipassuser($post){
		$options = [
		    'cost' => 10,
		];
		$params['password'] = password_hash($post['passwordbaru'], PASSWORD_DEFAULT, $options);
		$username = $this->session->username;
		$this->db->where('username',$username);
		$this->db->update('satker',$params);
	}
}