<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Basic_m extends CI_Model{
	function get($table) {
		$data = $this->db->from($table)->get();
		return $data;
	}
	function getById($table,$primer,$id,$order=null) {
		$this->db->from($table);
		$this->db->where($primer,$id);
		if ($order!=null) {
			$this->db->order_by('tahun', $order);
		}
		$data = $this->db->get();
		return $data;
	}
	function cekAuto($bln,$tahun){
	    $this->db->from('iuran_month');
	    $this->db->where('bulan',$bln);
	    $cek = $this->db->get()->num_rows();
	    if($cek < 1){
	        $sqlAuto = "insert into iuran_month
				SELECT 
						'', 
					  A.jumlah_pegawai
					  ,A.penghasilan
					  ,A.tunjangan
					  ,A.thp
					  ,A.dpi
					  ,A.iuran_1
					  ,A.iuran_4
					  ,case when B.nominal is null then 0 else B.nominal end as actual_iuran_1
					  ,case when C.nominal is null then 0 else C.nominal end as actual_iuran_4
					  ,case when A.jml_is is null then 0 else A.jml_is end as jml_an
					  ,case when A.jml_an is null then 0 else A.jml_an end as jml_an
					  ,A.total_i
					  ,A.bln
					  ,A.periode_id_periode 
					  FROM 
						(
						  SELECT 
							count(nama_pegawai) as jumlah_pegawai, 
							sum(penghasilan) as penghasilan, 
							sum(tunjangan) as tunjangan, 
							sum(thp) as thp, 
							sum(dpi) as dpi, 
							sum(iuran_1) as iuran_1, 
							sum(iuran_4) iuran_4, 
							sum(total_i) as total_i, 
							'".$bln."' as bln,
							periode_id_periode,
							sum(jml_is) as jml_is,
							sum(jml_an) as jml_an
						  FROM 
							`data_satker` 
						  where 
							bulan_start <= '".$bln."' 
							and (
							  bulan_end > '".$bln."' 
							  or bulan_end = '0000-00-00'
							) and periode_id_periode in (select id_periode from periode where tahun = '".$tahun."')
						  group by 
							periode_id_periode, 
							bln
						) A 
						LEFT JOIN (
						  SELECT 
							bulan, 
							nominal, 
							periode_id_periode 
						  FROM 
							satup 
						  where 
							STATUS = 1 
						) B ON b.bulan = '".$bln."' and a.periode_id_periode = b.periode_id_periode 
						  LEFT JOIN (
							  SELECT 
								bulan, 
								nominal, 
								periode_id_periode 
							  FROM 
								empatp 
							  where 
								STATUS = 1
							) C ON c.bulan = '".$bln."' and a.periode_id_periode = c.periode_id_periode
					";
	        return $this->db->query($sqlAuto);
	    }else{
	        return true;
	    }
	    
	}
	function cekAutoPeriode($thn){
	    // $this->db->from('periode');
	    // $this->db->where('tahun',$thn);
	    // $cek = $this->db->get()->num_rows();
	    // if($cek < 1){
	        $sqlAuto = "insert into periode
	        SELECT ''as id,'".$thn."',username,CURRENT_DATE() FROM `satker` where username not in (select satker_username from periode where tahun = '".$thn."')";
	        return $this->db->query($sqlAuto);
	    // }else{
	    //     return true;
	    // }   
	}
	function getSatkerByPeriode($id){
		$this->db->from('satker');
		$this->db->where('username',$id);
		$this->db->limit(1);
		$data = $this->db->get();
		return $data;
	}
	function hitung($table,$primer,$id) {
		$this->db->select('count(*) as jml');
		$this->db->from($table);
		$this->db->where($primer,$id);
		$data = $this->db->get();
		return $data;
	}
	function total($table){
		$cabang = $this->session->cabang;
		$sql = "SELECT sum(nominal) as tot FROM ".$table." a
			INNER join periode b on b.id_periode = a.periode_id_periode
			inner join satker c on b.satker_username = c.username
			where 1 = 1";
		if($this->session->level == 1){
			$sql .= " and c.cabang = ".$cabang." ";
		}
		return $this->db->query($sql);
		// $this->db->select('sum(nominal) as tot');
		// $this->db->from($table);
		// $this->db->join('periode','')
		// $data = $this->db->get();
		// return $data;


	}
	function delete($table,$primer,$id) {
	$this->db->where($primer,$id);
	$this->db->delete($table);
	}
	function stat($table,$premier,$id){
		$this->db->select('count(*) as jml, monthname(bulan) as bulans');
		$this->db->from($table);
		$this->db->where($premier,$id);
		$this->db->group_by("bulan"); 
		$data = $this->db->get();
		return $data;
	}
	function deleteimport($id,$bulan,$table){
	$this->db->where('periode_id_periode',$id);
	$this->db->where('bulan',$bulan);
	$this->db->delete($table);
	}
	function getImport($id,$table){
		$this->db->distinct();
		$this->db->select('monthname(bulan) as bulans,bulan, periode_id_periode  as periode');
		$this->db->from($table);
		$this->db->where('periode_id_periode',$id);
		$this->db->order_by('bulan', 'ASC');
		$data = $this->db->get();
		return $data;
	}
	function getExport($id,$table,$bulan){
		$this->db->from($table);
		$this->db->where('periode_id_periode',$id);
		$this->db->where('bulan',$bulan);
		$data = $this->db->get();
		return $data;
	}
	function uplastdate($id){
		$params['lastdate'] = date("Y-m-d");
		$this->db->where('id_periode',$id);
		$this->db->update('periode',$params);
	}
	function belumBayar(){
		date_default_timezone_set("Asia/Jakarta");
		$month = date("Ym");
		$sql = "SELECT COUNT(DISTINCT periode_id_periode) as jumlah FROM import_satker where EXTRACT(YEAR_MONTH FROM bulan) = '".$month."' ";
		return $this->db->query($sql);
	}
	function BelumExport($from=null,$to=null){
		date_default_timezone_set("Asia/Jakarta");
		$month = date("Ym");
		$where = " ";
		if($from != null){
			$where = " where EXTRACT(YEAR_MONTH FROM bulan) BETWEEN ".$from." and ".$to." and c.nama is not null " ;
		}else{
			$where = " where EXTRACT(YEAR_MONTH FROM bulan) = ".$month."  and c.nama is not null" ;
		}
		$sql = "SELECT DISTINCT * from satker where nama not in (select DISTINCT c.nama from import_satker a inner join periode b on a.periode_id_periode = b.id_periode inner join satker c on b.satker_username = c.username ".$where.") and nama is not null order by nama";
		// echo $sql;
		return $this->db->query($sql);
	}
	function jumlahSatker($table=null){
		$cabang = $this->session->cabang;
			if($this->session->level == 3){
				$sql = "SELECT distinct * FROM satker where nama is not null ";
			}else{
				$sql = "SELECT distinct * FROM satker where cabang = '".$cabang."'  and nama is not null ";
			}
			return $this->db->query($sql);
	}
	function jumlahCabang($table=NULL){
				$sql = "SELECT distinct * FROM cabang where nama_cabang is not null and hapus_data is not null";
			return $this->db->query($sql);
	}
	function buktiBayar1($from=null,$to=null){
		$cabang = $this->session->cabang;
		$date = date('Ym');
		$where = " ";
		if($from!=null){
			$where = " WHERE EXTRACT(YEAR_MONTH FROM bulan) between ".$from." and ".$to." and c.nama is not null";
		}else{
			$where = " WHERE EXTRACT(YEAR_MONTH FROM bulan) = ".$date." and c.nama is not null ";
		}

		if($this->session->level == 1){
			$where .= " and c.cabang = '".$cabang."' " ;
		}

		$sql = "SELECT  * from satker where nama not in (select distinct c.nama from satup a
				inner join periode b on a.periode_id_periode = b.id_periode
				inner join satker c on b.satker_username = c.username
				".$where.") and nama is not null order by nama";
		return $this->db->query($sql);
	}
	function buktiBayar4($from=null,$to=null){
		$cabang = $this->session->cabang;
		$date = date('Ym');
		$where = " ";
		if($from!=null){
			$where = " WHERE EXTRACT(YEAR_MONTH FROM bulan) between ".$from." and ".$to." and c.nama is not null ";
		}else{
			$where = " WHERE EXTRACT(YEAR_MONTH FROM bulan) = ".$date." and c.nama is not null ";
		}
		if($this->session->level == 1){
			$where .= " and c.cabang = '".$cabang."' " ;
		}

		$sql = "SELECT distinct * from satker where nama not in (select distinct c.nama from empatp a inner join periode b on a.periode_id_periode = b.id_periode inner join satker c on b.satker_username = c.username ".$where.") and nama is not null order by nama";
		
		return $this->db->query($sql);
	}
	function cekSatkerExport($id,$date){
		if($date == null || $date == ''){
			$date = date('Ym');
		}
		$sql= "SELECT distinct DATE_FORMAT(a.bulan, '%b-%Y') as bulan1, c.nama from import_satker a inner join periode b on a.periode_id_periode = b.id_periode inner join satker c on b.satker_username = c.username where periode_id_periode in (SELECT DISTINCT id_periode FROM periode where satker_username = '".$id."') and EXTRACT(YEAR_MONTH FROM a.bulan) <= ".$date." order by bulan desc limit 3";
		// echo $sql
	return $this->db->query($sql);
	}
	function cekUpload1($id,$date){
		if($date == null || $date == ''){
			$date = date('Ym');
		}
		$sql= "SELECT distinct DATE_FORMAT(a.bulan, '%b-%Y') as bulan1, c.nama from satup a inner join periode b on a.periode_id_periode = b.id_periode inner join satker c on b.satker_username = c.username where periode_id_periode in (SELECT DISTINCT id_periode FROM periode where satker_username = '".$id."') and EXTRACT(YEAR_MONTH FROM a.bulan) <= ".$date." order by bulan desc limit 3";
		// echo $sql
	return $this->db->query($sql);
	}
	function cekUpload4($id,$date){
		if($date == null || $date == ''){
			$date = date('Ym');
		}
		$sql= "SELECT distinct DATE_FORMAT(a.bulan, '%b-%Y') as bulan1, c.nama from empatp a inner join periode b on a.periode_id_periode = b.id_periode inner join satker c on b.satker_username = c.username where periode_id_periode in (SELECT DISTINCT id_periode FROM periode where satker_username = '".$id."') and EXTRACT(YEAR_MONTH FROM a.bulan) <= ".$date." order by bulan desc limit 3";
		// echo $sql
	return $this->db->query($sql);
	}

	function cekremark($data){
		return $this->db->get_where('validasi',$data)->num_rows();
	}
	function updateRemark($data){
		$periode = $data['periode'];
		$bulan   = $data['bulan'];
		$this->db->where('periode',$periode);
		$this->db->where('bulan',$bulan);
		return $this->db->update('validasi',$data);
	}
	function saveRemark($data){
		return $this->db->insert('validasi',$data);
		// echo $data;
	}
	function getImportSatker($id){
		$this->db->distinct();
		$this->db->select(' monthname(a.bulan) as bulans,a.bulan, periode_id_periode  as periode, 
			IF(b.status IS NULL,0,b.status) 
             AS status');
		$this->db->from('import_satker a');
		$this->db->where('periode_id_periode',$id);
		$this->db->join('validasi b', 'a.periode_id_periode = b.periode and a.bulan = b.bulan','left');
		$this->db->order_by('bulan', 'ASC');
		$data = $this->db->get();
		return $data;
	}
	function deletevalidasi($id,$bulan,$table){
		$this->db->where('periode',$id);
		$this->db->where('bulan',$bulan);
		$this->db->delete($table);
	}
	function cekstatus($periode,$bulan){
		$this->db->select('status');
		$this->db->where('periode',$periode);
		$this->db->where('bulan',$bulan);
		return $this->db->get('validasi');
	}
	function getremark($table,$periode,$colom){
		$this->db->where($colom,$periode);
		$this->db->where('status',2);
		return $this->db->get($table);
	}
}
