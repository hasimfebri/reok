<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengajuan_m extends CI_Model{
	function getbyId($id) {
		$this->db->from("pengajuan_satker");
		$this->db->where("id_impsatker",$id);
		$data = $this->db->get();
		return $data;
	}
	function getexist($id) {
		$this->db->from("satker_active");
		$this->db->where("periode_id_periode",$id);
		$data = $this->db->get();
		return $data;
	}
	function getPengajuan($id) {
		$this->db->from("pengajuan_satker");
		$this->db->where("periode_id_periode",$id);
		$data = $this->db->get();
		return $data;
	}
    function getData($id) {
		$this->db->from("pengajuan_satker");
		$this->db->where("periode_id_periode",$id);
        $this->db->where("status <>",1);
		$data = $this->db->get();
		return $data;
	}
	function getPeriode($id) {
		$this->db->from("periode");
		$this->db->join("satker",'satker_username = username');
		$this->db->where("id_periode",$id);
		$data = $this->db->get();
		return $data;
	}
	function addPengajuan($data){
		return $this->db->insert('pengajuan_satker',$data);
	}
	function addBukti($post){
		$params['bukti'] = $post['bukti'];
		$this->db->where('id_impsatker',$post['id']);
		return $this->db->update('pengajuan_satker',$params);
	}
	function updatePengajuan($data,$id){
		$this->db->where('id_impsatker',$id);
		return $this->db->update('pengajuan_satker',$data);
	}
	function getNik($id){
		$this->db->from("satker_active");
		$this->db->where("periode_id_periode",$id);
		$data = $this->db->get();
		return $data;
	}
	function delPengajuan($id,$j){
	    if($j == 2 || $j == "2"){
	         $sqlbackup = "insert into b_pengajuan_anggota_keluarga 
		    select '',pengajuan_anggota_keluarga.*,DATE_FORMAT(CURDATE(), '%Y-%m-%d'),2 from pengajuan_anggota_keluarga where id_impsatker = '".$id."' ";
		    
		    if ($this->db->query($sqlbackup)){
		        	$this->db->where("id_impsatker",$id);
		            return $data = $this->db->delete('pengajuan_anggota_keluarga');
		    }else{
		        return false;
		    }
	    }else if($j == 1 || $j == "1"){
	        $sqlbackup = "insert into b_pengajuan_satker 
		    select '',pengajuan_satker.*,DATE_FORMAT(CURDATE(), '%Y-%m-%d'),2 from pengajuan_satker where id_impsatker = '".$id."' ";
		      if ($this->db->query($sqlbackup)){
	        	    $this->db->where("id_impsatker",$id);
		            return $data = $this->db->delete('pengajuan_satker');
		      }else{
		        return false;
		    }
	    }
	}
	function getEdit($id){
		$this->db->select("*, date_format(bulan,'%m') as month");
		$this->db->from("pengajuan_satker");
		$this->db->where('id_impsatker',$id);
		$this->db->where('status',2);
		return $this->db->get();
		
	}
	function getAnggota(){
		$this->db->from("pengajuan_anggota_keluarga");
		$this->db->where('periode_id_periode',$this->session->idperiod);
		$this->db->where('status !=',0);
		return $cek = $this->db->get();
	}
	function validasiPenonAnggota($id){
	    $this->db->from("pengajuan_anggota_keluarga");
		$this->db->where('id_impsatker',$id);
		$cek = $this->db->get();
		if($cek->num_rows() > 0){
		    $sqlbackup = "insert into b_pengajuan_anggota_keluarga 
		    select '',pengajuan_anggota_keluarga.*,DATE_FORMAT(CURDATE(), '%Y-%m-%d'),1 from pengajuan_anggota_keluarga where id_impsatker = '".$id."' ";
		    
		    $backup = $this->db->query($sqlbackup);
		    if($backup){
		        $sqldelp = "DELETE FROM anggota_keluarga where nik = '".$cek->row()->nik."' ";
		        if($this->db->query($sqldelp)){
		           $sqldela = "DELETE FROM pengajuan_anggota_keluarga where id_impsatker = '".$id."' ";
		            return $this->db->query($sqldela); 
		        }else{
		            return false;
		        }
		        
		    }else{
		        	return false;
		    }
		    
		}else{
		    return 0;
		}
	}
	function validasiAnggota($id){
	    $this->db->from("pengajuan_anggota_keluarga");
		$this->db->where('id_impsatker',$id);
		$cek = $this->db->get();
		if($cek->num_rows() > 0){
		    $sqlbackup = "insert into b_pengajuan_anggota_keluarga 
		    select '',pengajuan_anggota_keluarga.*,DATE_FORMAT(CURDATE(), '%Y-%m-%d'),1 from pengajuan_anggota_keluarga where id_impsatker = '".$id."' ";
		    
		    $backup = $this->db->query($sqlbackup);
		    if($backup){
		        $sqlsave = "insert into anggota_keluarga
		                SELECT '',nik_sumber, nik, no_kk,nama,status, tgl_lahir, entry_date,periode_id_periode FROM pengajuan_anggota_keluarga where id_impsatker = '".$id."' ";
		        if($this->db->query($sqlsave)){
		                $sqldela = "DELETE FROM pengajuan_anggota_keluarga where id_impsatker = '".$id."' ";
		                return $this->db->query($sqldela);
		        }else{
		            return false;
		        }
		    }else{
		        	return false;
		    }
		    
		}else{
		    return 0;
		}
	}

    function validasi($id){
        $this->load->library('encryption');	
		$this->db->from("pengajuan_satker");
		$this->db->where('id_impsatker',$id);
		$cek = $this->db->get();
		
		if($cek->num_rows() < 1){
		    return 0;
		}
		$sqlbackup = "insert into b_pengajuan_satker 
		              select '',pengajuan_satker.*,DATE_FORMAT(CURDATE(), '%Y-%m-%d'),1 from pengajuan_satker where id_impsatker = '".$id."' ";
        if ($this->db->query($sqlbackup)){
				if($cek->row()->jenis == '1'){
					$sqlActive = "insert into satker_active
						SELECT '', nama_satker, nama_pegawai,nik, nip, jml_is, jml_an, penghasilan, tunjangan, thp, dpi, iuran_1, iuran_4, total_i, bulan, periode_id_periode FROM pengajuan_satker where id_impsatker = '".$id."' and status = 0
						";
					$addActive =  $this->db->query($sqlActive);
					if($addActive){
						$sqlData = "insert into data_satker
						SELECT '', nama_satker, nama_pegawai,nik, nip, jml_is, jml_an, penghasilan, tunjangan, thp, dpi, iuran_1, iuran_4, total_i, bulan,'', periode_id_periode FROM pengajuan_satker where id_impsatker = '".$id."' and status = 0 
						";
						$dataSatker =  $this->db->query($sqlData);
					}else{
						return false;
					}
				}else if($cek->row()->jenis == '2'){
				    $sqlceknik = "select * from satker_active WHERE nama_pegawai = '".$cek->row()->nama_pegawai."' and  periode_id_periode = '".$cek->row()->periode_id_periode."' ";
				    $ceknik = $this->db->query($sqlceknik)->result();
				    
				    foreach($ceknik as $key){
				        if($this->encryption->decrypt($key->nik) == $this->encryption->decrypt($cek->row()->nik)){
				            	$sqlActive = "DELETE FROM satker_active WHERE nama_pegawai = '".$cek->row()->nama_pegawai."' and nik = '".$key->nik."' and periode_id_periode = '".$cek->row()->periode_id_periode."' ";
				                $addActive =  $this->db->query($sqlActive);
				            if($addActive){
					        	$sqlData = "UPDATE data_satker SET bulan_end = '".$cek->row()->bulan."' WHERE nama_pegawai = '".$cek->row()->nama_pegawai."' and nik = '".$key->nik."' and periode_id_periode = '".$cek->row()->periode_id_periode."'";
					        	$dataSatker =  $this->db->query($sqlData);
					        }else{
					        	return false;
					        }
				        }
				    }
					
				}
			if($dataSatker){
			        $this->db->where('id_impsatker',$id);
		 	        return $this->db->delete('pengajuan_satker');
			}else{
			
			    return false;
			}
        }else{
             return false;
        }
	}
	function Tolak($id,$catatan,$jenis){
		$params['catatan']  = $catatan;
		$params['id']		= $id;
	
		if($jenis == 1 || $jenis == "1"){
		    $this->db->from("pengajuan_satker");
		}else if($jenis == 2 || $jenis == "2"){
		    $this->db->from("pengajuan_anggota_keluarga");
		}
		$this->db->where('id_impsatker',$id);
		$cek = $this->db->get()->num_rows();
		if($cek > 0){
			$this->db->from("catatan");
			$this->db->where('id',$id);
			$cekCatat = $this->db->get()->num_rows();
			if($cekCatat < 1){
				$this->db->insert('catatan',$params);
			}else{
				$this->db->set('catatan',$catatan);
				$this->db->where('id',$id);
				$this->db->update('catatan');
			}
			    if($jenis == 1 || $jenis == "1"){
    				    $this->db->set('status',2);
				    $this->db->where('id_impsatker',$id);
				    return $this->db->update('pengajuan_satker');
			    }else if($jenis == 2 || $jenis == "2"){
				    $this->db->where('id_impsatker',$id);
				    return $this->db->update('pengajuan_anggota_keluarga');
			    }
		}
		return false;
	}
	function getTolak($id,$j){
			$this->db->from("catatan");
			$this->db->where('id',$id);
			return $cekCatat = $this->db->get();
	}
    
}
