<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Satker_m extends CI_Model{

	function getById($id) {
		$this->db->from("periode");
		$this->db->where("satker_username",$id);
		$this->db->order_by("tahun", "desc");
    	$this->db->limit(1);
		$data = $this->db->get();
		return $data;
	}
	function getId(){
		$this->db->from("satker");
		$this->db->where("username",$this->session->username);
		return $this->db->get();
	}
	function edit($post){
		$params['nama'] = $post['namasatker'];
		$params['nama_kepala'] = $post['namakepalasatker'];
		$params['nama_admin'] = $post['namaadmin'];
		$params['no_hp_satker'] = $post['hpsatker'];
		$params['no_hp_admin'] = $post['hpadmin'];
		$params['alamat'] = $post['alamat'];
		$params['kota'] = $post['kota'];
		$params['gambar'] = $post['logo'];
		$this->db->where("username",$post['username']);
		$this->db->update("satker",$params);
	}
	public function get($data){
		$sql = "SELECT * FROM satker a 
				left join cabang b on a.cabang  = b.id
				where hapus = 0";
		if($this->session->level == 1){
			$sql .= " and a.cabang = ".$this->session->cabang." ";
		}
		if($this->session->level == 3){
			if($data['cabang']!= ""){
				$sql .=" and a.cabang = ".$data['cabang']." ";
			}
		}
		if($data['id']){
			$sql .=" and a.username = '".$data['id']."'  ";
		}
		return $this->db->query($sql);


		// $this->db->from('satker');
		// if($id!=null){
		// 	$this->db->where('username',$id);
		// }
		// if($this->session->level == 1){
		// 	$this->db->where('cabang',$this->session->cabang);
		// }
		// $this->db->where('hapus',0);		
		// $query = $this->db->get();
		// return $query;
	}
	public function add($post){
		$options = [
		    'cost' => 10,
		];
		$params['gambar'] = $post['logo'];
		$params['username'] = $post['username'];
		$params['password'] = password_hash($post['password'], PASSWORD_DEFAULT, $options);
		$par['cabang'] = $this->session->cabang; 
		$this->db->insert('satker',$params);
		$par['satker_username'] = $post['username'];
		$par['tahun'] = date("Y");
		$this->db->insert('periode',$par);
	}
	public function editsatker($post){
		$options = [
		    'cost' => 10,
		];
		if(!empty($post['password'])){
		$params['password'] = password_hash($post['password'], PASSWORD_DEFAULT, $options);
		}
		$params['nama'] = $post['nama'];
		$params['alamat'] = $post['alamat'];
		$this->db->where('username',$post['username']);
		$this->db->update('satker',$params);
			}

	public function updateqr($post,$img){
		$params['qr'] = $img;
		$this->db->where('username',$post['username']);
		$this->db->update('satker',$params);
	}
	public function addlogo($post){
		$params['gambar'] = $post['logo'];
		$this->db->where('idlogo',"1");
		$this->db->update('logobpjs',$params);
	}
	public function updateqrbpjs($img){
		$params['qr'] = $img;
		$this->db->where('idlogo',"1");
		$this->db->update('logobpjs',$params);
	}
	public function del($id){
		$params['hapus'] = 1;
		$this->db->where('username',$id);
		$this->db->update('satker',$params);
	}
}
