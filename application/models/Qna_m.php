<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Qna_m extends CI_Model{
	public function get($id=null){
		$this->db->select('*');
		$this->db->from('qna');
		if($id!=null){
			$this->db->where('tema',$id);
		}
		$query = $this->db->get();
		return $query;
	}
	public function gettema(){
		$this->db->select('tema');
		$this->db->from('qna');
		$this->db->group_by('tema');
		$query = $this->db->get();
		return $query;
	}
	public function uploadBerkas($post,$id){
        $params['berkas'] = $post['image'.$id];
		$this->db->where('id_qna ',$id);
		$this->db->update('qna',$params);
	}
	public function userget($post=null){
		$userinput = $this->fungsi->user_login()->username;
		$this->db->select('*');
		$this->db->from('qna');
		$query = $this->db->get();
		return $query;
	}
	public function del($id){
		$this->db->where('id_qna',$id);
		$this->db->delete('qna');
	}
	public function edit($post){
		$params['tema'] = $post['tema'];
		$params['question'] = $post['question'];
		$params['answer'] = $post['answer'];
		$params['regulasi'] = $post['regulasi'];
		$this->db->where('id_qna',$post['id_qna']);
		$this->db->update('qna',$params);
	}		
}