<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Satup_m extends CI_Model{
	public function add($post){
		$level = $this->session->level;
		$params['nominal'] = $post['nominal1'];
		$params['bulan'] = $post['tahun1'].'-'.$post['bulan1'].'-01';
		if ($level == "2") {
			$params['bukti'] = $post['userfile1'] ;
			$params['status'] = 0 ;
		}
		$params['tgl_bayar'] = $post['tgl_byr1'] ;
		$params['periode_id_periode'] = $this->session->idperiod;
		if ($level == "2") {
		$this->db->insert('satup',$params);
		}else{
			$this->db->insert('fikssatu',$params);
		}
	}
	public function fsatu($row){
		$params['nominal'] = $row->nominal;
		$params['bulan'] = $row->bulan;
		$params['tgl_bayar'] = $row->tgl_bayar;
		$params['periode_id_periode'] = $row->periode_id_periode;
		$params['id_satu_id'] = $row->id_satu;
		$this->db->insert('fikssatu',$params);
	}
	public function tlkfsatu($row,$post){
		$params['nominal'] = $post['nominalsatu'];
		$params['bulan'] = $row->bulan;
		$params['tgl_bayar'] = $row->tgl_bayar;
		$params['periode_id_periode'] = $row->periode_id_periode;
		$params['id_satu_id'] = $row->id_satu;
		$this->db->insert('fikssatu',$params);
	}

	public function upsatu($id){
		$params['status'] = 1;
		$this->db->where('id_satu',$id);
		$this->db->update('satup',$params);
	}
	public function tolak($id,$post){
		$params['status'] = 2;
		$params['remark'] = $post['remark1'];
		$this->db->where('id_satu',$id);
		$this->db->update('satup',$params);
	}
	function cekdata($data){
		return $this->db->get_where('satup',$data);
	}
	function deletedata($id){
		$this->db->where('id_satu',$id);
		return $this->db->delete('satup');
	}
}