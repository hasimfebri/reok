<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Empatp_m extends CI_Model{
	public function add($post){
		$level = $this->session->level;
		$params['nominal'] = $post['nominal2'];
		$params['bulan'] = $post['tahun2'].'-'.$post['bulan2'].'-01';
		if ($level == "2") {
			$params['bukti'] = $post['userfile2'] ;
			$params['status'] = 0 ;
		}
		$params['tgl_bayar'] = $post['tgl_byr2'] ;
		$params['periode_id_periode'] = $this->session->idperiod;
		if ($level == "2") {
		$this->db->insert('empatp',$params);
		}else{
			$this->db->insert('fiksempat',$params);
		}
	}
	public function fempat($row){
		$params['nominal'] = $row->nominal;
		$params['bulan'] = $row->bulan;
		$params['tgl_bayar'] = $row->tgl_bayar;
		$params['periode_id_periode'] = $row->periode_id_periode;
		$params['id_empat_id'] = $row->id_empat;
		$this->db->insert('fiksempat',$params);
	}
	public function tlkfempat($row,$post){
		$params['nominal'] = $post['nominalempat'];
		$params['bulan'] = $row->bulan;
		$params['tgl_bayar'] = $row->tgl_bayar;
		$params['periode_id_periode'] = $row->periode_id_periode;
		$params['id_empat_id'] = $row->id_empat;
		$this->db->insert('fiksempat',$params);
	}
	public function upempat($id){
		$params['status'] = 1;
		$this->db->where('id_empat',$id);
		$this->db->update('empatp',$params);
	}
	public function tolak($id,$post){
		$params['status'] = 2;
		$params['remark'] = $post['remark4'];
		$this->db->where('id_empat',$id);
		$this->db->update('empatp',$params);
	}
	function cekdata($data){
		return $this->db->get_where('empatp',$data);
	}
	function deletedata($id){
		$this->db->where('id_empat',$id);
		return $this->db->delete('empatp');
	}			
}