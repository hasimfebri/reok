<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pegawai_m extends CI_Model{
    function cekData($nik){
        $sql = "SELECT * FROM pengajuan_satker where nik = '".$nik."' and jenis= 2 and status = 0";
		return $this->db->query($sql);
    }
    function cekPengajuan($nik){
        $sql = "SELECT * FROM pengajuan_anggota_keluarga where nik = '".$nik."' and status = 0";
		return $this->db->query($sql);
    }
    function getAnggota($nik){
        $sql = "SELECT * FROM anggota_keluarga where nik_sumber = '".$nik."'";
		return $this->db->query($sql);
    }
    function getAnggotaPenon($nik){
        $sql = "SELECT * FROM anggota_keluarga where nik = '".$nik."'";
		return $this->db->query($sql);
    }
    function getKK($nik){
        $sql = "SELECT DISTINCT * FROM anggota_keluarga where nik_sumber = '".$nik."' limit 1 ";
        return $this->db->query($sql);
    }
	function getbyId($id) {
		$this->db->from("pengajuan_satker");
		$this->db->where("id_impsatker",$id);
		$data = $this->db->get();
		return $data;
	}
	function getexist($id) {
		$this->db->from("satker_active");
		$this->db->where("periode_id_periode",$id);
		$data = $this->db->get();
		return $data;
	}
	function getPengajuan($id) {
		$this->db->from("pengajuan_satker");
		$this->db->where("periode_id_periode",$id);
		$data = $this->db->get();
		return $data;
	}
    function getData($id,$month) {
        if($month == ""){
		    $this->db->from("satker_active");
		    $this->db->where("periode_id_periode",$id);
		    return $this->db->get();
        }else{
             $sql = "SELECT *, ".$month." as bulan FROM `data_satker` where bulan_start <= '".$month."' and ( bulan_end > '".$month."' or bulan_end = '0000-00-00') and periode_id_periode = '".$id."' ";
	        return $this->db->query($sql);
	       //echo $sql;
        }
	}
	function getIuran($id){
	    $sql = "SELECT count(nama_pegawai) as jumlah_pegawai, sum(thp) as thp, sum(dpi) as dpi, sum(iuran_1) as iuran_1, sum(iuran_4) iuran_4, sum(total_i) as total_i FROM satker_active where periode_id_periode = '".$id."' ";
	    return $this->db->query($sql);
	}
	
	function getIuranMonth($id,$month){
	    $sql = "SELECT count(nama_pegawai) as jumlah_pegawai, sum(thp) as thp, sum(dpi) as dpi, sum(iuran_1) as iuran_1, sum(iuran_4) iuran_4, sum(total_i) as total_i FROM `data_satker` where bulan_start <= '".$month."' and ( bulan_end > '".$month."' or bulan_end = '0000-00-00') and periode_id_periode = '".$id."' ";
	    return $this->db->query($sql);
	   //echo $sql;
	}
	function saveMutasi($nik,$bukti){
	    var_dump($nik);
	    $sqlcek = "SELECT * from satker_active where nik = '".$nik."'";
	    if($this->db->query($sqlcek)->num_rows() < 1 ){
	        return 1;
	    }else{
	        $sqlsave = "insert into pengajuan_satker
	        SELECT '', nama_satker, nama_pegawai, nik, nip, jml_is, jml_an, penghasilan, tunjangan, thp, dpi, iuran_1, iuran_4, total_i,date_format(curdate(),'%Y-%m-%d'), periode_id_periode,0,2, '".$bukti."' FROM `satker_active` WHERE nik = '".$nik."' limit 1 "; 
	        return $this->db->query($sqlsave);
	        
	    }
	}
	function saveAnggota($post){
	    $param['nik_sumber']    = $post['nik_sumber'];
	    $param['nik']           = $post['nik_encript'];
	    $param['no_kk']         = $post['no_kk'];
	    $param['nama']          = $post['nama_anggota'];
	    $param['status']        = $post['status'];
	    $param['tgl_lahir']     = $post['birth'];
	    $param['entry_date']    = date('Y-m-d');
	    $param['periode_id_periode']    = $this->session->idperiod;
	    $param['status2']        = 0;
	    $param['bukti']        = $post['bukti'];
	    $param['jenis']        = 1;
	    return $this->db->insert('pengajuan_anggota_keluarga',$param);
	}
	function saveAnggotaPenon($post){
	    $sqlMutasi = "insert into pengajuan_anggota_keluarga
	         SELECT '', nik, nik_sumber, no_kk, nama, status, tgl_lahir, entry_date, periode_id_periode,0,'".$post['bukti']."',2 FROM anggota_keluarga where nik = '".$post['nik_penon']."' ";
	    return $this->db->query($sqlMutasi);
	}
	
    
}
