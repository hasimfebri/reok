<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan_m extends CI_Model{
	public function userget($post=null){
		$cbg = $this->fungsi->user_login()->username;
		if($post!=null){
			$query = $this->db->query("select surat.no_regis as no_regis,surat.tgl_validasi as validasi,surat.tgl_setor as tgl_setor,fpk.kasus as kasus, SUM(fpk.biaya) as bia, fpk.no_fpk as fpk,DATE_FORMAT(fpk.bulan_pelayanan, '%M %Y') as bulans,monthname(fpk.bulan_pelayanan), month(surat.tgl_setor) as bln, year(surat.tgl_setor) as thn,GROUP_CONCAT(jenis_pelayanan.jenis_pelayanan) as lyn from surat left join fpk on surat.no_regis = fpk.surat_no_regis left join pelayanan on pelayanan.fpk_no_fpk = fpk.no_fpk left join jenis_pelayanan on jenis_pelayanan.id = pelayanan.jenis_pelayanan_id  where surat.surat_username = '$cbg' and surat.status = 1 and surat.tgl_setor between '$post[tgl_awal]' and '$post[tgl_akhir]' group by fpk.no_fpk order by surat.no_regis ASC");
		}else{
			$query = $this->db->query("select surat.no_regis as no_regis,surat.tgl_validasi as validasi,surat.tgl_setor as tgl_setor,fpk.kasus as kasus, SUM(fpk.biaya) as bia, monthname(fpk.bulan_pelayanan) as bln2, month(surat.tgl_setor) as bln, year(surat.tgl_setor) as thn,GROUP_CONCAT(jenis_pelayanan.jenis_pelayanan) as lyn from surat left join fpk on surat.no_regis = fpk.surat_no_regis left join pelayanan on pelayanan.fpk_no_fpk = fpk.no_fpk left join jenis_pelayanan on jenis_pelayanan.id = pelayanan.jenis_pelayanan_id  where surat.status = 1 and surat.tgl_setor between '$post[tgl_awal]' and '$post[tgl_akhir]' group by surat.no_regis order by surat.tgl_setor ASC");
		}	
		return $query;
	}
	public function adminget($post=null){
		$cbg = $post['fktps'];
		$adm =  $this->fungsi->user_login()->cabang;
		if($post!=null){
			if ($cbg=="0") {
			$query = $this->db->query("select surat.no_regis as no_regis,surat.tgl_validasi as validasi,surat.tgl_setor as tgl_setor,fpk.kasus as kasus, SUM(fpk.biaya) as bia,nama_fktp, fpk.no_fpk as fpk,DATE_FORMAT(fpk.bulan_pelayanan, '%M %Y') as bulans,monthname(fpk.bulan_pelayanan), month(surat.tgl_setor) as bln, year(surat.tgl_setor) as thn,GROUP_CONCAT(jenis_pelayanan.jenis_pelayanan) as lyn from surat left join fpk on surat.no_regis = fpk.surat_no_regis left join pelayanan on pelayanan.fpk_no_fpk = fpk.no_fpk left join jenis_pelayanan on jenis_pelayanan.id = pelayanan.jenis_pelayanan_id join user on surat.surat_username = user.username join fktp on user.idfktp_user = fktp.idfktp where surat.status = 1 and fktp.kode_cabang_fktp = '$adm' and surat.tgl_setor between '$post[tgl_awal]' and '$post[tgl_akhir]' group by fpk.no_fpk order by surat.no_regis ASC");					
			}else{
			$query = $this->db->query("select surat.no_regis as no_regis,surat.tgl_validasi as validasi,surat.tgl_setor as tgl_setor,fpk.kasus as kasus, SUM(fpk.biaya) as bia,nama_fktp, fpk.no_fpk as fpk,DATE_FORMAT(fpk.bulan_pelayanan, '%M %Y') as bulans,monthname(fpk.bulan_pelayanan), month(surat.tgl_setor) as bln, year(surat.tgl_setor) as thn,GROUP_CONCAT(jenis_pelayanan.jenis_pelayanan) as lyn from surat left join fpk on surat.no_regis = fpk.surat_no_regis left join pelayanan on pelayanan.fpk_no_fpk = fpk.no_fpk left join jenis_pelayanan on jenis_pelayanan.id = pelayanan.jenis_pelayanan_id join user on surat.surat_username = user.username join fktp on user.idfktp_user = fktp.idfktp where user.idfktp_user = '$cbg' and fktp.kode_cabang_fktp = '$adm' and surat.status = 1 and surat.tgl_setor between '$post[tgl_awal]' and '$post[tgl_akhir]' group by fpk.no_fpk order by surat.no_regis ASC");				
			}

		}else{
			$query = $this->db->query("select surat.no_regis as no_regis,surat.tgl_setor as tgl_setor,fpk.kasus as kasus, SUM(fpk.biaya) as bia, monthname(fpk.bulan_pelayanan) as bln2, month(surat.tgl_setor) as bln, year(surat.tgl_setor) as thn,GROUP_CONCAT(jenis_pelayanan.jenis_pelayanan) as lyn from surat left join fpk on surat.no_regis = fpk.surat_no_regis left join pelayanan on pelayanan.fpk_no_fpk = fpk.no_fpk left join jenis_pelayanan on jenis_pelayanan.id = pelayanan.jenis_pelayanan_id  where surat.status = 1 and fktp.kode_cabang_fktp = '$adm' and surat.tgl_setor between '$post[tgl_awal]' and '$post[tgl_akhir]' group by surat.no_regis order by surat.tgl_setor ASC");
		}	
		return $query;
	}

	public function superget($post=null){
		$fktp = $post['fktpsup'];
		$cabang = $post['kdc'];
		if($post!=null){
			if ($cabang=="0") {
			$query = $this->db->query("select surat.no_regis as no_regis, nama_cabang, surat.tgl_validasi as validasi,surat.tgl_setor as tgl_setor,fpk.kasus as kasus, SUM(fpk.biaya) as bia,nama_fktp, fpk.no_fpk as fpk,DATE_FORMAT(fpk.bulan_pelayanan, '%M %Y') as bulans,monthname(fpk.bulan_pelayanan), month(surat.tgl_setor) as bln, year(surat.tgl_setor) as thn,GROUP_CONCAT(jenis_pelayanan.jenis_pelayanan) as lyn from surat left join fpk on surat.no_regis = fpk.surat_no_regis left join pelayanan on pelayanan.fpk_no_fpk = fpk.no_fpk left join jenis_pelayanan on jenis_pelayanan.id = pelayanan.jenis_pelayanan_id join user on surat.surat_username = user.username join fktp on user.idfktp_user = fktp.idfktp join cabang on cabang.kode_cabang = fktp.kode_cabang_fktp where surat.status = 1 and surat.tgl_setor between '$post[tgl_awal]' and '$post[tgl_akhir]' group by fpk.no_fpk order by surat.no_regis ASC");					
			}else{
				if ($fktp == "0") {
					$query = $this->db->query("select surat.no_regis as no_regis, nama_cabang, surat.tgl_validasi as validasi,surat.tgl_setor as tgl_setor,fpk.kasus as kasus, SUM(fpk.biaya) as bia,nama_fktp, fpk.no_fpk as fpk,DATE_FORMAT(fpk.bulan_pelayanan, '%M %Y') as bulans,monthname(fpk.bulan_pelayanan), month(surat.tgl_setor) as bln, year(surat.tgl_setor) as thn,GROUP_CONCAT(jenis_pelayanan.jenis_pelayanan) as lyn from surat left join fpk on surat.no_regis = fpk.surat_no_regis left join pelayanan on pelayanan.fpk_no_fpk = fpk.no_fpk left join jenis_pelayanan on jenis_pelayanan.id = pelayanan.jenis_pelayanan_id join user on surat.surat_username = user.username join fktp on user.idfktp_user = fktp.idfktp join cabang on cabang.kode_cabang = fktp.kode_cabang_fktp where fktp.kode_cabang_fktp = '$cabang' and surat.status = 1 and surat.tgl_setor between '$post[tgl_awal]' and '$post[tgl_akhir]' group by fpk.no_fpk order by surat.no_regis ASC");	
				}else{
					$query = $this->db->query("select surat.no_regis as no_regis, nama_cabang, surat.tgl_validasi as validasi,surat.tgl_setor as tgl_setor,fpk.kasus as kasus, SUM(fpk.biaya) as bia,nama_fktp, fpk.no_fpk as fpk,DATE_FORMAT(fpk.bulan_pelayanan, '%M %Y') as bulans,monthname(fpk.bulan_pelayanan), month(surat.tgl_setor) as bln, year(surat.tgl_setor) as thn,GROUP_CONCAT(jenis_pelayanan.jenis_pelayanan) as lyn from surat left join fpk on surat.no_regis = fpk.surat_no_regis left join pelayanan on pelayanan.fpk_no_fpk = fpk.no_fpk left join jenis_pelayanan on jenis_pelayanan.id = pelayanan.jenis_pelayanan_id join user on surat.surat_username = user.username join fktp on user.idfktp_user = fktp.idfktp join cabang on cabang.kode_cabang = fktp.kode_cabang_fktp where fktp.idfktp = '$fktp' and surat.status = 1 and surat.tgl_setor between '$post[tgl_awal]' and '$post[tgl_akhir]' group by fpk.no_fpk order by surat.no_regis ASC");	
				}			
			}

		}else{
			$query = $this->db->query("select surat.no_regis as no_regis,surat.tgl_setor as tgl_setor,fpk.kasus as kasus, SUM(fpk.biaya) as bia, monthname(fpk.bulan_pelayanan) as bln2, month(surat.tgl_setor) as bln, year(surat.tgl_setor) as thn,GROUP_CONCAT(jenis_pelayanan.jenis_pelayanan) as lyn from surat left join fpk on surat.no_regis = fpk.surat_no_regis left join pelayanan on pelayanan.fpk_no_fpk = fpk.no_fpk left join jenis_pelayanan on jenis_pelayanan.id = pelayanan.jenis_pelayanan_id  where surat.status = 1 and surat.tgl_setor between '$post[tgl_awal]' and '$post[tgl_akhir]' group by surat.no_regis order by surat.tgl_setor ASC");
		}	
		return $query;
	}

}