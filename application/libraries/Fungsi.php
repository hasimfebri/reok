<?php 

Class Fungsi{
	protected $ci;
	function __construct(){
		$this->ci =& get_instance();

	}

	function user_login() {
		$this->ci->load->model('users_m');
		$user_session = $this->ci->session->userdata('username');
		$level = $this->ci->session->userdata('level');
		if($level=="2"){
			$user_data = $this->ci->users_m->getuser($user_session)->row();
			$user_data->level = "2";
			return $user_data;
		}else if($level=="1" || $level=="3"){
			$user_data = $this->ci->users_m->getadmin($user_session)->row();
			return $user_data;
		}
	}
	function periode() {
		$this->ci->load->model('basic_m');
		$level = $this->ci->session->userdata('level');
		if ($level == "2") {
			$user_session = $this->ci->session->userdata('username');			
		}else{
			$user_session = $this->ci->session->userdata('usersatker');
		}
		$rowperiode = $this->ci->basic_m->getById("periode","satker_username",$user_session,"DESC");
		return $rowperiode;
	}
	function satker() {
		$this->ci->load->model('basic_m');
		$rowsatker = $this->ci->basic_m->get("satker");
		return $rowsatker;
	}
	function pegawai() {
		$this->ci->load->model('basic_m');
		$user_session = $this->ci->session->userdata('idperiod');
		$jmlpegawai = $this->ci->basic_m->hitung("import_satker","periode_id_periode",$user_session)->row();
		return $jmlpegawai;
	}
	function satu() {
		$this->ci->load->model('basic_m');
		$user_session = $this->ci->session->userdata('idperiod');
		$jmlsatu = $this->ci->basic_m->hitung("satup","periode_id_periode",$user_session)->row();
		return $jmlsatu;
	}
	function empat() {
		$this->ci->load->model('basic_m');
		$user_session = $this->ci->session->userdata('idperiod');
		$jmlempat = $this->ci->basic_m->hitung("empatp","periode_id_periode",$user_session)->row();
		return $jmlempat;
	}
	function stats() {
		$this->ci->load->model('basic_m');
		$user_session = $this->ci->session->userdata('idperiod');
		$q = $this->ci->basic_m->stat("import_satker","periode_id_periode",$user_session);
		return $q;
	}
	function jmlsatker() {
		$this->ci->load->model('basic_m');
		$q = $this->ci->basic_m->jumlahSatker();
		return $q;
	}
	function jmlcabang() {
		$this->ci->load->model('basic_m');
		$q = $this->ci->basic_m->jumlahCabang();
		return $q;
	}
	function jmlbpjs() {
		$this->ci->load->model('basic_m');
		$q = $this->ci->basic_m->get("bpjs");
		return $q;
	}
	function totsatu() {
		$this->ci->load->model('basic_m');
		$q = $this->ci->basic_m->total("fikssatu")->row();
		return $q;
	}
	function totempat() {
		$this->ci->load->model('basic_m');
		$q = $this->ci->basic_m->total("fiksempat")->row();
		return $q;
	}
	function belumBayar() {
		$this->ci->load->model('basic_m');
		$q = $this->ci->basic_m->belumBayar()->row();
		return $q;
	}
	function belum1() {
		$this->ci->load->model('basic_m');
		$q = $this->ci->basic_m->buktiBayar1()->num_rows();
		return $q;
	}
	function belum4() {
		$this->ci->load->model('basic_m');
		$q = $this->ci->basic_m->buktiBayar4()->num_rows();
		return $q;
	}
	function pdfGenerator($html,$filename,$paper,$orientation){
		// instantiate and use the dompdf class
		$options = new Dompdf\Options();
		$options->setIsRemoteEnabled(true);
		$dompdf = new Dompdf\Dompdf($options);
		$dompdf->loadHtml($html);

		// (Optional) Setup the paper size and orientation
		$dompdf->setPaper($paper, $orientation);

		// Render the HTML as PDF
		$dompdf->render();

		// Output the generated PDF to Browser
		$dompdf->stream($filename,array('Attachment'=>0));
	}
	function countdata(){
		$this->ci->load->model('basic_m');
		$user_session = $this->ci->session->userdata('idperiod');
		$q = $this->ci->basic_m->getremark('validasi',$user_session,'periode')->num_rows();
		return $q;
	}
	function countdata1(){
		$this->ci->load->model('basic_m');
		$user_session = $this->ci->session->userdata('idperiod');
		$q = $this->ci->basic_m->getremark('satup',$user_session,'periode_id_periode')->num_rows();
		return $q;
	}
	function countdata4(){
		$this->ci->load->model('basic_m');
		$user_session = $this->ci->session->userdata('idperiod');
		$q = $this->ci->basic_m->getremark('empatp',$user_session,'periode_id_periode')->num_rows();
		return $q;
	}

}
 ?>
