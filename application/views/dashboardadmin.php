<?php $this->session->unset_userdata("page");
 ?>
                    <?php if($this->session->level == 3){ ?>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class=" row">
                            <div class="card col-lg-12">
                                <div class="stat-widget-one">
                                    <div class="stat-icon dib col-lg-2"><i class="ti-target color-danger border-danger"></i>
                                    </div>
                                    <div class="stat-content dib col-lg-6">
                                        <div class="stat-text"><b>JUMLAH CABANG</b></div>
                                        <div class="stat-digit"><?= $this->fungsi->jmlcabang()->num_rows(); ?></div>
                                    </div>
                                </div>
                            </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class=" row">
                            <div class="card col-lg-12">
                                <div class="stat-widget-one">
                                    <div class="stat-icon dib col-lg-2"><i class="ti-target color-danger border-danger"></i>
                                    </div>
                                    <div class="stat-content dib col-lg-6">
                                        <div class="stat-text"><b>JUMLAH Satker</b></div>
                                        <div class="stat-digit"><?= $this->fungsi->jmlsatker()->num_rows(); ?></div>
                                    </div>
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                    <?php }else{ ?>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class=" row">
                            <div class="card col-lg-12">
                                <div class="stat-widget-one">
                                    <div class="stat-icon dib col-lg-2"><i class="ti-target color-danger border-danger"></i>
                                    </div>
                                    <div class="stat-content dib col-lg-6">
                                        <div class="stat-text"><b>JUMLAH SATKER</b></div>
                                        <div class="stat-digit"><?= $this->fungsi->jmlsatker()->num_rows(); ?></div>
                                    </div>
                                </div>
                            </div>
                            </div>
                        </div>
                        <!-- <div class="col-lg-6">
                            <div class=" row">
                            <div class="card col-lg-12">
                                <div class="stat-widget-one">
                                    <div class="stat-icon dib col-lg-2"><i class="ti-target color-danger border-danger"></i>
                                    </div>
                                    <div class="stat-content dib col-lg-6">
                                        <div class="stat-text"><small><b>SATKER YANG BELUM INPUT DATA DIBULAN INI</b></small></div>
                                        <?php $belum = ( $this->fungsi->jmlsatker()->num_rows() - $this->fungsi->belumBayar()->jumlah) ; ?>
                                        <div class="stat-digit"><div class="stat-digit"><?= $belum ?></div></div>
                                    </div>
                                    <div class="stat-content dib col-lg-2">
                                        <div><button type="button" class="btn btn-primary" onclick="sendnotif('1')" ><i class="fa fa-paper-plane" aria-hidden="true"> Send Notif</i></button></div>
                                    </div>
                                </div>
                            </div>
                            </div>
                        </div> -->
                        <div class="col-lg-6">
                            <div class = "row">
                                <div class="card col-lg-12">
                                    <div class="stat-widget-one">
                                        <div class="stat-icon dib col-lg-2"><i class="ti-close color-danger border-danger"></i>
                                        </div>
                                        <div class="stat-content dib col-lg-6">
                                            <div class="stat-text"><small><b>YANG BELUM UPLOAD 1%</b></small></div>
                                            <?php $cabang = $this->session->level; var_dump($cabang); $belum1 = $this->fungsi->belum1(); ?>
                                            <div class="stat-digit"><div class="stat-digit"><?= $belum1 ?></div></div>
                                        </div>
                                        <div class="stat-content dib col-lg-2">
                                            <div><button type="button" class="btn btn-primary" onclick="sendnotif('2')" ><i class="fa fa-paper-plane" aria-hidden="true"> Send Notif</i></button></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-6">
                            <div class = "row">
                                <div class="card col-lg-12">
                                    <div class="stat-widget-one">
                                        <div class="stat-icon dib col-lg-2"><i class="ti-close color-danger border-danger"></i></div>
                                        <div class="stat-content dib col-lg-6">
                                             <div class="stat-text"><small><b>YANG BELUM UPLOAD 4%</b></small></div>
                                            <?php $belum4 =  $this->fungsi->belum4() ; ?>
                                            <div class="stat-digit"><div class="stat-digit"><?= $belum4 ?></div></div>
                                        </div>
                                        <div class="stat-content dib col-lg-2">
                                            <div><button type="button" class="btn btn-primary" onclick="sendnotif('3')" ><i class="fa fa-paper-plane" aria-hidden="true"> Send Notif</i></button></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-lg-6">
                            <div class="card">
                                <div class="stat-widget-one">
                                    <div class="stat-icon dib"><i class="ti-money color-success border-success"></i>
                                    </div>
                                    <div class="stat-content dib">
                                        <div class="stat-text"><b>PEMBAYARAN 1%</b></div>
                                        <?php $hasil_rupiah = "Rp " . number_format($this->fungsi->totsatu()->tot,2,',','.'); ?>
                                        <div class="stat-digit"><?= $hasil_rupiah ?></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="card">
                                <div class="stat-widget-one">
                                    <div class="stat-icon dib"><i class="ti-money color-success border-success"></i></div>
                                    <div class="stat-content dib">
                                        <div class="stat-text"><b>PEMBAYARAN 4%</b></div>
                                        <?php $hasil_4 = "Rp " . number_format($this->fungsi->totempat()->tot ,2,',','.'); ?>
                                        <div class="stat-digit"><div class="stat-digit"><?= $hasil_4 ?></div></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>  
                <?php }?>               
                <script type="text/javascript">
                    var bln = [];
                    var hsl = [];
                    
                    function sendnotif(e){
                        // console.log(e);
                            $.post("<?= site_url('Dashboard/sendnotif') ?>",{jenis:e},function(data){
                                    alert(data.message);
                            },"json");
                    }
                </script>