<?php $tdatae= strtotime($tgl); $tanggal = date('d-m-Y', $tdatae) ?>
              <style type="text/css">
.tftable {font-size:12px;color:#000000;width:100%;border-width: 1px;border-color: #729ea5;border-collapse: collapse;}
.tftable th {font-size:13px;border-width: 1px;padding: 3px;border-style: solid;border-color: #000000;text-align:center;}
.tftable tr {background-color:#fff;}
.tftable td {font-size:12px;border-width: 1px;padding: 3px;border-style: solid;border-color: #000000;}
.tftable tr:hover {background-color:#ffffff;}
</style>
                <div style="clear: both;"></div>
                <!-- <center>BERITA ACARA REKONSILIASI <br> JUMLAH PEKERJA DAN BESARAN IURAN JAMINAN KESEHATAN NASIONAL ANTARA BPJS KESEHATAN KANTOR CABANG MAGELANG TAHUN 2020 DENGAN SLB NEGERI TEMANGGUNG</h4></center><br><br> -->
                <div style="text-align: center;"><font size="2"><b>BERITA ACARA REKONSILIASI<br> Iuran Jaminan Kesehatan Peserta Pekerja Penerima Upah (PPU) Pemerintah Daerah <br>Antara <?= $satker ?> dan BPJS Kesehatan Kantor Cabang Magelang <br>Nomor: …../……./…..<br>Nomor: …../……./…..  </b></font><!-- </center> --></div>
              <p style="margin-right:10%;margin-left:5%;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              Pada hari ini tanggal <?= $tgl ?> bertempat di <?= $kota ?> telah dilakukan Rekonsiliasi antara <font color="RED"><?= $satker ?></font> dan BPJS Kesehatan Cabang Magelang dengan hasil sebagai berikut :</p> <br>
              <table class="tftable" border="1">
                <tr style='background-color: #BFBFBF'><th rowspan="2" style="width:3%">No</th><th rowspan="2">Bulan</th><th >Iuran 1%</th><th>Iuran 4%</th></tr>
                <tr style="background-color: #BFBFBF"><td align="center">PPNPN</td><td align="center">PPNPN</td></tr>
                    <tr style='background-color: #8EA9DB'>
                      <td align="center">1</td>
                      <td align="center">2</td>
                      <td align="center">3</td>
                      <td align="center">4</td>
                    </tr>
                    <?php $bulan = ["Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember"]; $t="";?>
                    <?php $satupersensat = [0]; $empatpersensat = [0]; ?>
                  <?php $no=1;for($i=0; $i < count($bulan); $i++){ 
                  $iu1 = 0; $iu4 = 0; ?>
                  <tr>
                    <td><?= $no ?></td>
                    <td><?= $bulan[$i] ?></td>
                    <?php $a=0; foreach ($row->result() as $key => $value) {?>
                      <?php if ((int)date('m', strtotime($value->bulan)) == (int)$no) { ?>
                        <td><?php echo "Rp " . number_format($value->iuran_1, 2, ",", "."); ?></td>
                        <td><?php echo "Rp " . number_format($value->iuran_4, 2, ",", "."); ?></td>
                        <?php $iu1 += $value->iuran_1; $iu4 += $value->iuran_4?>
                        <?php $a=1 ?>
                      <?php }else{ ?>
                        <td>0</td>
                        <td>0</td>
                      <?php } ?>
                     <?php } ?>
                  </tr>
                  <?php $no++ ?>
                  <?php } ?>
                    <tr style="background-color: #D9E1F2"><th colspan="2" >Total </th> <td><?php echo "Rp " . number_format($iu1, 2, ",", ".");  ?></td><td><?php echo "Rp " . number_format($iu4, 2, ",", "."); ?></td></tr>
                </table>
<br>
              <br>
            <!-- <p style="margin-right:10%;margin-left:5%;">-->
            <!--  *) Berdasarkan hasil perhitungan tersebut diatas,Setoran dinyatakan <u><b>BELUM LUNAS</b></u> dan kekurangan setoran iuran jaminan kesehatan akan dibayarkan oleh Pemerintah Prov/Kab/Kota <?= $kota ?>.  maksimal pada tanggal 10, bulan berikutnya <br>-->
            <!-- *) Berdasarkan hasil perhitungan tersebut diatas, Setoran dinyatakan <u><b>LUNAS</b></u> dan kelebihan setoran iuran jaminan kesehatan akan dikompensasikan untuk iuran bulan berikutnya-->
            <!--    *) Berdasarkan hasil perhitungan tersebut diatas, Setoran dinyatakan <u><b>LUNAS</b></u>.-->
            <!--</p>-->
            <p style="margin-right:10%;margin-left:5%;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              Rincian perhitungan iuran jaminan kesehatan tersaji dalam lampiran Berita Acara yang merupakan bagian yang tidak terpisahkan dari Berita Acara Hasil Rekonsiliasi ini. <br><br>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Apabila dikemudian hari terhadap hasil rekonsiliasi masih terdapat selisih perhitungan sebagai akibat perbedaan data, berita acara yang telah ditetapkan akan dilakukan perbaikan sebagaimana mestinya.<br><br>
              Demikian Berita Acara Rekonsiliasi ini dibuat dengan sebenar-benarnya dan untuk dipergunakan sebagaimana mestinya.</p>
              <br>
              <div style="float:right;margin-right:10%;text-align:center;"><font size="2"><?= $kota ?>, <?= $tanggal ?></font></div><br><br>

              <div style="float:left;margin-left:10%;text-align:center;"><font size="2">PIHAK PERTAMA <br>
             BPJS KESEHATAN KC MAGELANG
              </font><br><br>
              <img src="<?php echo $imglogo2 ?>" alt="logo" width="15%">
              </div>
              <div style="float:right;margin-right:10%;text-align:center;"><font size="2">PIHAK KEDUA<br>
              <b><?= $satker ?></b></font>
              <br><br>
              <img src="<?php echo $imgsatker2 ?>" alt="logo" width="15%">
              </div>
              
