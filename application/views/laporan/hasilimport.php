 <style type="text/css">
.tftable {font-size:12px;color:#000000;width:100%;border-width: 1px;border-color: #000000;border-collapse: collapse;}
.tftable th {font-size:12px;border-width: 1px;padding: 8px;border-style: solid;border-color: #000000;text-align:left;}
.tftable tr {background-color:#fff;}
.tftable td {font-size:12px;border-width: 1px;padding: 8px;border-style: solid;border-color: #000000;}
.tftable tr:hover {background-color:#ffffff;}
.tftable div {background-color: red; background:red }
.str{mso-number-format:\@;
  }
  .num{mso-text-format:\@;
  }
</style>
<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=hasilkertaskerja".$of.$satker.$bulan.".xls");
?>              
                
               
               <br>
                <div style="clear: both;"></div>
                <center><h4 >KERTAS KERJA PERHITUNGAN IURAN JAMINAN KESEHATAN PPNPN PEMERINTAH PROVINSI/KABUPATEN/KOTA...</h4></center>
                <div style="float:left; background-color:#00ffff;"><font size="2"><b>Bulan : <?=$bulan ?></b>&nbsp;&nbsp;&nbsp;</font></div>
              <table class="tftable" border="1">
              <tr style="background-color: #1e90ff"><th>No.</th>
                <th>Nama Satker</th>
                <th>Nama Pegawai</th>
                <th>NIK (Nomor Induk Kependudukan)</th>
                <th>NIP (Nomor Induk Pegawai)</th>
                <th>Jumlah Istri/Suami</th>
                <th>Jumlah Anak</th>
                <th>Penghasilan Tetap (Minimal UMK/UMP)</th>
                <th>Tunjangan lainya (jika ada)</th>
                <th>Total Take Home Pay (THP)</th>
                <th>Dasar Penghitungan Iuran (DPI)</th>
                <th>Iuaran 1%</th>
                <th>Iuaran $4%</th>
                <th>Total Iuran 5%</th>
              </tr>
                  <tr>
                    <th>1</th>
                    <th>2</th>
                    <th>3</th>
                    <th>4</th>
                    <th>5</th>
                    <th>6</th>
                    <th>7</th>
                    <th>8</th>
                    <th>9</th>
                    <th>10</th>
                    <th align="center">11</th>
                    <th>12</th>
                    <th>13=6-11</th>
                    <th>14=7-12</th>
                  </tr>
                  <?php $no=1;$cons = 6; foreach ($row->result() as $key => $value) {?>
                  <tr>
                    <td><?= $no++ ?></td>
                    <td><?= $value->nama_satker ?></td>
                    <td><?= $value->nama_pegawai ?></td>
                    <td class="str"><?= $value->nik ?></td>
                    <td class="str"><?= $value->nip ?></td>
                    <td><?= $value->jml_is ?></td>
                    <td><?= $value->jml_an ?></td>
                    <td><?= $value->penghasilan ?></td>
                    <td><?= $value->tunjangan ?></td>
                    <td style="background-color:#ffd700"><?= "=H".$cons."+I".$cons ?></td>
                    <td style="background-color:#ffd700"><?= "=IF(J".$cons.">12000000;12000000;J".$cons.")" ?></td>
                    <td style="background-color:#ffd700"><?= "=ROUND(K".$cons."*1%;0)" ?></td>
                    <td style="background-color:#ffd700"><?= "=ROUND(K".$cons."*4%;0)" ?></td>
                    <td style="background-color:#ffd700"><?= "=L".$cons."+M".$cons ?></td>
                  </tr>
                  <?php $cons++ ?>
                  <?php } ?>
                  <?php $x = $no ?>
                  <?php for($i=$x; $i <= 12; $i++){?>
                   <tr>
                    <td><?= $no++ ?></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td style="background-color:#ffd700"><?= "=H".$cons."+I".$cons ?></td>
                    <td style="background-color:#ffd700"><?= "=IF(J".$cons.">12000000;12000000;J".$cons.")" ?></td>
                    <td style="background-color:#ffd700"><?= "=ROUND(K".$cons."*1%;0)" ?></td>
                    <td style="background-color:#ffd700"><?= "=ROUND(K".$cons."*4%;0)" ?></td>
                    <td style="background-color:#ffd700"><?= "=L".$cons."+M".$cons ?></td>
                  </tr>
                  <?php $cons++ ?>
                  <?php } ?>
                  <tr style="background-color: #ffd700">
                    <td colspan="3">Total </td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td><?= "=SUM(J6:J17)" ?></td>
                    <td><?= "=SUM(K6:K17)" ?></td>
                    <td><?= "=SUM(L6:L17)" ?></td>
                    <td><?= "=SUM(M6:M17)" ?></td>
                    <td><?= "=SUM(N6:N17)" ?></td>
                  </tr>
                </table>
                  <div style="float:left; background: red;"><font size="2"><b>Catatan: Mohon untuk tidak menginsert kolom ataupun mengubah rumus pada kolom yang berwarna orange. Silahkan menginsert row untuk menambahkan rincian pegawai</b></font></div>
                  
                     