 <style type="text/css">
.tftable {font-size:12px;color:#000000;width:100%;border-width: 1px;border-color: #000000;border-collapse: collapse;}
.tftable th {font-size:12px;border-width: 1px;padding: 8px;border-style: solid;border-color: #000000;text-align:left;}
.tftable tr {background-color:#fff;}
.tftable td {font-size:12px;border-width: 1px;padding: 8px;border-style: solid;border-color: #000000;}
.tftable tr:hover {background-color:#ffffff;}
.tftable div {background-color: red; background:red }
.str{mso-number-format:\@;
  }
  .num{mso-text-format:\@;
  }
</style>
<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=hasilkertaskerja".$satker.$bulan.".xls");
$this->load->library('encryption');
?>              
                
               
               <br>
                <div style="clear: both;"></div>
                <center><h4 >DATA PENGAJUAN SATKER <?=$satker ?></h4></center>
                <div style="float:left; background-color:#00ffff;"><font size="2"><b>Bulan : <?=$bulan ?></b>&nbsp;&nbsp;&nbsp;</font></div>
              <table class="tftable" border="1">
              <tr>
			    <th>#</th>
				<th>Jenis</th>
                <th>nama_satker</th>
                <th>nama_pegawai</th>
                <th>nik</th>
                <th>nip</th>
                <th>jml_is</th>
                <th>jml_an</th>
                <th>penghasilan</th>
                <th>tunjangan</th>
                <th>thp</th>
                <th>dpi</th>
                <th>iuran_1</th>
                <th>iuran_4</th>
                <th>total_i</th>
                <th>bulan</th>
            </tr>
                  <?php $no=1; foreach ($row->result() as $value => $key) {?>
                  <tr>
			<td><?= $no++?></td>
            <td><?= (($key->jenis == 1) ? 	'Pengajuan' :'Mutasi') ?></td>
			<td><?= $key->nama_satker  ?></td>
            <td><?= $key->nama_pegawai  ?></td>
            <td><?= (int)$this->encryption->decrypt($key->nik)?></td>
            <td><?= $key->nip  ?></td>
            <td><?= $key->jml_is  ?></td>
            <td><?= $key->jml_an  ?></td>
            <td> Rp. <?= number_format($key->penghasilan,2,",",".")  ?></td>
            <td> Rp. <?= number_format($key->tunjangan,2,",",".")  ?></td>
            <td> Rp. <?= number_format($key->thp,2,",",".")  ?></td>
            <td> Rp. <?= number_format($key->dpi,2,",",".")  ?></td>
            <td> Rp. <?= number_format($key->iuran_1,2,",",".")  ?></td>
            <td> Rp. <?= number_format($key->iuran_4,2,",",".")  ?></td>
            <td> Rp. <?= number_format($key->total_i,2,",",".")  ?></td>
            <td><?= $key->bulan ?></td>
            
            </tr>
            <?php } ?>
                </table>
                  
                     