<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Focus Admin: Widget</title>

    <!-- ================= Favicon ================== -->
    <!-- Standard -->
    <link rel="shortcut icon" href="http://placehold.it/64.png/000/fff">
    <!-- Retina iPad Touch Icon-->
    <link rel="apple-touch-icon" sizes="144x144" href="http://placehold.it/144.png/000/fff">
    <!-- Retina iPhone Touch Icon-->
    <link rel="apple-touch-icon" sizes="114x114" href="http://placehold.it/114.png/000/fff">
    <!-- Standard iPad Touch Icon-->
    <link rel="apple-touch-icon" sizes="72x72" href="http://placehold.it/72.png/000/fff">
    <!-- Standard iPhone Touch Icon-->
    <link rel="apple-touch-icon" sizes="57x57" href="http://placehold.it/57.png/000/fff">

    <!-- Styles -->
    <link href="<?php echo base_url()?>assets/css/lib/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo base_url()?>assets/css/lib/themify-icons.css" rel="stylesheet">
    <link href="<?php echo base_url()?>assets/css/lib/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url()?>assets/css/lib/helper.css" rel="stylesheet">
    <link href="<?php echo base_url()?>assets/css/style.css" rel="stylesheet">
</head>

<body class="bg-primary">

    <div class="unix-login">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                  <div class="card">
                  <h4 align="center">SUDAH MELAKUKAN REKON</h4>
                  <div class="text-center">
                  <img src="<?= base_url('uploads/logo/').$image ?>" width="300">
                </div>
                </div>
                </div>
            </div>
        </div>
    </div>

</body>

</html>