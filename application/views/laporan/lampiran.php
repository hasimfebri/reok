<?php $tdatae= strtotime($tgl); $tanggal = date('d/m/Y', $tdatae) ?>
              <style type="text/css">
.tftable {font-size:12px;color:#000000;width:100%;border-width: 1px;border-color: #000000;border-collapse: collapse;}
.tftable th {font-size:12px;border-width: 1px;padding: 8px;border-style: solid;border-color: #000000;text-align:left;}
.tftable tr {background-color:#fff;}
.tftable td {font-size:12px;border-width: 1px;padding: 8px;border-style: solid;border-color: #000000;}
.tftable tr:hover {background-color:#ffffff;}
</style>
<?php

header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=Lampiran_REOK_".$satker."_".$tahun.".xls");
?>              
               <br>
                <div style="clear: both;"></div>
                <center><h4 >BERITA ACARA REKONSILIASI JUMLAH PEKERJA DAN BESARAN IURAN JAMINAN KESEHATAN NASIONAL ANTARA BPJS KESEHATAN KANTOR CABANG MAGELANG TAHUN <?=$tahun ?> DENGAN <?= $satker ?></h4></center><br>
                <div style="float:right"><font size="1"><b>NOMOR BERITA ACARA :</b></font></div>
              <table class="tftable" border="1">
              <tr><th rowspan="2">No</th><th rowspan="2">Bulan</th><th colspan="5" align="center">Data Satker</th><th colspan="5" align="center">Data BPJS Kesehatan</th><th></th><th></th></tr>
                  <tr>
                    <th>Pekerja</th>
                    <th>Tagihan iuran 1%</th>
                    <th>Tagihan iuran 4%</th>
                    <th>iuran dibayar 1%</th>
                    <th>iuran dibayar 4%</th>
                    <th>Pekerja</th>
                    <th>Tagihan iuran 1%</th>
                    <th>Tagihan iuran 4%</th>
                    <th>iuran dibayar 1%</th>
                    <th>iuran dibayar 4%</th>
                    <th>lebih/kurang(bayar) 1%</th>
                    <th>lebih/kurang(bayar) 4%</th>
                  </tr>
                  <tr>
                    <th>1</th>
                    <th>2</th>
                    <th>3</th>
                    <th>4</th>
                    <th>5</th>
                    <th>6</th>
                    <th>7</th>
                    <th>8</th>
                    <th>9</th>
                    <th>10</th>
                    <th align="center">11</th>
                    <th>12</th>
                    <th>13=6-11</th>
                    <th>14=7-12</th>
                  </tr>
                  <tr>
                    <th colspan="14">Tahun : <?=$tahun ?> </th>
                  </tr>
                  <?php $bulan = ["Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember"] ?>
                  <?php $no=1;$constanta = 9;for($i=0; $i < count($bulan); $i++){?>
                  <tr>
                    <td><?= $no ?></td>
                    <td><?= $bulan[$i] ?></td>
                    <?php $a=0; foreach ($row->result() as $key => $value) {?>
                      <?php if ($value->angka == $no) { ?>
                        <td><?php echo $value->pgw ?></td>
                        <td><?php echo $value->satu ?></td>
                        <td><?php echo $value->empat ?></td>
                        <?php $a=1 ?>
                      <?php } ?>
                    <?php } ?>
                    <?php if ($a!=1) {?>
                        <td></td><td></td><td></td>
                    <?php } ?>
                    <?php $b=0; foreach ($rowsatu->result() as $key => $valsat) {?>
                      <?php if ($valsat->angka == $no) { ?>
                        <td><?php echo $valsat->nom ?></td>
                        <?php $b=1 ?>
                      <?php } ?>
                    <?php } ?>
                    <?php if ($b!=1) {?>
                        <td></td>
                    <?php } ?>
                    <?php $c=0; foreach ($rowempat->result() as $key => $valemp) {?>
                      <?php if ($valemp->angka == $no) { ?>
                        <td><?php echo $valemp->nom ?></td>
                        <?php $c=1 ?>
                      <?php } ?>
                    <?php } ?>
                    <?php if ($c!=1) {?>
                        <td></td>
                    <?php } ?>
                    <?php $d=0; foreach ($rowbpjs->result() as $key => $bpjs) {?>
                      <?php if ($bpjs->angka == $no) { ?>
                        <td><?php echo $bpjs->pgw ?></td>
                        <td><?php echo $bpjs->satu ?></td>
                        <td><?php echo $bpjs->empat ?></td>
                        <?php $d=1 ?>
                      <?php } ?>
                    <?php } ?>
                    <?php if ($d!=1) {?>
                        <td></td><td></td><td></td>
                    <?php } ?>
                    <?php $e=0; foreach ($rowfikssatu->result() as $key => $fs) {?>
                      <?php if ($fs->angka == $no) { ?>
                        <td><?php echo $fs->nom ?></td>
                        <?php $e=1 ?>
                      <?php } ?>
                    <?php } ?>
                    <?php if ($e!=1) {?>
                        <td></td>
                    <?php } ?>
                    <?php $f=0; foreach ($rowfiksempat->result() as $key => $fe) {?>
                      <?php if ($fe->angka == $no) { ?>
                        <td><?php echo $fe->nom ?></td>
                        <?php $f=1 ?>
                      <?php } ?>
                    <?php } ?>
                    <?php if ($f!=1) {?>
                        <td></td>
                    <?php } ?>
                    <td><?= "=I".$constanta."-K".$constanta ?></td>
                    <td><?= "=J".$constanta."-L".$constanta ?></td>
                  </tr>
                  <?php $no++;$constanta++; ?>
                  <?php } ?><tr>
                      <td colspan="2"><b>Total</b></td><td><?= "=SUM(C9:C20)" ?></td><td><?= "=SUM(D9:D20)" ?></td><td><?= "=SUM(E9:E20)" ?></td><td><?= "=SUM(F9:F20)" ?></td><td><?= "=SUM(G9:G20)" ?></td><td><?= "=SUM(H9:H20)" ?></td><td><?= "=SUM(I9:I20)" ?></td><td><?= "=SUM(J9:J20)" ?></td><td><?= "=SUM(K9:K20)" ?></td><td><?= "=SUM(L9:L20)" ?></td><td><?= "=SUM(M9:M20)" ?></td><td><?= "=SUM(N9:N20)" ?></td>
                    </tr>
              </table>
              <br>
              
              <br><br>
              <table>
                <tr>
                <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td colspan="3" align="center"> <?=$kota ?>, <?=$tanggal ?> </td>
                </tr>
                <tr>
                  <td colspan="5" align="center"> PIHAK PERTAMA </td><td></td><td></td><td></td><td></td><td></td><td colspan="3" align="center"> PIHAK KEDUA</td>
                </tr>
                 <tr>
                  <td colspan="5" align="center">  BPJS KESEHATAN KC MAGELANG </td><td></td><td></td><td></td><td></td><td></td><td colspan="3" align="center"> Bendahara</td>
                </tr>
                 <tr>
                  <td colspan="5" align="center"></td><td></td><td></td><td></td><td></td><td></td><td colspan="3" align="center"></td>
                </tr>
                 <tr>
                  <td colspan="5" align="center"></td><td></td><td></td><td></td><td></td><td></td><td colspan="3" align="center"></td>
                </tr>
                 <tr>
                  <td colspan="5" align="center">Nama....</td><td></td><td></td><td></td><td></td><td></td><td colspan="3" align="center">Nama......</td>
                </tr>                                
              </table>

              <div style="clear: both;"></div>
              <br>
              </div>

              <div style="clear: both;"></div>