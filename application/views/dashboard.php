<?php $this->session->unset_userdata("page");
 ?>
<div class="row">
                        <div class="col-lg-3">
                            <div class="card">
                                <div class="stat-widget-one">
                                    <div class="stat-icon dib"><i class="ti-calendar color-danger border-primary"></i>
                                    </div>
                                    <div class="stat-content dib">
                                        <div class="stat-text">Periode</div>
                                        <div class="stat-digit"><?= $this->session->tahun?></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="card">
                                <div class="stat-widget-one">
                                    <div class="stat-icon dib"><i class="ti-user color-primary border-primary"></i>
                                    </div>
                                    <div class="stat-content dib">
                                        <div class="stat-text">Pegawai</div>
                                        <div class="stat-digit"><?= $this->fungsi->pegawai()->jml; ?></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="card">
                                <div class="stat-widget-one">
                                    <div class="stat-icon dib"><i class="ti-money color-success border-success"></i>
                                    </div>
                                    <div class="stat-content dib">
                                        <div class="stat-text">Bukti Bayar 1%</div>
                                        <div class="stat-digit"><?= $this->fungsi->satu()->jml; ?></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="card">
                                <div class="stat-widget-one">
                                    <div class="stat-icon dib"><i class="ti-money color-warning border-warning"></i></div>
                                    <div class="stat-content dib">
                                        <div class="stat-text">Bukti Bayar 4%</div>
                                        <div class="stat-digit"><?= $this->fungsi->empat()->jml; ?></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                            <div id="accordion">
                              <div class="card">
                                <div class="card-header" id="headingOne">
                                  <h5 class="mb-0">
                                    <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                      Data Pelaporan
                                    </button>
                                  </h5>
                                </div>

                                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                  <div class="card-body"><br>
                                     <table id="example2" class="display responsive nowrap" style="width:100%">
                                        <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Bulan</th>
                                                    <th>Pekerja</th>
                                                    <th>Tagihan iuran 1%</th>
                                                    <th>Tagihan iuran 4%</th>
                                                    <th>iuran dibayar 1%</th>
                                                    <th>iuran dibayar 4%</th>
                                                    <th>lebih/kurang(bayar) 1%</th>
                                                    <th>lebih/kurang(bayar) 4%</th>
                                                  </tr>
                                                  
                                        </thead>
                                        <tbody>
                                            <?php $bulan = ["Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember"];
                                            $i1s = [0];
                                            $i1b = [0]; 
                                            $i4s = [0];
                                            $i4b = [0]; ?>
																										<tr>
																										<?php $no=1; foreach ($data->result() as $key => $value) {?>
																											<tr>
																														<td><?= $no ?></td>
                                                    				<td><?= $bulan[(int)$value->bulan - 1] ?></td>
																														<td><?= $value->jumlah_pegawai ?></td>
																														<td><?= "Rp " . number_format($value->iuran_1, 2, ",", ".") ?></td>
																														<td><?= "Rp " . number_format($value->iuran_4, 2, ",", ".") ?></td>
																														<td><?= "Rp " . number_format($value->actual_iuran_1, 2, ",", ".") ?></td>
																														<td><?= "Rp " . number_format($value->actual_iuran_4, 2, ",", ".") ?></td>
																														<td><?= "Rp " . number_format($value->kurang1, 2, ",", ".") ?></td>
																														<td><?= "Rp " . number_format($value->kurang4, 2, ",", ".") ?></td>
																										</tr>
																														
																									<?php $no++;?>
																										<?php } ?>
																										
																									</tr>
                                        </tbody>
                                    </table>
                                    
                                  </div>
                                </div>
                              </div>
                                    
                <script type="text/javascript">
                    var bln = [];
                    var hsl = [];
                </script>
                <?php $dt = $this->fungsi->stats(); ?>
               <?php foreach ($dt->result() as $key => $ch) {?>
               <script type="text/javascript">
                    hsl.push("<?=$ch->jml?>");
                    bln.push("<?=$ch->bulans?>");
               </script>
                    <?php } ?>
