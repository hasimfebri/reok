<?php $this->session->unset_userdata("page");
$params = array(
                        'page' => "Ganti Password"
                    );
        $this->session->set_userdata($params);
 ?>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                               <form role="form" action="" method="post">
                                <div class="form-group">
                                    <label>Password Lama</label>
                                    <input type="password" class="form-control" value="<?=set_value('passwordlama')?>" name="passwordlama">
                                    <?= form_error('passwordlama') ?>
                                </div>
                                <div class="form-group">
                                    <label>Password Baru</label>
                                    <input type="password" class="form-control" value="<?=set_value('passwordbaru')?>" name="passwordbaru">
                                    <?= form_error('passwordbaru') ?>
                                </div>                              
                                <div class="form-group">
                                    <label>Password Konfirmasi</label>
                                    <input type="password" class="form-control" value="<?=set_value('passconf')?>" name="passconf">
                                    <?= form_error('passconf') ?>
                                </div>                      
                                        <button type="submit" class="btn btn-primary">Submit Button</button>
                                    <button type="reset" class="btn btn-default">Reset Button</button>
                                </form>         
                            </div>
                        </div>
                    </div>                    
                    