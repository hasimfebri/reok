<?php $this->session->unset_userdata("page");
$params = array(
                        'page' => "Cek Upload Bukti Bayar"
                    );
        $this->session->set_userdata($params);
 ?>
            <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <?php echo $this->session->flashdata('notif') ?>
                    <h4>
                        
                            <form role="form" action="" method="post">
                                <div class="row col-md-12">
                                <div class="col-md-4">
                                    <div class="form-group">
                                            <label>DARI BULAN : </label>
                                        <div class='input-group date'>
                                            <input type="text" class="form-control" value='<?=set_value('fromdate')?>' name="fromdate"  id='datepicker' autocomplete="off">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">    
                                    <div class="form-group">
                                        <label>SAMPAI BULAN : </label>
                                        <div class='input-group date'>
                                            <input type="text" class="form-control" value='<?=set_value('todate')?>' name="todate"  id='datepicker2' autocomplete="off">
                                        </div>
                                    </div>
                                </div>
                                <div style="padding-top: 5%;">
                                        <button type="submit" name='cari' class="btn btn-primary">Cari</button>
                                        <button type="button" class="btn" onclick="reset()">Refresh</button>
                                </div>
                            </form>
                        </div>
                        <!-- <a href="<?= site_url('satker/add'); ?>" class="btn btn-success">Tambah Data Satker</a> -->
                    </h4><div class="row col-lg-12">
                            <div class="col-lg-6" id="bukti1">
                                 <div class="card">
                                <table id="example" class="display responsive nowrap" style="width:100%;">
                                    <thead>
                                        <tr>
                                            <th style="max-width: 10px;">No</th>
                                            <th style="max-width: 8px;">cek</th>
                                            <th>Nama Satker</th>
                                            <th>Nama Kepala</th>
                                            <th>Nama Admin</th>
                                            <th>No HP Satker</th>
                                            <th>No HP Admin</th>
                                            <th>Kota</th>
                                            <th>Alamat</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $no=1; foreach ($row1->result() as $key => $value) {?>
                                        <tr>
                                            <td style="max-width: 10px;"><?= $no++ ?></td>
                                            <td style="max-width: 8px;"><small><span>
                    <button type="button" onclick="modal1('<?= $value->username ?>')" class="fa fa-eye" style="font-size:10px"></button>
                </span></small></td>
                                            <td><small><?= $value->nama ?></small></td>
                                            <td><?= $value->nama_kepala ?></td>
                                            <td><?= $value->nama_admin ?></td>
                                            <td><?= $value->no_hp_satker ?></td>
                                            <td><?= $value->no_hp_admin ?></td>
                                            <td><?= $value->kota ?></td>
                                            <td><?= $value->alamat ?></td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table> 
                                </div>           
                            </div>
                            <div class="col-lg-6" id="bukti4">
                                 <div class="card">
                                <table id="example2" class="display responsive nowrap" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th style="max-width: 10px;">No</th>
                                            <th style="max-width: 8px;">Cek</th>
                                            <th>Nama Satker</th>
                                            <th>Nama Kepala</th>
                                            <th>Nama Admin</th>
                                            <th>No HP Satker</th>
                                            <th>No HP Admin</th>
                                            <th>Kota</th>
                                            <th>Alamat</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $no=1; foreach ($row4->result() as $key => $value) {?>
                                        <tr>
                                            <td style="max-width: 10px;"><?= $no++ ?></td>
                                            <td style="max-width: 8px;"><small><span>
                    <button type="button" onclick="modal4('<?= $value->username ?>')"  class="fa fa-eye" style="font-size:10px"></button>
                </span></small></td>
                                            <td ><small><?= $value->nama ?></small></td>
                                            <td ><?= $value->nama_kepala ?></td>
                                            <td><small><?= $value->nama_admin ?></small></td>
                                            <td><?= $value->no_hp_satker ?></td>
                                            <td><?= $value->no_hp_admin ?></td>
                                            <td><?= $value->kota ?></td>
                                            <td><?= $value->alamat ?></td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                                 </div>           
                            </div>
                        </div>
                        </div>
                        </div>
                    </div>
                    <div class="modal fade" id="modal-ket1" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h5 class="modal-title">Detail Satker UPLOAD BUKTI BAYAR 1%</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <div class="modal-body" id='body-modal1'>
                            <table id="example3" class="display responsive nowrap" style='color:black;font-size:100%;width: 100%;'>
                                <thead>
                                <th style='text-align:center; vertical-align:middle'>Satker</th>
                                <th style='text-align:center; vertical-align:middle'>Bulan</th>
                                </thead>
                                <tbody id='dataya1'>
                                    
                                </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="modal fade" id="modal-ket4" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h5 class="modal-title">Detail Satker UPLOAD BUKTI BAYAR 4%</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <div class="modal-body" id='body-modal4'>
                            <table id="example3" class="display responsive nowrap" style='color:black;font-size:100%;width: 100%;'>
                                <thead>
                                <th style='text-align:center; vertical-align:middle'>Satker</th>
                                <th style='text-align:center; vertical-align:middle'>Bulan</th>
                                </thead>
                                <tbody id='dataya4'>
                                    
                                </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                    </div>


<script type="text/javascript">
    function reset() {
        $('#datetimepicker').val('');
        $('#datetimepicker2').val('');
    }
    function modal1(e){
        $('#dataya1 tr').remove();
        content = '';
        var date = $('#datepicker2').val();
       $.post( "BelumBuktiBayar/cekUpload1", { id:e,date:date}, function( data ) {
                // console.log(data.length);
                for(x = 0; x < data.length ; x++){
                content +=`<tr >`;
                content +=`<td style='text-align:center; vertical-align:middle' >`+data[x].nama+`</td>`; 
                content +=`<td style='text-align:center; vertical-align:middle'>`+data[x].bulan1+`</td>`;
                content +=`</tr>`;
                }
                // console.log(content);
                $('#dataya1').append(content);
                $('#modal-ket1').modal('show');
            }, "json");
       // $('#modal-ket').modal('show');
    }
    function modal4(e){
        $('#dataya4 tr').remove();
        content = '';
        var date = $('#datepicker2').val();
       $.post( "BelumBuktiBayar/cekUpload4", { id:e,date:date}, function( data ) {
                // console.log(data.length);
                for(x = 0; x < data.length ; x++){
                content +=`<tr >`;
                content +=`<td style='text-align:center; vertical-align:middle' >`+data[x].nama+`</td>`; 
                content +=`<td style='text-align:center; vertical-align:middle'>`+data[x].bulan1+`</td>`;
                content +=`</tr>`;
                }
                // console.log(content);
                $('#dataya4').append(content);
                $('#modal-ket4').modal('show');
            }, "json");
       // $('#modal-ket').modal('show');
    }
</script>