<?php $this->session->unset_userdata("page");
$params = array(
                        'page' => "Bukti Bayar"
                    );
        $this->session->set_userdata($params);
 ?>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="card">
                                <div class="card-title">
                                    <h4>Input 1%</h4>
                                </div>
                                <hr>
                                <?php echo $this->session->flashdata('notif') ?>
                    <form method="POST" action="<?= site_url('BuktiBayar/uploadsat') ?>" enctype="multipart/form-data">                                 
                <div class="form-group">
                    <label>Pilih Bulan</label>
                    <select name="bulan1" class="form-control" required>
                        <option ></option>
                        <option value="01">Januari</option>
                        <option value="02">Februari</option>
                        <option value="03">Maret</option>
                        <option value="04">April</option>
                        <option value="05">Mei</option>
                        <option value="06">Juni</option>
                        <option value="07">Juli</option>
                        <option value="08">Agustus</option>
                        <option value="09">September</option>
                        <option value="10">Oktober</option>
                        <option value="11">November</option>
                        <option value="12">Desember</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>Tahun</label>
                    <input type="hidden" name="tahun1" value="<?= $this->session->tahun ?>">
                    <input class="form-control" type="text" value="<?= $this->session->tahun ?>" readonly="">
                </div>
                <div class="form-group" id="sandbox-container">
                    <label>Tanggal Bayar</label>
                    <input type="text" class="form-control" name="tgl_byr1">
                </div> 
                <div class="form-group">
                    <label>Nominal</label>
                    <input type="number" class="form-control" name="nominal1">
                </div>
                <?php if ($this->session->level == "2") { ?>
                <div class="form-group">
                <label for="exampleInputEmail1">UNGGAH BUKTI BAYAR (Pdf/Gambar)</label>
                <input type="file" name="userfile1" class="form-control" required>
              </div>
              <?php } ?>

              <button type="submit" class="btn btn-success">UPLOAD</button>
            </form>          
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="card">
                                <div class="card-title">
                                    <h4>Input 4%</h4>
                                    
                                </div>
                                <hr>
                                <?php echo $this->session->flashdata('notif') ?>
                    <form method="POST" action="<?= site_url('BuktiBayar/uploademp') ?>" enctype="multipart/form-data">                                
                <div class="form-group">
                    <label>Pilih Bulan</label>
                    <select name="bulan2" class="form-control" required>
                        <option ></option>
                        <option value="01">Januari</option>
                        <option value="02">Februari</option>
                        <option value="03">Maret</option>
                        <option value="04">April</option>
                        <option value="05">Mei</option>
                        <option value="06">Juni</option>
                        <option value="07">Juli</option>
                        <option value="08">Agustus</option>
                        <option value="09">September</option>
                        <option value="10">Oktober</option>
                        <option value="11">November</option>
                        <option value="12">Desember</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>Tahun</label>
                    <input type="hidden" name="tahun2" value="<?= $this->session->tahun ?>">
                    <input class="form-control" type="text" value="<?= $this->session->tahun ?>" readonly="">
                </div>
                <div class="form-group" id="sandbox-container">
                    <label>Tanggal Bayar</label>
                    <input type="text" class="form-control" name="tgl_byr2">
                </div> 
                <div class="form-group">
                    <label>Nominal</label>
                    <input type="number" class="form-control" name="nominal2">
                </div>
                <?php if ($this->session->level == "2") { ?>
                <div class="form-group">
                <label for="exampleInputEmail1">UNGGAH BUKTI BAYAR (Pdf/Gambar)</label>
                <input type="file" name="userfile2" class="form-control" required>
              </div>
              <?php } ?>

              <button type="submit" class="btn btn-success">UPLOAD</button>
            </form>          
                            </div>
                        </div>
                    </div>                    
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="card">
                                <table id="example" class="display responsive nowrap" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>Nominal</th>
                                            <th>Bulan</th>
                                            <th style="max-width: 100px;">Status</th>
                                            <th>Tgl Bayar</th>
                                            <?php if ($this->session->level == "2"): ?>
                                                <th>Bukti</th>
                                            <?php endif ?>
                                            <th>action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $no=1; foreach ($row->result() as $key => $value) {?>
                                        <tr>
                                            <td><?php echo "Rp " . number_format($value->nominal, 2, ",", "."); ?></td>
                                            <td><?php $yrdata= strtotime($value->bulan); echo  date('F', $yrdata)  ?></td>

                                            <?php if ($value->status == 1) {?>
                                                <td style="max-width: 100px;float:center;" text-align="center">Disetujui</td>
                                            <?php } else if($value->status == 2) {?>
                                                <td style="max-width: 100px;float:center;" text-align="center">Ditolak</td>
                                            <?php } else { ?>
                                                    <td style="max-width: 100px;float:center;" text-align="center">Menunggu</td>
                                                <?php } ?>

                                            <td><?= $value->tgl_bayar ?></td>
                                            <?php if ($this->session->level == "2"): ?>
                                            <td><a href="<?=base_url('uploads/bb/'.$value->bukti)?>" class="btn btn-success btn-sm m-b-10 m-l-5" target="_blank">Lihat</a></td>
                                            <?php endif ?>
                                            <td><form method="POST" action="<?= site_url('BuktiBayar/delsatu')?>"><input type="hidden" name="id1" value="<?= $value->id_satu?>" /> <button type="submit" class="btn btn-danger btn-sm m-b-10 m-l-5">Delete</button></form></td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>Nominal</th>
                                            <th>Bulan</th>
                                            <th style="max-width: 100px;">Status</th>
                                            <th>Tgl Bayar</th>
                                            <?php if ($this->session->level == "2"): ?>
                                            <th>Bukti</th>
                                            <?php endif ?>
                                            <th>action </th>
                                        </tr>
                                    </tfoot>
                            </table>         
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="card">
                                <table id="example2" class="display responsive nowrap" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>Nominal</th>
                                            <th>Bulan</th>
                                            <th style="max-width: 100px;">Status</th>
                                            <th>Tgl Bayar</th>
                                            <?php if ($this->session->level == "2"): ?>
                                                <th>Bukti</th>
                                            <?php endif ?>
                                            <th>action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $no=1; foreach ($rowe->result() as $key => $val) {?>
                                        <tr>
                                            <td><?php echo "Rp " . number_format($val->nominal, 2, ",", "."); ?></td>
                                            <td><?php $yrdatae= strtotime($val->bulan); echo  date('F', $yrdatae)  ?></td>
                                            <?php if ($val->status == 1) {?>
                                                <td style="max-width: 100px;float:center;" text-align="center">Disetujui</td>
                                            <?php } else if($val->status == 2) {?>
                                                <td style="max-width: 100px;float:center;" text-align="center">Ditolak</td>
                                            <?php } else { ?>
                                                    <td style="max-width: 100px;float:center;" text-align="center">Menunggu</td>
                                            <?php }?>
                                            <td><?= $val->tgl_bayar ?></td>
                                            <?php if ($this->session->level == "2"): ?>
                                            <td><a href="<?=base_url('uploads/bb/'.$val->bukti)?>" class="btn btn-success btn-sm m-b-10 m-l-5" target="_blank">Lihat</a></td>
                                            <?php endif ?>
                                            <td><form method="POST" action="<?= site_url('BuktiBayar/delempat')?>"><input type="hidden" name="id4" value="<?= $val->id_empat?>" /> <button type="submit" class="btn btn-danger btn-sm m-b-10 m-l-5">Delete</button></form></td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>Nominal</th>
                                            <th>Bulan</th>
                                            <th style="max-width: 100px;">Status</th>
                                            <th>Tgl Bayar</th>
                                            <?php if ($this->session->level == "2"): ?>
                                                <th>Bukti</th>
                                            <?php endif ?>
                                            <th>action</th>
                                        </tr>
                                    </tfoot>
                            </table>         
                            </div>
                        </div>
                    </div>