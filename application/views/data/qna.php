<?php $this->session->unset_userdata("page");
$params = array(
                        'page' => "QNA"
                    );
        $this->session->set_userdata($params);
 ?>
            <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <?php echo $this->session->flashdata('notif') ?>
                    <h4>  
                        <div class="row col-lg-12">
                            <?php if($this->session->level == "1") { ?>
                            <form method="POST" class="row col-lg-12" action="<?= site_url('qna/upload') ?>" enctype="multipart/form-data">
                              <div class="form-group col-lg-8">
                                <label for="exampleInputEmail1">UNGGAH FILE EXCEL</label>
                                <input type="file" name="userfile" class="form-control">
                              </div>
                              <div class="col-lg-4">
                                 <button type="submit" class="btn btn-success" style="margin-top: 12%;">UPLOAD</button>
                              </div>
                            </form>
                          <?php } ?>
                          </div>
                          <div class="row col-lg-12">
                            <form role="form" action="" method="post" class="row col-lg-12">
                              <div class="form-group col-lg-8">
                              <label>Filter Tema</label>
                              <select class="form-control" name="temas">
                                <?php $tema = $this->input->post('temas') ?>
                                <?php foreach ($tema2->result() as $key => $value) {?>
                                <option value="<?= $value->tema ?>" <?=$tema == $value->tema ? "Selected": null?> ><?= $value->tema ?></option>
                                <?php } ?>
                              </select>
                              </div>
                              <div class="col-lg-4">
                                <button style="margin-top: 12%;" type="submit" name='search' class="btn btn-primary">Cari</button>
                                <button style="margin-top: 12%;" type="button" class="btn btn-success" onclick="wa()">Chat Admin</button>
                              </div>
                            </form>
                          </div>
                        <table id="example2" class="display responsive nowrap" style="width:100%;font-size: 0.6em;">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>TEMA</th>
                                            <th>PERTANYAAN</th>
                                            <th>JAWABAN</th>
                                            <th>REGULASI</th>
                                            <th>BERKAS</th>
                                            <?php if ($this->session->level == "1") { ?>
                                              <th>UPLOAD BERKAS</th>
                                              <th>Action</th>
                                            <?php }?>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $no=1; foreach ($row->result() as $key => $value) {?>
                                        <tr>
                                            <td><?= $no++ ?></td>
                                            <td><?= $value->tema ?></td>
                                            <td><?= $value->question ?></td>
                                            <td><?= $value->answer?></td>
                                            <td><a href="<?= $value->regulasi ?>" target="_blank"><?= $value->regulasi ?></a></td>
                                            <?php if($this->session->level == "1")  { ?>
			                                <td align="center">
			                                	<?php echo form_open_multipart('qna/uploadBerkas/'.$value->id_qna) ?>
			                                			<input type="hidden" name="regis" value="<?= $value->id_qna?>">
			                                			 <!--<label for="file-input<?= $value->id_qna?>">-->
			                                		  <!--     <em class="fa fa-upload fa-1x" style="cursor: pointer;color: white; background-color: #FF00FF;padding: 8px;border-radius: 5px"></em>-->
			                                		  <!--  </label>-->
			                                			<input id="file-input<?= $value->id_qna?>" type="file" name="image<?= $value->id_qna?>" style="" >
			                                			<button id="tombolup<?= $value->id_qna?>" type="submit" style="">Upload</button>
			                                			<br>
			                                	<?php echo form_close() ?>
			                                </td>
			                                <?php } ?>
			                                    <td align="center">
			                                	<?php if($value->berkas != null){ ?>
			                                	    <a href="<?=site_url('uploads/qna/');echo $value->berkas?>"><em class="fa fa-download fa-1x" style="cursor: pointer;color: white; background-color: #40E0D0;padding: 8px;border-radius: 5px"></em></a>
			                                	<?php }else{ ?>Belum Tersedia
			                                    <?php } ?>
			                                    </td>
                                            <?php if($this->session->level == "1") { ?>
                                            <td>
                                                <form action="<?=site_url('qna/del');?>" method="post">
                                                    <input type="hidden" name="id" value="<?= $value->id_qna?>">
                                                    <button onclick="return confirm('Apakah anda yakin ingin menghapus?')" class="btn btn-sm btn-danger">Hapus</button>
                                                </form>
                                            </td>
                                          <?php } ?> 
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                            </table>
                      </div>
              </div>
          </div>
<script type="text/javascript">
  function wa(){
    window.open('https://wa.me/6285643562118', '_blank').focus();
    // $(location).prop('href', 'https://wa.me/6285643562118')
  }
  
  function myFunction(e,id){
      console.log(e,id);
    //   $.post('<?=site_url('qna/del');?>',{'file:e,id:id},
    //         function(data){
         
    //         });
  }
</script>