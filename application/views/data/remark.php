<?php $this->session->unset_userdata("page");
$params = array(
                        'page' => "Remark"
                    );
        $this->session->set_userdata($params);
 ?>                   
        <div id="accordion">
            <div class="card">
                <div class="card-header" id="headingOne">
                    <h5 class="mb-0">
                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false " aria-controls="collapseOne">
                                      Data Export Pegawai &nbsp; <span class="badge badge-pill badge-primary count-notif" style="float:right;"><span><span></spa></span><?php echo  $this->fungsi->countdata() ?></span>
                                    </button>
                    </h5>
                </div>
                <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                    <div class="card-body"><br>
                        <table id="example2" class="display responsive nowrap" style="width:100%">
                            <thead>
                                <tr>
                                    <th>NO</th>
                                    <th>Bulan</th>
                                    <th style="max-width: 100px;">Status</th>
                                    <th>Remark</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no=1; foreach ($data->result() as $key => $value) {?>
                                <tr>
                                    <td><?php echo $no++; ?></td>
                                    <td><?php $yrdata= strtotime($value->bulan); echo  date('F', $yrdata)  ?></td>
                                    <?php if ($value->status == 1) {?>
                                        <td style="max-width: 100px;float:center;" text-align="center">Disetujui</td>
                                    <?php } else if($value->status == 2) {?>
                                        <td style="max-width: 100px;float:center;" text-align="center">Ditolak</td>
                                    <?php } else { ?>
                                            <td style="max-width: 100px;float:center;" text-align="center">Menunggu</td>
                                        <?php } ?>
                                    <td><?= $value->remark ?></td>
                                </tr>
                                <?php } ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>Nominal</th>
                                    <th>Bulan</th>
                                    <th style="max-width: 100px;">Status</th>
                                    <th>Remark</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
        </dv>                   
    </div>
    <div id="accordion">
            <div class="card">
                <div class="card-header" id="headingTwo">
                    <h5 class="mb-0">
                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false " aria-controls="collapseTwo">
                                      Data Upload 1% &nbsp; <span class="badge badge-pill badge-primary count-notif" style="float:right;"><span><span></spa></span><?php echo  $this->fungsi->countdata1() ?></span>
                                    </button>
                    </h5>
                </div>
                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                    <div class="card-body"><br>
                        <table id="example3" class="display responsive nowrap" style="width:100%">
                            <thead>
                                <tr>
                                    <th>NO</th>
                                    <th>Bulan</th>
                                    <th style="max-width: 100px;">Status</th>
                                    <th>Remark</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no=1; foreach ($row1->result() as $key => $value) {?>
                                <tr>
                                    <td><?php echo $no++; ?></td>
                                    <td><?php $yrdata= strtotime($value->bulan); echo  date('F', $yrdata)  ?></td>
                                    <?php if ($value->status == 1) {?>
                                        <td style="max-width: 100px;float:center;" text-align="center">Disetujui</td>
                                    <?php } else if($value->status == 2) {?>
                                        <td style="max-width: 100px;float:center;" text-align="center">Ditolak</td>
                                    <?php } else { ?>
                                            <td style="max-width: 100px;float:center;" text-align="center">Menunggu</td>
                                        <?php } ?>
                                    <td><?= $value->remark ?></td>
                                </tr>
                                <?php } ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>Nominal</th>
                                    <th>Bulan</th>
                                    <th style="max-width: 100px;">Status</th>
                                    <th style="float: right;">Remark</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
        </div>                   
    </div>
    <div id="accordion">
            <div class="card">
                <div class="card-header" id="headingTree">
                    <h5 class="mb-0">
                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTree" aria-expanded="false " aria-controls="collapseTree">
                                      Data Upload 4% &nbsp;<span class="badge badge-pill badge-primary count-notif" style="float:right;"><span><span></spa></span><?php echo  $this->fungsi->countdata4() ?></span>
                                    </button>
                    </h5>
                </div>
                <div id="collapseTree" class="collapse" aria-labelledby="headingTree" data-parent="#accordion">
                    <div class="card-body"><br>
                        <table id="example3" class="display responsive nowrap" style="width:100%">
                            <thead>
                                <tr>
                                    <th>NO</th>
                                    <th>Bulan</th>
                                    <th style="max-width: 100px;">Status</th>
                                    <th>Remark</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no=1; foreach ($row4->result() as $key => $value) {?>
                                <tr>
                                    <td><?php echo $no++; ?></td>
                                    <td><?php $yrdata= strtotime($value->bulan); echo  date('F', $yrdata)  ?></td>
                                    <?php if ($value->status == 1) {?>
                                        <td style="max-width: 100px;float:center;" text-align="center">Disetujui</td>
                                    <?php } else if($value->status == 2) {?>
                                        <td style="max-width: 100px;float:center;" text-align="center">Ditolak</td>
                                    <?php } else { ?>
                                            <td style="max-width: 100px;float:center;" text-align="center">Menunggu</td>
                                        <?php } ?>
                                    <td><?= $value->remark ?></td>
                                </tr>
                                <?php } ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>Nominal</th>
                                    <th>Bulan</th>
                                    <th style="max-width: 100px;">Status</th>
                                    <th style="float: right;">Remark</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
        </div>                   
    </div>