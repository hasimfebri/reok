<?php $this->session->unset_userdata("page");
$params = array(
                        'page' => "Satker Data"
                    );
        $this->session->set_userdata($params);
 ?>
            <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <?php echo $this->session->flashdata('notif') ?>
                    <h4>
                        <?php if($this->session->level == 1) {?>
                            <a href="<?= site_url('satker/add'); ?>" class="btn btn-success">Tambah Data Satker</a>
                        <?php }else if($this->session->level == 3){ ?>
                                <!-- <div class="form-group col-lg-4">
                                    <label>Pilih Cabang</label>
                                    <select id ="filter-cabang" class="form-control" onChange="getSatker(this.value)">
                                        <option value ="">Pilih Cabang</option>
                                    </select>
                                </div> -->
                            <?php } ?>

                        
                    </h4><br><br>
                             <table id="example2" class="display responsive nowrap" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Aksi</th>
                                        <?php if($this->session->level == 3){ ?>
                                            <th>Cabang</th>
                                        <?php } ?>
                                            <th>Username</th>
                                            <th>Nama Satker</th>
                                            <th>Nama Kepala</th>
                                            <th>Nama Admin</th>
                                            <th>No HP Satker</th>
                                            <th>No HP Admin</th>
                                            <th>Kota</th>
                                            <th>Alamat</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody >
                                        <?php $no=1; foreach ($row->result() as $key => $value) {?>
                                        <tr>
                                            <td><?= $no++ ?></td>
                                            <td ><a class="btn btn-sm btn-success" href="<?=site_url('satker/change/') ?><?= $value->username; ?>">Pilih</a></td>
                                            <?php if($this->session->level == 3){ ?>
                                                <td><?= $value->nama_cabang ?></td>
                                            <?php } ?>
                                            <td><?= $value->username ?></td>
                                            <td><?= $value->nama ?></td>
                                            <td><?= $value->nama_kepala ?></td>
                                            <td><?= $value->nama_admin ?></td>
                                            <td><?= $value->no_hp_satker ?></td>
                                            <td><?= $value->no_hp_admin ?></td>
                                            <td><?= $value->kota ?></td>
                                            <td><?= $value->alamat ?></td>
                                            <td>
                                                <form action="<?=site_url('Satker/del');?>" method="post">
                                                    <a href="<?=site_url('satker/edit/'.$value->username);?>" class="btn btn-sm btn-warning">Edit</a>
                                                    <input type="hidden" name="username" value="<?= $value->username?>">
                                                    <button onclick="return confirm('Apakah anda yakin ingin menghapus?')" class="btn btn-sm btn-danger">Hapus</button>
                                                </form>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                            </table>            
                            </div>
                        </div>
                    </div>
<script src="<?php echo base_url()?>assets/js/lib/jquery.min.js"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script type="text/javascript">
 $(document).ready(function() {
        //  $.post( "admin/getCabang",function(data) {
        //     $('#filter-cabang').html(data);
        // });
 });
</script>