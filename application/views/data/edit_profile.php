<?php $this->session->unset_userdata("page");
$params = array(
                        'page' => "Edit Profile"
                    );
        $this->session->set_userdata($params);
 ?>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                    <form method="POST" action="<?= site_url('profile/editproce') ?>" enctype="multipart/form-data">                                
                
                <div class="form-group">
                    <label>Nama Satker</label>
                    <input type="hidden" class="form-control" name="username" value="<?= $row->username ?>">
                    <input type="text" class="form-control" name="namasatker" value="<?= $row->nama ?>">
                </div>
                <div class="form-group">
                    <label>Nama Kepala Satker</label>
                    <input type="text" class="form-control" name="namakepalasatker" value="<?= $row->nama_kepala ?>">
                </div>
                <div class="form-group">
                    <label>Nama Admin</label>
                    <input type="text" class="form-control" name="namaadmin" value="<?= $row->nama_admin ?>">
                </div>
                <div class="form-group">
                    <label>No HP Satker</label>
                    <input type="number" class="form-control" name="hpsatker" value="<?= $row->no_hp_satker ?>">
                </div>
                <div class="form-group">
                    <label>No HP Admin</label>
                    <input type="number" class="form-control" name="hpadmin" value="<?= $row->no_hp_admin ?>">
                </div>
                <div class="form-group">
                    <label>Kota</label>
                    <input type="text" class="form-control" name="kota" value="<?=  $row->kota ?>">
                </div>
                <div class="form-group">
                    <label>Alamat Lengkap Satker</label>
                    <input type="text" class="form-control" name="alamat" value="<?=  $row->alamat ?>">
                </div>
                <div class="form-group">
                                    <label>Upload gambar</label>
                                    <input type="file" name="logo" class="form-control" required />
                                    <?= form_error('logo') ?>
                                </div>
              <button type="submit" class="btn btn-success">Simpan</button>
            </form>            
                            </div>
                        </div>
                    </div>                    
                    