<?php $this->session->unset_userdata("page");
$params = array(
                        'page' => "Edit Satker"
                    );
        $this->session->set_userdata($params);
 ?>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-title">
                                    <h4>Edit Data Satker</h4>
                                    
                                </div>
                                <hr>
                                <?php echo $this->session->flashdata('notif') ?>
                                <?php $row = $row->row(); ?>
                                <form method="POST" action="" enctype="multipart/form-data">                                 
                                <div class="form-group">
                                    <label>Username</label>
                                    <input class="form-control" type="text"  value="<?=$this->input->post('username') ?? $row->username ?>" disabled readonly>
									<input class="form-control" type="hidden"  value="<?=$this->input->post('username') ?? $row->username ?>" name="username">
                                    <?= form_error('username') ?>
                                </div>
                                <div class="form-group">
                                    <label>Nama Satker</label>
                                    <input class="form-control" type="text"  value="<?=$this->input->post('nama') ?? $row->nama ?>" name="nama">
                                    <?= form_error('nama') ?>
                                </div>
                                <div class="form-group">
                                    <label>Alamat </label>
                                    <input class="form-control" type="text" value="<?=$this->input->post('alamat') ?? $row->alamat ?>"  name="alamat">
                                    <?= form_error('alamat') ?>
                                </div>
                                <div class="form-group">
                                    <label>Password</label>
                                    <input type="password" class="form-control" value="<?=set_value('password')?>" name="password">
                                    <?= form_error('password') ?>
                                </div>
                                <div class="form-group">
                                    <label>Password Konfirmasi</label>
                                    <input type="password" class="form-control" value="<?=set_value('passconf')?>" name="passconf">
                                    <?= form_error('passconf') ?>
                                </div>
                                <button type="submit" class="btn btn-primary">Submit Button</button>
                                    <button type="reset" class="btn btn-default">Reset Button</button></form>
                                    </div>
                                    </div>
                                    </div>          
