<style>
.modal-dialog-full-width {
    width: 80% !important;
    height: 100% !important;
    margin: 0 !important;
    padding: 0 !important;
    max-width:none !important;
    /* position: center !important; */
    margin-left: 10% !important;
    margin-right: 10% !important;
}
.modal-dialog {
    height: 100% !important;
}
.container {
  height: 200px;
  position: relative;
  border: 3px solid green;
}

.vertical-center {
  margin: 0;
  position: absolute;
  top: 50%;
  -ms-transform: translateY(-50%);
  transform: translateY(-50%);
}
.table2 {
  width: 100%;
  /*border-collapse: collapse;*/
}
</style>

<?php $this->session->unset_userdata("page");
$params = array(
                        'page' => "Pegawai Data"
                    );
        $this->session->set_userdata($params);
 ?>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <?php echo $this->session->flashdata('notif') ?>
								<br><br>
								<div class="row col-lg-12">
								    <div class="col-lg-4">
								<div class="form-group">
                                    <label>Pilih Bulan</label>
                                    <select name="bulan" id = "filter-month" class="form-control" onChange="cek()">
                                        <option value ="">Pilih Bulan</option>
                                        <option value="01">Januari</option>
                                        <option value="02">Februari</option>
                                        <option value="03">Maret</option>
                                        <option value="04">April</option>
                                        <option value="05">Mei</option>
                                        <option value="06">Juni</option>
                                        <option value="07">Juli</option>
                                        <option value="08">Agustus</option>
                                        <option value="09">September</option>
                                        <option value="10">Oktober</option>
                                        <option value="11">November</option>
                                        <option value="12">Desember</option>
                                    </select>
                                </div>
                                </div>
         <!--                       <div class="col-lg-4">-->
         <!--                       <div class="form-group vertical-center" style="vertical-center">-->
									<!--	<button onclick='cetakpembayaran()' class="btn btn-success">Cetak Pembayaran</button>-->
									<!--</div>-->
									<!--</div>-->
									</div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div id="table"></div>
                        </div>
                    </div>
<div class="modal fade" id="generateModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-dialog-full-width" role="document">
    <div class="modal-content">
      <div class="modal-header">
          <h5 class="modal-title" id="myModalLabel">Pengajuan Aggota Keluarga</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <form class="form-generate form-horizontal" id="form-addAnggota" action="<?= site_url('Pegawai/addAnggota')?>" method="POST" enctype="multipart/form-data" >
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
                <div class="row col-md-12">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>NOMOR KK</label>
                            <input id="no_kk" onkeypress="return hanyaAngka(event)" class="form-control input" type="text" name="no_kk">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>NIK</label>
                            <input id="nik_b" class="form-control" type="text" name="nik_b" readonly>
                        </div>
                     </div>
                </div>
                <hr>
                <table class="table table-bordered generate-table">
                    <thead >
                        <tr class="bg-blue" style="text-align:center;">
                            <th width="20%">NIK</th>
                            <th width="30%">NAMA</th>
                            <th width="10%">tgl lahir</th>
                            <th width="10%">Status</th>
                            <th width="10%">anak ke</th>
                            <th width="15%">Bukti</th>
                            <th width="5%">Action</th>
                        </tr>
                        <?php if ($this->session->level == "2"){?>
                        <tr class="bg-blue" style="text-align:center;">
                            <th width="20%">
                                <input id="nik_tambah" onkeypress="return hanyaAngka(event)" class="form-control input" maxlength="16" type="text" name="nik_tambah" placeholder="NIK Anggota">
                                <input id="nik_sumber" class="form-control input" type="hidden" name="nik_sumber">
                            </th>
                            <th width="30%">
                                 <input id="nama_anggota" class="form-control input" type="text" name="nama_anggota" placeholder="Nama Anggota">
                            </th>
                            <th width="10%">
                                <div class='input-group date'>
                                    <input type="text" name="birth" class="form-control datepicker4 input" placeholder="birth">
                                </div>
                            </th>
                            <th width="10%">
                                    <select class="form-control select2" style="width:100%" name="status" onchange="getStatuskeluarga(this.value)">
                                        <option value="0">Status</option>
                                        <option value="1">SUAMI</option>
                                        <option value="2">ISTRI</option>
                                        <option value="3">ANAK</option>
                                    </select>
                            </th>
                            <th width="10%">
                                <input type="number" name="anak" id="anak" class="form-control" placeholder="anak">
                            </th>
                            <th width="15%">
                                    <input type="file" name="bukti" class="form-control input">
                            </th>
                            <th width="5%">
                                <button class="btn btn-success" type="button" onclick="addAnggota(this)">Add</button>
                            </th>
                        </tr>
                        <?php }?>
                    </thead>
                    <tbody id="body">
                    </tbody>
                </table>
          </div>
        </div>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <!--<button type="submit" class="btn btn-success">Generate</button>-->
      </div>
      </form>
    </div>
  </div>
</div>

   <!-- jquery vendor -->
   <script src="<?php echo base_url()?>assets/js/lib/jquery.min.js"></script>
   <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <!-- nano scroller -->

    <!-- scripit init-->
    <!-- <script src="https://cdn.jsdelivr.net/npm/chart.js@3.3.0/dist/chart.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/searchpanes/1.3.0/js/dataTables.searchPanes.min.js"></script>
    <script src="https://cdn.datatables.net/select/1.3.3/js/dataTables.select.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.7.1/js/dataTables.buttons.min.js"></script> -->
<script type="text/javascript">
        $(document).ready(function() {
            cek();
        } );
    function cek(){
        startLoader();
        var month = $('#filter-month').val();
        $.post( "Pegawai/getData", {month:month},function( data ) {
            $("#table" ).html(data);
             stopLoader();
            $('#example').DataTable( {
            initComplete: function () {
                this.api().columns().every( function () {
                    var column = this;
                } );
            }
        } );
        });
    }
    function hanyaAngka(event) {
        var angka = (event.which) ? event.which : event.keyCode
        if (angka != 46 && angka > 31 && (angka < 48 || angka > 57))
          return false;
        return true;
    }
    function getStatuskeluarga(e){
        if(e == 3){
            $('#anak').prop('readonly', false);
            $("#anak").attr("required", true);
            $('#anak').addClass('required');
        }else{
            $('#anak').prop('readonly', true);
            $("#anak").attr("required", false);
            $('#anak').removeClass('required');
            $("#anak").val("");
        }
    }
    function addAnggota(){
        if ($('#form-addAnggota').find('.input').filter(function(){ return this.value === '' }).length > 0) {
				event.preventDefault();
				alert('Silahkan Isi Semua Data');
				return false;
			}
        var kk = $('#no_kk').val();
        // var nik = $('#nik_b').val();
        if (kk == ""){
            Alert("NO KK Tidak Boleh Kosong");
            return;
        }
        if (nik == ""){
            Alert("NIK Sumber Tidak Boleh Kosong");
        }
        swal.fire({
					title: "Apakah Semua Data Sudah Benar?",
					text: "Data Yang Disubmit Tidak Bisa Dikembalikan!",
					icon: "warning",
					showCancelButton: true,
					confirmButtonColor: '#DD6B55',
					confirmButtonText: 'Simpan',
					cancelButtonText: "Batal",
					closeOnConfirm: false,
					closeOnCancel: false
				}).then(function(isConfirm) {
                    if (isConfirm.value) {
                        $('#form-addAnggota')[0].submit();
                        startLoader();
					} else {
						Alert("Gagal");
					}
			});
    }
    function Ajukan(){
        swal.fire({
					title: "Apakah Semua Data Sudah Benar?",
					text: "Data Yang Disubmit Tidak Bisa Dikembalikan!",
					icon: "warning",
					showCancelButton: true,
					confirmButtonColor: '#DD6B55',
					confirmButtonText: 'Simpan',
					cancelButtonText: "Batal",
					closeOnConfirm: false,
					closeOnCancel: false
				}).then(function(isConfirm) {
                    if (isConfirm.value) {
                        $('#form-add')[0].submit();
                         startLoader();
					} else {
						Alert("Gagal");
					}
			});
    }
    function Ajukan_pen(){
        var cek =  $('#bukti_pen').val();
        if(cek == ""){
            alert("Silahkan Upload bukti");
            return;
        }
        swal.fire({
					title: "Apakah Semua Data Sudah Benar?",
					text: "Data Yang Disubmit Tidak Bisa Dikembalikan!",
					icon: "warning",
					showCancelButton: true,
					confirmButtonColor: '#DD6B55',
					confirmButtonText: 'Simpan',
					cancelButtonText: "Batal",
					closeOnConfirm: false,
					closeOnCancel: false
				}).then(function(isConfirm) {
                    if (isConfirm.value) {
                        $('#form-del')[0].submit();
                         startLoader();
					} else {
						Alert("Gagal");
					}
			});
    }
    function mutasi(e,tgl,nik) {
//         if(tgl > 20){
// 			alert('Pengajuan Hanya Bisa Diajukan sebelum tanggal 20');
// 			return;
// 		}
        $('#jenisNm').html('Mutasi');
		$('#form-add').trigger("reset");
		$('.input').attr('readonly', true);
		$('#jenis').val('2');
		$('#modal-ket').modal('show');
		startLoader();
			$.post( "Pengajuan/cekNik",{nik:e},function( data ) {
			    stopLoader();
				if(data.length == 0){
					alert('Data Peserta Tidak Terdaftar');
					return false;
				}
				$('#pegawai').val(data.data.nama_pegawai);
				$('#nik').val(nik);
				$('#idp').val(e);
				$('#nip').val(data.data.nip);
				$('#jml_is').val(data.data.jml_is);
				$('#jml_an').val(data.data.jml_an);
				$('#penghasilan').val(data.data.penghasilan);
				$('#tunjangan').val(data.data.tunjangan);
				$('#thp').val(data.data.thp);
				$('#dpi').val(data.data.dpi);
				$('#iuran_1').val(data.data.iuran_1);
				$('#iuran_4').val(data.data.iuran_4);
				$('#total_i').val(data.data.total_i);
			},"json");
	}
	function penambahan(e,tgl,nik){
	    var content = "";
	    $('#nik_b').val(nik);
	    $('#nik_sumber').val(e);
	    $('#generateModal').modal('show');
	    startLoader();
	    $.post( "Pegawai/getKK",{e:e},function( data ) {
	        stopLoader();
	        if(data != null){
	            $('#no_kk').val(data.no_kk);
	            $('#no_kk').attr('readonly', true);
	        }else{
	            $('#no_kk').val("");
	            $('#no_kk').attr('readonly', false);
	        }
	    },"json");
	    	$.post( "Pegawai/getAnggota",{e:e},function( data ) {
	    	    $('#body').html(data);
	    	    stopLoader();
            //   if (data.length == 0){
            //      content = "<tr>"
            //     +"<td>-</td>"
            //     +"<td>-</td>"
            //     +"<td>-</td>"
            //     +"<td>-</td>"
            //     +"<td>-</td>"
            //     +"<td>-</td>"
            //     +"<td>-</td></tr>";
            //     $('.generate-table > tbody').append(content);
            //   }else{
            //       data.forEach(function(item) {
            //           console.log(item)
            //      content = "<tr>"
            //     +"<td>"+item.nama+"</td>"
            //     +"<td>"+item.nama+"</td>"
            //     +"<td>"+item.tgl_lahir+"</td>"
            //     +"<td>-</td>"
            //     +"<td>-</td>"
            //     +"<td>-</td>"
            //     +"<td>-</td></tr>";            
            //         });
                    
            //         $('.generate-table > tbody').append(content);
            //   }
			});
	    
	}
    function cetakpembayaran(){
        var month = $('#filter-month').val();
        const monthNames = ["January", "February", "March", "April", "May", "June",
                "July", "August", "September", "October", "November", "December"];
        const d = new Date();
        $.post( "Pegawai/cetakPembayaran", {month:month},function( data ) {
            // $("#table" ).html(data);
            console.log(data.jumlah_pegawai);
            if(data.jumlah_pegawai != 0){
            $('#modal-bukti').modal('show');
            $('#jumlah').html(data.jumlah_pegawai);
            $('#jum1').html(formatRupiah(data.iuran_1, 'Rp. '));
            $('#jum4').html(formatRupiah(data.iuran_4, 'Rp. '));
            $('#jum5').html(formatRupiah(data.total_i, 'Rp. '));
            if(month == ""){
                 $('#b').html(monthNames[d.getMonth()]);
            }else{
                $('#b').html(monthNames[parseInt(month)-1]);
            }
            }else{
                 $('#modal-bukti').modal('show');
                 $('#jumlah').html("-");
                 $('#jum1').html("-");
                 $('#jum4').html("-");
                 $('#jum5').html("-");
                 if(month == ""){
                      $('#b').html(monthNames[d.getMonth()]);
                 }else{
                     $('#b').html(monthNames[parseInt(month)-1]);
                 }
            }
        },"json");
    }
    function formatRupiah(angka, prefix){
	var number_string = angka.replace(/[^,\d]/g, '').toString(),
	split   		= number_string.split(','),
	sisa     		= split[0].length % 3,
	rupiah     		= split[0].substr(0, sisa),
	ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);
 
	// tambahkan titik jika yang di input sudah menjadi angka ribuan
	if(ribuan){
		separator = sisa ? '.' : '';
		rupiah += separator + ribuan.join('.');
	}
 
	rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
	return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
    }
    function delAnggota(e,tgl,nik){
        $.post( "Pegawai/getKK",{e:e},function( data ) {
            $('#modal-del').modal('show');
            console.log(data);
	            $('#no_kk_penon').val(data.no_kk);
	            $('#nik_pen').val(nik);
	            $('#nik_penon').val(data.nik);
	            $('#nama_penon').val(data.nama);
	    },"json");
    }
</script>
<div class="modal fade" id="modal-bukti" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
          <h5 class="modal-title">Rekap Tagihan Pembayaran <b id="b"></b> <?php echo $this->session->tahun;?></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body" id='body-modal'>
			<table border="solid black" width="100%" style="text-align:left">
			    <tr>
			        <td>Satker</td>
			        <td>:</td>
			        <td id="satker" style="text-align:left"><?php echo $this->session->satker;?></td>
			    </tr>
			    <tr>
			        <td>Jumlah Pegawai</td>
			        <td>:</td>
			        <td id="jumlah" style="text-align:left"></td>
			    </tr>
			    <tr>
			        <td>Iurab 1%</td>
			        <td>:</td>
			        <td id="jum1" style="text-align:left"></td>
			    </tr>
			    <tr>
			        <td>Iuran 4%</td>
			        <td>:</td>
			        <td id="jum4" style="text-align:left"></td>
			    </tr>
			    <tr>
			        <td>Total Iuran (5%)</td>
			        <td>:</td>
			        <td id="jum5" style="text-align:left"></td>
			    </tr>
			</table>
	  </div>
      </div>
    </div>
</div>

<div class="modal fade" id="modal-ket" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                      <div class="modal-dialog modal-dialog-full-width" role="document">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h5 class="modal-title">Tambah <b id="jenisNm">Pengajuan</b> Peserta</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <div class="modal-body" id='body-modal'>
                          <form id="form-add" action="<?= site_url('Pegawai/getMutasi')?>"  method="POST" enctype="multipart/form-data"> 
                          <div class="row col-lg-12">
                            <div class="col-lg-4">                                
                                <div class="form-group">
                                    <label>Nama Pegawai</label>
                                    <input id="pegawai" class="form-control input" type="text" name="pegawai">
									<input type="hidden" name="jenis" id="jenis">
									<input type="hidden" name="idp" id="idp">
                                </div>                                 
                                <div class="form-group">
                                    <label>NIK</label>
                                    <input id="nik" class="form-control input" type="text" name="nik">
                                </div>                                 
                                <div class="form-group">
                                    <label>NIP</label>
                                    <input id="nip" class="form-control input" type="number" name="nip">
                                </div>                                 
                                <div class="form-group">
                                    <label>Jumlah Istri</label>
                                    <input id="jml_is" class="form-control input" type="number" name="jml_is">
                                </div>                                 
                                <div class="form-group">
                                    <label>Jumlah Anak</label>
                                    <input id="jml_an" class="form-control input" type="number" name="jml_an">
                                </div>
                            </div>
                            <div class="col-lg-4">                                
                                <div class="form-group">
                                    <label>Penghasilan</label>
                                    <input id="penghasilan" class="form-control input" type="number" name="penghasilan" onfocusout="sumOtomatis('thp','tunjangan',this.value)">
                                </div>                                 
                                <div class="form-group">
                                    <label>Tunjangan</label>
                                    <input id="tunjangan" class="form-control input" type="number" name="tunjangan" onfocusout="sumOtomatis('thp','penghasilan',this.value)">
                                </div>                                 
                                <div class="form-group">
                                    <label>thp</label>
                                    <input id="thp" class="form-control input" type="number" name="thp">
                                </div>                                 
                                <div class="form-group">
                                    <label>dpi</label>
                                    <input id="dpi" class="form-control input" type="number" name="dpi">
                                </div>                                 
                                <div class="form-group">
                                    <label>iuran_1</label>
                                    <input id="iuran_1" class="form-control input" type="number" name="iuran_1">
                                </div>
                            </div>
                            <div class="col-lg-4">                                                              
                                <div class="form-group">
                                    <label>Iuran_4</label>
                                    <input id="iuran_4" class="form-control input" type="number" name="iuran_4">
                                </div>                                 
                                <div class="form-group">
                                    <label>total_i</label>
                                    <input id="total_i" class="form-control input" type="number" name="total_i">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">UNGGAH FILE BUKTI (pdf/png/jpg)<label>
                                    <input type="file" name="bukti" class="form-control input" required>
                                </div>
                            </div>
                            <button onclick="Ajukan()" type="button" class="btn btn-primary">Submit Button</button>
                                <button type="reset" class="btn btn-default">Reset Button</button>
                            </form>
                          </div>
                        </div>
                      </div>
</div>
</div>

<div class="modal fade" id="modal-del" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h5 class="modal-title">Tambah <b id="jenisNm">Penonaktifan</b> Anggota Keluarga</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <div class="modal-body" id='body-modal'>
                          <form id="form-del" action="<?= site_url('Pegawai/getPenon')?>"  method="POST" enctype="multipart/form-data"> 
                          <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label>No_KK</label>
                                    <input id="no_kk_penon" class="form-control" type="text" name="no_kk_penon" readonly>
                                </div>  
                                <div class="form-group">
                                    <label>NIK</label>
                                    <input id="nik_pen" class="form-control input" type="text" name="nik_pen" readonly>
                                    <input id="nik_penon" class="form-control input" type="hidden" name="nik_penon" readonly>
                                </div>                                 
                                <div class="form-group">
                                    <label>Nama</label>
                                    <input id="nama_penon" class="form-control input" type="text" name="nama_penon" readonly>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">UNGGAH FILE BUKTI (pdf/png/jpg)<label>
                                    <input id="bukti_pen" type="file" name="bukti" class="form-control input" required>
                                </div>
                            </div>
                            <button onclick="Ajukan_pen()" type="button" class="btn btn-primary">Submit Button</button>
                               <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </form>
                          </div>
                        </div>
                      </div>
</div>
</div>



                    