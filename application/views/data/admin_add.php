<?php $this->session->unset_userdata("page");
$params = array(
                        'page' => "Tambah Admin"
                    );
        $this->session->set_userdata($params);
 ?>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-title">
                                    <h4>Tambah Data Satker</h4>
                                    
                                </div>
                                <hr>
                                <?php echo $this->session->flashdata('notif') ?>
                                <?php echo form_open_multipart('admin/add');?>
                                <div class="form-group">
                                    <label>Username</label>
                                    <input class="form-control" type="text" placeholder="Username" value="<?=set_value('username')?>" name="username">
                                    <?= form_error('username') ?>
                                </div>
                                <div class="form-group">
                                    <label>Nama</label>
                                    <input class="form-control" type="text" placeholder="nama" value="<?=set_value('nama')?>" name="nama">
                                    <?= form_error('nama') ?>
                                </div>

                                <div class="form-group">
                                    <label>Password</label>
                                    <input type="password" class="form-control" value="<?=set_value('password')?>" name="password" placeholder="******">
                                    <?= form_error('password') ?>
                                </div>
                                <div class="form-group">
                                    <label>Password Konfirmasi</label>
                                    <input type="password" class="form-control" value="<?=set_value('passconf')?>" name="passconf" placeholder="******">
                                    <?= form_error('passconf') ?>
                                </div>
                                <button type="submit" class="btn btn-primary">Submit Button</button>
                                    <button type="reset" class="btn btn-default">Reset Button</button></form>
                                    </div>
                                    </div>
                                    </div>          