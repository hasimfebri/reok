<?php $this->session->unset_userdata("page");
$params = array(
                        'page' => "Profile"
                    );
        $this->session->set_userdata($params);
 ?>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <h4><a href="<?= site_url('profile/edit') ?>" class="btn btn-primary">Edit</a></h4>
                               
                                <br>
                                <table border="1px" cellpadding="10">
                                    <tr>
                                        <td width="30%">Nama Satker</td><td style="text-align: left"><?= $row->nama ?></td>
                                    </tr>
                                    <tr>
                                        <td >Nama Kepala Satker</td><td style="text-align: left"><?= $row->nama_kepala ?></td>
                                    </tr>
                                    <tr>
                                        <td >Nama Admin</td><td style="text-align: left"><?= $row->nama_admin ?></td>
                                    </tr>
                                    <tr>
                                        <td >No HP Satker</td><td style="text-align: left"><?= $row->no_hp_satker ?></td>
                                    </tr>
                                    <tr>
                                        <td >No HP Admin</td><td style="text-align: left"><?= $row->no_hp_admin ?></td>
                                    </tr>
                                    <tr>
                                        <td >Kota </td><td style="text-align: left"><?= $row->kota ?></td>
                                    </tr>
                                    <tr>
                                        <td >alamat Satker</td><td style="text-align: left"><?= $row->alamat ?></td>
                                    </tr>
                                </table> 
                            </div>
                        </div>
                    </div>                    
                    