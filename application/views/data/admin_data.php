<?php $this->session->unset_userdata("page");
$params = array(
                        'page' => "Admin Data"
                    );
        $this->session->set_userdata($params);
 ?>
            <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <?php echo $this->session->flashdata('notif') ?>
                    <h4>
                       <button type="button" onclick="addAdmin()" class="btn btn-sm btn-success">Tambah Admin Cabang</a>
                    </h4><br><br>
                             <table id="example2"  style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Username</th>
                                            <th>Nama Admin</th>
                                            <th>Cabang</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $no=1; foreach ($row->result() as $key => $value) {?>
                                        <tr>
                                            <td><?= $no++ ?></td>
                                            <td><?= $value->username ?></td>
                                            <td><?= $value->nama ?></td>
                                            <td><?= $value->nama_cabang ?></td>
                                            <td>
                                                <form >
                                                    <button type="button" onclick="aku(`edit`,`<?= $value->username?>`)" class="btn btn-sm btn-warning">Edit</button>
                                                    <button type="button" onclick="action(`del`,`<?= $value->username?>`)" class="btn btn-sm btn-danger">Hapus</button>
                                                </form>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>No</th>
                                            <th>Username</th>
                                            <th>Nama Admin</th>
                                            <th>Cabang</th>
                                            <th></th>
                                        </tr>
                                    </tfoot>
                            </table>            
                            </div>
                        </div>
                    </div>
   <script src="<?php echo base_url()?>assets/js/lib/jquery.min.js"></script>
   <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
   <script type="text/javascript">
    $(document).ready(function() {
            getCabang();
            $('#addusername').on('keypress', function(e) {
                if (e.which == 32){
                    return false;
                }
            });
            $('#addusername').on('focusout', function(e) {
                // startLoader();
                var username = $('#addusername').val();
                $.post( "admin/cekusername",{username:username},function(data) {
                    // stopLoader();
                    console.log(data.status);
                    if(data.status){
                        alert('username sudah digunakan');
                        $('#addusername').val("");
                    }
                },"json");
            });
            $(".form-control").attr("autocomplete", 'off');
    });
    function getCabang(){
        $.post( "admin/getCabang",function(data) {
            $('#filter-cabang').html(data);
            $('#add-cabang').html(data);
        });
    }
    function aku(jenis,id) {
        if(jenis == 'edit'){
         $.post( "admin/getEdit", {id:id},function( data ) {
            $('#username').val(id);
            $('#usernameasli').val(id);
            $('#nama').val(data.nama);
            $("#filter-cabang").val(data.cabang).trigger("change");
            // $('.selects').append('<select><option value="1">Added 1</option><option value="2">Added 2</option></select>');
             $('#modal-edit').modal('show');
         },"json");
        }else{        }
    }
    function dia(e,target){
        if($('#'+target).val() != e){
            alert('Password Tidak sama');
            return;
        }
    }
    function save(e){
        if (e == 1){
            if($('#password').val() != $('#newpassword').val()){
              alert('Password Konfirmasi Tidak sama');
                return;  
            }else{
                $('#edit-post')[0].submit();
            }    
        }else if(e == 2){
             if($('#addusername').val() == ""){
                alert('Password Konfirmasi Tidak sama');
                return;  
            }else{
                $('#add-post')[0].submit();
            }
        }
    }

    function addAdmin(){
         $('#modal-add').modal('show');
    }


   </script>

<div class="modal fade" id="modal-edit" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
          <h5 class="modal-title">Edit Admin</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body" id='body-modal'>
            <form method="POST" id="edit-post" action="<?= site_url('Admin/edit')?>"  enctype="multipart/form-data">                                 
                <div class="form-group">
                    <label>Username</label>
                    <input class="form-control" type="text"  id="username" name="username" disabled readonly>
                    <input class="form-control" type="hidden" id="usernameasli" name="usernameasli">
                </div>
                <div class="form-group">
                    <label>Nama Satker</label>
                    <input class="form-control" type="text" id="nama" name="nama">
                </div>
                <div class="form-group">
                    <label>Pilih Cabang</label>
                    <select id ="filter-cabang"  name="cabang" class="form-control selects">
                    </select>
                </div>
                <div class="form-group">
                    <label>Password</label>
                    <input type="password" class="form-control" id="password" name="password"  autocomplete="off">
                </div>
                <div class="form-group">
                    <label>Password Konfirmasi</label>
                    <input type="password" class="form-control" id="newpassword" name="passconf" onfocusout="dia(this.value,'password')" autocomplete="off">
                </div>
                <button type="button" onclick="save(1)" class="btn btn-primary">Submit Button</button>
                <button type="reset" class="btn btn-default">Reset Button</button>
            </form>
      </div>
      </div>
    </div>
</div>

<div class="modal fade" id="modal-add" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
          <h5 class="modal-title">Tambah Admin Cabang</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body" id='body-modal'>
            <form method="POST" id="add-post" action="<?= site_url('Admin/addAdmin')?>"  enctype="multipart/form-data">                                 
                <div class="form-group">
                    <label>Username</label>
                    <input class="form-control" type="text" id="addusername" name="addusername" required>
                </div>
                <div class="form-group">
                    <label>Nama Satker</label>
                    <input class="form-control" type="text" id="nama" name="addnama" required>
                </div>
                <div class="form-group">
                    <label>Pilih Cabang</label>
                    <select id ="add-cabang"  name="addcabang" class="form-control selects" required>
                    </select>
                </div>
                <div class="form-group">
                    <label>Password</label>
                    <input type="password" class="form-control" id="addpassword" name="addpassword"  autocomplete="off" required>
                </div>
                <div class="form-group">
                    <label>Password Konfirmasi</label>
                    <input type="password" class="form-control" id="newpassword" name="addpassconf" onfocusout="dia(this.value,'addpassword')" autocomplete="off" required>
                </div>
                <button type="button" onclick="save(2)" class="btn btn-primary">Submit Button</button>
                <button type="reset" class="btn btn-default">Reset Button</button>
            </form>
      </div>
      </div>
    </div>
</div>
