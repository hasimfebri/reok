<style>
.modal-dialog-full-width {
    width: 80% !important;
    height: 100% !important;
    margin: 0 !important;
    padding: 0 !important;
    max-width:none !important;
    /* position: center !important; */
    margin-left: 10% !important;
    margin-right: 10% !important;
}
</style>

<?php $this->session->unset_userdata("page");
$params = array(
                        'page' => "Pengajuan Data"
                    );
        $this->session->set_userdata($params);
 ?>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <?php echo $this->session->flashdata('notif') ?>                           
                                   <?php if($this->session->level == "2") {
									 if ($jumlah < 1 && $pengajuan < 1){?>
									<form method="POST" action="<?= site_url('Pengajuan/upload') ?>" enctype="multipart/form-data">
                                    	<div class="form-group col-lg-6">
                                    		<label for="exampleInputEmail1">UNGGAH FILE EXCEL</label>
                                    	<input type="file" name="userfile" class="form-control" required>
                                  		<button type="submit" class="btn btn-success load">UPLOAD</button>
                                  		<button onclick='Anggota()' class="btn btn-success">Anggota Keluarga</button>
								   </form>
                                  </div>
                                  <?php }else{?>
                                    <div class="form-group col-lg-6">
                                        <button onclick='addPengajuan(<?= $tanggal ?>)' class="btn btn-success">Tambah Pengajuan</button>
                                        <button onclick='Anggota()' class="btn btn-success">Anggota Keluarga</button>
                                    </div>
                                    <?php }
									}else { ?>
									<div class="form-group col-lg-6">
										<?php if($this->session->level == "1") {?>
											<button onclick='validasi(1)' class="btn btn-success">Validasi Persetujuan</button>
										<?php } ?>
										<button onclick='Anggota()' class="btn btn-success">Anggota Keluarga</button>
										<a href="<?= site_url('Pengajuan/downloadpengajuan')?>"  class="btn btn-warning">Download Excel</a>
									</div>
									<?php }?>
									<br><br>
                    <div class="row">
                        <div class="col-lg-12">
                            <div id="table"></div>
                        </div>
                    </div>
 
<div class="modal fade" id="modal-bukti" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
          <h5 class="modal-title">Tambah Bukti</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body" id='body-modal'>
			<form id="form-bukti" class="load"  method="POST" action="<?= site_url('Pengajuan/addBukti') ?>" enctype="multipart/form-data"> 
				<div class="form-group">
        		        <label for="exampleInputEmail1">UNGGAH BUKTI BAYAR (Pdf/Gambar)</label>
        		        <input type="file" name="bukti" class="form-control">
						<input id="id" type="hidden" name="id" >
        		</div>
				<button type="submit" class="btn btn-success">UPLOAD</button>
			</form>
	  </div>
      </div>
    </div>
</div>

<div class="modal fade" id="modal-Tolak" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
          <h5 class="modal-title">Alasan Penolakan Pengajuan</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body" id='body-modal'>
			<form id="form-bukti" class="load" method="POST" action="<?= site_url('Pengajuan/Tolak') ?>" enctype="multipart/form-data"> 
				<div class="form-group">
        		        <label for="exampleInputEmail1">Alasan Penolakan</label>
        		        <textarea rows="5" cols="60" id="text_tolak" type="file" name="text_tolak"></textarea>
						<input id="id_tolak" type="hidden" name="id_tolak" >
						<input id="jenis_tolak" type="hidden" name="jenis_tolak" >
        		</div>
				<?php if($this->session->level == "1") {?>
				<button type="submit" class="btn btn-success">UPLOAD</button>
				<?php }?>
			</form>
	  </div>
      </div>
    </div>
</div>

   <!-- jquery vendor -->
   <script src="<?php echo base_url()?>assets/js/lib/jquery.min.js"></script>
   <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <!-- nano scroller -->

    <!-- scripit init-->
    <!-- <script src="https://cdn.jsdelivr.net/npm/chart.js@3.3.0/dist/chart.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/searchpanes/1.3.0/js/dataTables.searchPanes.min.js"></script>
    <script src="https://cdn.datatables.net/select/1.3.3/js/dataTables.select.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.7.1/js/dataTables.buttons.min.js"></script> -->
<script type="text/javascript">
        $(document).ready(function() {
            $(".load").on("submit", function(){
                stopLoader();
            });//submit
            cek();
            $('.form-control').attr('autocomplete','off');
            $('.form-control').addClass('required');
			$('.form-control').attr('readonly', true);
			$( "#target" ).keyup(function() {
                alert( "Handler for .keyup() called." );
            });
        } );
    function cek(){
        startLoader();
        $.post( "Pengajuan/getData", function( data ) {
            stopLoader();
            // console.log(data)    ;
            $("#table" ).html(data);
            
            $('#example').DataTable( {
            initComplete: function () {
                this.api().columns().every( function () {
                    var column = this;
                } );
            }
        } );
        });
    }
    function addPengajuan(e){
        // startLoader()
        // return;
		if(e > 20){
			alert('Pengajuan Hanya Bisa Diajukan sebelum tanggal 20');
			return;
		}
        $('#jenisNm').html('Pengajuan');
		$('#form-add').trigger("reset");
		$('.input').attr('readonly', false);
		$('#jenis').val('1');
        $('#modal-ket').modal('show');
    }
	function editPengajuan(id){
		$.post( "Pengajuan/getEdit",{id:id},function( data ){
				$('#modal-ket').modal('show');
				if(data[0].jenis == 1){
					$('#jenis').val('3');
				}else if(data[0].jenis == 2){
					$('#jenis').val('4');
				}
				$('.input').attr('readonly', false);
				$('#idp').val(id);
				$('#jenisNm').html('Edit Pengajuan');
				$('#pegawai').val(data[0].nama_pegawai);
				$('#nik').val(data['nik_e']);
				$('#nip').val(data[0].nip);
				$('#jml_is').val(data[0].jml_is);
				$('#jml_an').val(data[0].jml_an);
				$('#penghasilan').val(data[0].penghasilan);
				$('#tunjangan').val(data[0].tunjangan);
				$('#thp').val(data[0].thp);
				$('#dpi').val(data[0].dpi);
				$('#iuran_1').val(data[0].iuran_1);
				$('#iuran_4').val(data[0].iuran_4);
				$('#total_i').val(data[0].total_i);
				$("#bulan").val(data[0].month).change();
			},"json");
	}
	function addMutasi(e){
		if(e > 20){
			alert('Pengajuan Hanya Bisa Diajukan sebelum tanggal 20');
			return;
		}
		$('#jenisNm').html('Mutasi');
		$('#form-add').trigger("reset");
		$('.input').attr('readonly', true);
		$('#nik').attr('readonly', false);
		$('#bulan').attr('readonly', false);
		$('#nik').attr("onkeyup", "search(this.value)");
		$('#jenis').val('2');
		$('#modal-ket').modal('show');
	}
	function  search(e) {
		if (e.length == 16){
			$.post( "Pengajuan/cekNik",{nik:e},function( data ) {
			 //   console.log(data.data.nama_pegawai)
				if(data.status){
						$('#pegawai').val(data.data.nama_pegawai);
				    $('#nip').val(data.data.nip);
				    $('#jml_is').val(data.data.jml_is);
				    $('#jml_an').val(data.data.jml_an);
				    $('#penghasilan').val(data.data.penghasilan);
				    $('#tunjangan').val(data.data.tunjangan);
				    $('#thp').val(data.data.thp);
				    $('#dpi').val(data.data.dpi);
				    $('#iuran_1').val(data.data.iuran_1);
				    $('#iuran_4').val(data.data.iuran_4);
				    $('#total_i').val(data.data.total_i);
				}else{
				    	$('#pegawai').val("");
				        $('#nip').val("");
				        $('#jml_is').val("");
				        $('#jml_an').val("");
				        $('#penghasilan').val("");
				        $('#tunjangan').val("");
				        $('#thp').val("");
				        $('#dpi').val("");
				        $('#iuran_1').val("");
				        $('#iuran_4').val("");
				        $('#total_i').val("");
				}
				
			},"json");
		}
	}
    function Ajukan(){
        var nik = $('#nik').val();
        if ($('#form-add').find('.required').filter(function(){ return this.value === '' }).length > 0) {
				event.preventDefault();
				alert('Silahkan Isi Semua Data');
				return false;
			}
		if(nik.length != 16 ){
		    event.preventDefault();
			alert('NIK Tidak 16 digit');
			return false;
		}
        swal.fire({
					title: "Apakah Semua Data Sudah Benar?",
					text: "Data Yang Disubmit Tidak Bisa Dikembalikan!",
					icon: "warning",
					showCancelButton: true,
					confirmButtonColor: '#DD6B55',
					confirmButtonText: 'Simpan',
					cancelButtonText: "Batal",
					closeOnConfirm: false,
					closeOnCancel: false
				}).then(function(isConfirm) {
                    if (isConfirm.value) {
                        $('#form-add')[0].submit();
                        startLoader();
				// 		$.ajax({
				// 			type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
				// 			url         : '<?= site_url('Pengajuan/addPengajuan')?>', // the url where we want to POST
				// 			data        : new FormData($('#form-add')), // our data object
				// 			dataType    : 'json', // what type of data do we expect back from the server
				// 			encode      : true
				// 		})
				// 		// using the done promise callback
				// 		.done(function(data) {
				// 			if (data.status) {
				// 				alert('success');
				// 				$('#modal-ket').modal('hide');
				// 			}else{
				// 				alert(data.message);
				// 				$('#modal-ket').modal('hide');
				// 			}
				// 		}).fail(function (xhr, textStatus, error) {
				// 			// Handle error here
				// 			alert('warning');
				// 			$('#modal-ket').modal('hide');
				// 		});
					} else {
						Alert("Gagal");
					}
			});
    }
    function hanyaAngka(event) {
        var angka = (event.which) ? event.which : event.keyCode
        if (angka != 46 && angka > 31 && (angka < 48 || angka > 57))
          return false;
        return true;
    }
	function addBukti(id){
// 		console.log(id);
		$('#modal-bukti').modal('show');
		$('#id').val(id);
	}
	function hapusPengajuan(id,j){
	    startLoader();
		$.post( "Pengajuan/delPengajuan",{id:id,j:j},function( data ) {
		    stopLoader();
			alert(data.message);
			$('#modal-anggota').modal('hide');
			cek();
		},"json");
	}
	function sumOtomatis(target,from,input){
		var iFrom = $('#'+from).val();
		if (iFrom == ""){
			iFrom = 0;
		}
		var data = parseInt(input) + parseInt(iFrom);
		$('#'+target).val(data);
		if(data > 1200000){
			var  dpi = data;
		}else{
			var  dpi = parseInt("1200000");
		}
			$('#dpi').val(parseInt(dpi));
		
		var iuaran1 = (parseInt(dpi) * 1) / 100;
		var iuaran4 = (parseInt(dpi) * 4) / 100;
		var total_i = parseInt(iuaran1)+parseInt(iuaran4);
		$('#iuran_1').val(iuaran1);
		$('#iuran_4').val(iuaran4);
		$('#total_i').val(total_i);
	}
	function checkAll(ele,j) {
	    
            const checkboxes = document.getElementsByClassName('ck'+j);
      if (ele.checked) {
          for (var i = 0; i < checkboxes.length; i++) {
              if (checkboxes[i].type == 'checkbox' ) {
                  checkboxes[i].checked = true;
              }
          }
      } else {
          for (var i = 0; i < checkboxes.length; i++) {
              if (checkboxes[i].type == 'checkbox') {
                  checkboxes[i].checked = false;
              }
          }
      }
  }
  	function validasi(e) {
  	    if(e == 1){
		var materi = [];
			$(".ck").each(function(){
				if($(this).is(":checked")){
					materi.push($(this).val());
				}
			});
			materi = materi.toString();
			console.log(typeof materi);
			if(materi == ""){
			   alert('silahkan ceklist data');
			   return;
			}
			startLoader();
			$.ajax({
				url: "Pengajuan/validasiAll",
				method: "POST",
				data: {materi: materi},
				dataType: 'json',
				success: function(data){
				    stopLoader();
					if(data.length > 1){
						alert('data untuk peserta '+data+' tidak dapat disimpan');
					}else{
						alert('success validasi');
					}
					cek();
				}
			})
  	    }else if(e==2){
  	        	var materi = [];
			$(".ck2").each(function(){
				if($(this).is(":checked")){
					materi.push($(this).val());
				}
			});
			materi = materi.toString();
			console.log(typeof materi);
			if(materi == ""){
			   alert('silahkan ceklist data');
			   return;
			}
			startLoader();
			$.ajax({
				url: "Pengajuan/validasiAllAnggota",
				method: "POST",
				data: {materi: materi},
				dataType: 'json',
				success: function(data){
				    stopLoader();
					if(data.length > 1){
						alert('data untuk peserta '+data+' tidak dapat disimpan');
					}else{
						alert('success validasi');
					}
				    $('#modal-anggota').modal('hide');
				}
			})
  	    }
 	}
	function Tolak(e,j){
	    $('#modal-anggota').modal('hide');
		$('#id_tolak').val(e);
		$('#jenis_tolak').val(j);
		$('#modal-Tolak').modal('show');
	}
	function getTolak(id,j){
		$.post( "Pengajuan/getTolak",{id:id,j:j},function(data) {
			if(data.length){
				$('#modal-anggota').modal('hide');
				$('#modal-Tolak').modal('show');
				$('#text_tolak').val(data[0].catatan);
			}
		},"json");
	}
	
	function Anggota(){
	    $.post( "Pengajuan/getAnggota",function(data) {
	        $('#body-modal-anggota').html(data);
	        $('#modal-anggota').modal('show');
	        $('#exampleanggota').DataTable( {
            initComplete: function () {
                this.api().columns().every( function () {
                    var column = this;
                } );
            }
        } );
		});
	}

</script>
<div class="modal fade" id="modal-ket" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                      <div class="modal-dialog modal-dialog-full-width" role="document">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h5 class="modal-title">Tambah <b id="jenisNm">Pengajuan</b> Peserta</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <div class="modal-body" id='body-modal'>
                          <form id="form-add" action="<?= site_url('Pengajuan/addPengajuan')?>" method="POST" enctype="multipart/form-data"> 
                          <div class="row col-lg-12">
                            <div class="col-lg-4">                                
                                <div class="form-group">
                                    <label>Nama Pegawai</label>
                                    <input id="pegawai" class="form-control input" type="text" name="pegawai">
									<input type="hidden" name="jenis" id="jenis">
									<input type="hidden" name="idp" id="idp">
                                </div>                                 
                                <div class="form-group">
                                    <label>NIK</label>
                                    <input id="nik" onkeypress="return hanyaAngka(event)" class="form-control input" minlength="16" maxlength="16" type="text" name="nik">
                                </div>                                 
                                <div class="form-group">
                                    <label>NIP</label>
                                    <input id="nip" class="form-control input" type="number" name="nip">
                                </div>                                 
                                <div class="form-group">
                                    <label>Jumlah Istri</label>
                                    <input id="jml_is" class="form-control input" type="number" name="jml_is">
                                </div>                                 
                                <div class="form-group">
                                    <label>Jumlah Anak</label>
                                    <input id="jml_an" class="form-control input" type="number" name="jml_an">
                                </div>
                            </div>
                            <div class="col-lg-4">                                
                                <div class="form-group">
                                    <label>Penghasilan</label>
                                    <input id="penghasilan" class="form-control input" type="number" name="penghasilan" onfocusout="sumOtomatis('thp','tunjangan',this.value)">
                                </div>                                 
                                <div class="form-group">
                                    <label>Tunjangan</label>
                                    <input id="tunjangan" class="form-control input" type="number" name="tunjangan" onfocusout="sumOtomatis('thp','penghasilan',this.value)">
                                </div>                                 
                                <div class="form-group">
                                    <label>thp</label>
                                    <input id="thp" class="form-control" type="number" name="thp">
                                </div>                                 
                                <div class="form-group">
                                    <label>dpi</label>
                                    <input id="dpi" class="form-control" type="number" name="dpi">
                                </div>                                 
                                <div class="form-group">
                                    <label>iuran_1</label>
                                    <input id="iuran_1" class="form-control" type="number" name="iuran_1">
                                </div>
                            </div>
                            <div class="col-lg-4">                                                              
                                <div class="form-group">
                                    <label>Iuran_4</label>
                                    <input id="iuran_4" class="form-control" type="number" name="iuran_4">
                                </div>                                 
                                <div class="form-group">
                                    <label>total_i</label>
                                    <input id="total_i" class="form-control" type="number" name="total_i">
                                </div>                                 
                                <div class="form-group">
                                    <label for="exampleInputEmail1">UNGGAH FILE BUKTI (pdf/png/jpg)<label>
                                    <input type="file" name="bukti" class="form-control input" required>
                                </div>
                            </div>
                            <button onclick="Ajukan()"  type="button" class="btn btn-primary">Submit Button</button>
                                <button type="reset" class="btn btn-default">Reset Button</button>
                            </form>
                          </div>
                        </div>
                      </div>
</div> 
</div> 
<div class="modal fade" id="modal-anggota" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-full-width" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Pengajuan Anggota Keluarga</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        	<?php if($this->session->level == "1") {?>
        <div class="col-md-4">
            <button onclick='validasi(2)' class="btn btn-success">Validasi Persetujuan</button>
        </div>
        <?php }?>
        <div class="modal-body" id='body-modal-anggota'>
        </div>
      </div>
    </div>
</div>  
                    