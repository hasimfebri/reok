<?php $this->session->unset_userdata("page");
$params = array(
                        'page' => "Download Bukti Bayar"
                    );
        $this->session->set_userdata($params);
 ?>                   
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <h4>Pembayaran 1%</h4><br>
                                <table id="example" class="display responsive nowrap" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>Nominal</th>
                                            <th>Bulan</th>
                                            <th>Tgl Bayar</th>
                                            <th>Bukti</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $no=1; foreach ($row->result() as $key => $value) {?>
                                        <tr>
                                            <td><?php echo "Rp " . number_format($value->nominal, 2, ",", "."); ?></td>
                                            <td><?php $yrdata= strtotime($value->bulan); echo  date('F', $yrdata)  ?></td>
                                            <td><?= $value->tgl_bayar ?></td>
                                            <td><a href="<?=base_url('uploads/bb/'.$value->bukti)?>" class="btn btn-primary btn-sm m-b-10 m-l-5" target="_blank">Lihat</a></td>
                                            <td><?php if ($value->status == 0) {?>
                                                <?php if ($this->session->level == 1){?>
                                                    <a href="<?=base_url('BuktiBayar/validsatu/'.$value->id_satu)?>" class="btn btn-success btn-sm m-b-10 m-l-5">Setuju </a> 
                                                    <!-- modal klik --> 
                                                    <a data-toggle="modal" style="color:white" data-target="#modal-tolaksatu" id="tolaksatu"  class="btn btn-danger     btn-sm m-b-10 m-l-5" data-id-satu = "<?=$value->id_satu ?>">Tolak</a>
                                                <?php } else { ?>
                                                    Menunggu Validasi
                                                <?php } ?>
                                                <?php }else if($value->status == 2){ ?>
                                                    Ditolak
                                                <?php } else{ ?>
                                                    Sudah Valid
                                                <?php } ?>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>Nominal</th>
                                            <th>Bulan</th>
                                            <th>Tgl Bayar</th>
                                            <th>Bukti</th>
                                            <th>Action</th>
                                        </tr>
                                    </tfoot>
                            </table>         
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="card">
                                <h4>Pembayaran 4%</h4><br>
                                <table id="example2" class="display responsive nowrap" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>Nominal</th>
                                            <th>Bulan</th>
                                            <th>Tgl Bayar</th>
                                                <th>Bukti</th>
                                                <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $no=1; foreach ($rowe->result() as $key => $val) {?>
                                        <tr>
                                            <td><?php echo "Rp " . number_format( $val->nominal, 2, ",", "."); ?></td>
                                            <td><?php $yrdatae= strtotime($val->bulan); echo  date('F', $yrdatae)  ?></td>
                                            <td><?= $val->tgl_bayar ?></td>
                                            <td><a href="<?=base_url('uploads/bb/'.$val->bukti)?>" class="btn btn-primary btn-sm m-b-10 m-l-5" target="_blank">Lihat</a></td>
                                            <td><?php if ($val->status == 0) {?>
                                                <a href="<?=base_url('BuktiBayar/validempat/'.$val->id_empat)?>" class="btn btn-success btn-sm m-b-10 m-l-5">Setuju</a><a data-toggle="modal" data-target="#modal-tolakempat" id="tolakempat"  class="btn btn-danger btn-sm m-b-10 m-l-5" data-id-empat = "<?=$val->id_empat ?>" style="color:white">Tolak</a>
                                                <?php }else if($val->status == 2){ ?>
                                                    Ditolak
                                                <?php } else{ ?>
                                                Sudah Valid
                                                <?php } ?>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>Nominal</th>
                                            <th>Bulan</th>
                                            <th>Tgl Bayar</th>
                                                <th>Bukti</th>
                                                <th>Action</th>
                                        </tr>
                                    </tfoot>
                            </table>         
                            </div>
                        </div>
                    </div>
                    <!-- modal -->
                        <div class="modal fade" id="modal-tolakempat" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label = "Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
<form action="<?=site_url('BuktiBayar/rejectempat');?>" method="post" >
    <div class="form-group">
                        <input type="hidden" name="idempat" id="idempat">
                                        <div class="form-group">
                                            <div class="form-group col-lg-12">
                                                 <textarea class="form-control" rows="4" cols="50" name="remark4" placeholder="Catatan"></textarea>
                                            </div>
                                        </div>
    </div>
                        <button type="submit" class="btn btn-success" style="margin-right: 5px" >Oke</button>
                    </form>
        </div>
        </div>
    </div>
    </div>
                            <div class="modal fade" id="modal-tolaksatu" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label = "Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
<form action="<?=site_url('BuktiBayar/rejecsatu');?>" method="post" >
    <div class="form-group">
                        <input type="hidden" name="idsatu" id="idsatu">
                                        <div class="form-group">
                                            <div class="form-group col-lg-12">
                                                 <textarea class="form-control" rows="4" cols="50" name="remark1" placeholder="Catatan"></textarea>
                                            </div>
                                        </div>
                    </div>
                        <button type="submit" class="btn btn-success" style="margin-right: 5px" >Oke</button>
                    </form>
        </div>
        </div>
    </div>
    </div>