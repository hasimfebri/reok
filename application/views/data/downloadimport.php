                    <div class="row">
                        <div class="col-lg-6">
                            <div class="card">
                                <div class="card-title">
                                    <h4>Data Import Satker</h4>
                                    
                                </div>
                                <hr>
                                <table id="example" class="display responsive nowrap" style="width:100%">
                                    <thead>
                                        <tr text-align="center">
                                            <th>No</th>
                                            <th>Bulan</th>
                                            <th style="max-width: 100px;">Validasi</th>
                                            <th>action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $no=1; foreach ($satker->result() as $key => $value) {?>
                                        <tr>
                                            <td><?= $no++ ?></td>
                                            <td><?php $yrdata= strtotime($value->bulan); echo  date('F', $yrdata)  ?></td>

                                            <?php if ($value->status == 1) {?>
                                                <td style="max-width: 100px;float:center;" text-align="center">Disetujui</td>
                                            <?php } else if($value->status == 2) {?>
                                                <td style="max-width: 100px;float:center;" text-align="center">Ditolak</td>
                                            <?php } else {
                                                if ($this->session->level == "2") { ?>
                                                    <td style="max-width: 100px;float:center;" text-align="center">Menunggu</td>
                                                <?php } else { ?>
                                                    <td style="max-width: 100px;" text-align="center"><span style="float:center;">
                                                     <button type="button" onclick="modalsetuju('<?= $value->bulan ?>','<?= $value->periode ?>')" id="ketmodal" class="btn btn-success fa fa-check" style="font-size:12px"></button>
                                                        </span>
                                                        <span style="float:right;">
                                                     <button type="button" onclick="modaltolak('<?= $value->bulan ?>','<?= $value->periode ?>')" id="ketmodal" class="btn btn-danger fa fa-close" style="font-size:12px"></button>
                                                        </span>
                                                    </td>
                                                <?php } ?>
                                            <?php }?>
                                            <td><form method="POST" action="<?= site_url('import/exsatker')?>"><input type="hidden" name="xsatker" value="<?= $value->bulan?>" /> <button type="submit" class="btn btn-danger btn-sm m-b-10 m-l-5">Export</button></form></td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                    <tfoot>
                                        <tr text-align="center">
                                            <th>No</th>
                                            <th>Bulan</th>
                                            <th style="max-width: 100px;">Validasi</th>
                                            <th>Action </th>
                                        </tr>
                                    </tfoot>
                            </table>         
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="card">
                                <div class="card-title">
                                    <h4>Data Import BPJS</h4>
                                    
                                </div>
                                <hr>
                                <table id="example2" class="display responsive nowrap" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Bulan</th>
                                            <th>action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $no=1; foreach ($bpjs->result() as $key => $val) {?>
                                        <tr>
                                            <td><?= $no++ ?></td>
                                            <td><?php $yrdatae= strtotime($val->bulan); echo  date('F', $yrdatae)  ?></td>
                                            <td><form method="POST" action="<?= site_url('import/exbpjs')?>"><input type="hidden" name="xbpjs" value="<?= $val->bulan?>" /> <button type="submit" class="btn btn-danger btn-sm m-b-10 m-l-5">Export</button></form></td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>No</th>
                                            <th>Bulan</th>
                                            <th>Action</th>
                                        </tr>
                                    </tfoot>
                            </table>         
                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="modal-ket" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-sm">
                            <div class="card">
                            <div class="modal-content">
                            <div class="modal-header col-lg-12">
                                <h5 class="modal-title col-lg-10">Validasi Data</h5>
                                <button type="button" class="close col-lg-2" data-dismiss="modal" aria-label = "Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                
                            </div>
                            <div class="modal-body col-lg-12">
                                    <form action="<?=site_url('Import/setuju');?>" method="post">
                                        <input type="hidden" name="periode" id="periode">
                                        <div class="form-group">
                                            <div class="form-group col-lg-12">
                                                 <input type="text" name="bulan" id="bulan" readonly>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="form-group col-lg-12">
                                                 <textarea class="form-control" rows="4" cols="50" name="remark" placeholder="Catatan"></textarea>
                                            </div>
                                        </div>
                                            <button type="submit" class="btn btn-success" style="margin-right: 5px" >Save</button>
                                        </form>
                            </div>
                            </div>
                        </div>
                        </div>
                    </div>
                    <div class="modal fade" id="modal-ket2" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-sm">
                            <div class="card">
                            <div class="modal-content">
                            <div class="modal-header col-lg-12">
                                <h5 class="modal-title col-lg-10">Validasi Data</h5>
                                <button type="button" class="close col-lg-2" data-dismiss="modal" aria-label = "Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                
                            </div>
                            <div class="modal-body col-lg-12">
                                    <form action="<?=site_url('Import/tolak');?>" method="post">
                                        <input type="hidden" name="periode2" id="periode2">
                                        <div class="form-group">
                                            <div class="form-group col-lg-12">
                                                 <input type="text" name="bulan2" id="bulan2" readonly>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="form-group col-lg-12">
                                                 <textarea class="form-control" rows="4" cols="50" name="remark2" placeholder="Catatan"></textarea>
                                            </div>
                                        </div>
                                            <button type="submit" class="btn btn-success" style="margin-right: 5px" >Save</button>
                                        </form>
                            </div>
                            </div>
                        </div>
                        </div>
                    </div>
<script type="text/javascript">
    function modalsetuju(bulan,periode){
        $('#bulan').val(bulan);
        $('#periode').val(periode);
        $('#modal-ket').modal('show');
    }
    function modaltolak(bulan,periode){
        $('#bulan2').val(bulan);
        $('#periode2').val(periode);
        $('#modal-ket2').modal('show');
    }

</script>