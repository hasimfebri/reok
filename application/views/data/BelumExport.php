<?php $this->session->unset_userdata("page");
$params = array(
                        'page' => "Belum Export"
                    );
        $this->session->set_userdata($params);
 ?>
            <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <?php echo $this->session->flashdata('notif') ?>
                    <h4> 
                        <form role="form" action="" method="post">
                            <div class="row col-lg-12">
                                <div class="form-group col-lg-4">
                                        <label>DARI BULAN</label>
                                    <div class='input-group date'>
                                        <input type="text" class="form-control" value="<?=set_value('fromdate')?>" name="fromdate"  id='datepicker' autocomplete="off" required>
                                    </div>
                                </div>
                                <div class="form-group col-lg-4">
                                        <label>SAMPAI BULAN</label>
                                    <div class='input-group date'>
                                        <input type="text" class="form-control" value="<?=set_value('todate')?>" name="todate"  id='datepicker2' autocomplete="off" required>
                                    </div>
                                </div>
                                    <div style="padding-top: 5%;">
                                            <button type="submit" name='cari' class="btn btn-primary">Cari</button>
                                            <button type="button" class="btn" onclick="backserma()">Refresh</button>
                                    </div>
                            </div>
                                </form>
                    </h4>
                             <table id="example2" class="display responsive nowrap" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>cek</th>
                                            <th>Nama Satker</th>
                                            <th>Nama Kepala</th>
                                            <th>No HP Satker</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $no=1; foreach ($row->result() as $key => $value) {?>
                                        <tr>
                                            <td><?= $no++ ?></td>
                                            <td><span style="float:right;">
                    <button type="button" onclick="modalgas('<?= $value->username ?>')" id="ketmodal" class="fa fa-eye" style="font-size:24px"></button>
                </span></td>
                                            <td><?= $value->nama ?></td>
                                            <td><?= $value->nama_kepala ?></td>
                                            <td><?= $value->no_hp_satker ?></td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                            </table>          
                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="modal-ket" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h5 class="modal-title">Detail Satker EXPORT DATA</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <div class="modal-body" id='body-modal'>
                            <table id="example3" class="display responsive nowrap" style='color:black;font-size:100%;width: 100%;'>
                                <thead>
                                <th style='text-align:center; vertical-align:middle'>Satker</th>
                                <th style='text-align:center; vertical-align:middle'>Bulan</th>
                                </thead>
                                <tbody id='dataya'>
                                    
                                </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                    </div>
                
<script type="text/javascript">
    function modalgas(e){
        $('#dataya tr').remove();
        content = '';
        var date = $('#datepicker2').val();
       $.post( "BelumExport/cekSatkerExport", { id:e,date:date}, function( data ) {
                // console.log(data.length);
                for(x = 0; x < data.length ; x++){
                content +=`<tr >`;
                content +=`<td style='text-align:center; vertical-align:middle' >`+data[x].nama+`</td>`; 
                content +=`<td style='text-align:center; vertical-align:middle'>`+data[x].bulan1+`</td>`;
                content +=`</tr>`;
                }
                // console.log(content);
                $('#dataya').append(content);
                $('#modal-ket').modal('show');
            }, "json");
       // $('#modal-ket').modal('show');
    }
</script>