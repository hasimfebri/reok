<?php $this->session->unset_userdata("page");
$params = array(
                        'page' => "Import Data"
                    );
        $this->session->set_userdata($params);
 ?>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <?php echo $this->session->flashdata('notif') ?>
                    <form method="POST" action="<?= site_url('import/upload') ?>" enctype="multipart/form-data">                                
                <div class="form-group">
                    <label>Pilih Bulan</label>
                    <select name="bulan" class="form-control" required>
                        <option ></option>
                        <option value="01">Januari</option>
                        <option value="02">Februari</option>
                        <option value="03">Maret</option>
                        <option value="04">April</option>
                        <option value="05">Mei</option>
                        <option value="06">Juni</option>
                        <option value="07">Juli</option>
                        <option value="08">Agustus</option>
                        <option value="09">September</option>
                        <option value="10">Oktober</option>
                        <option value="11">November</option>
                        <option value="12">Desember</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>Tahun</label>
                    <input type="hidden" name="tahun" value="<?= $this->session->tahun ?>">
                    <input class="form-control" type="text" value="<?= $this->session->tahun ?>" readonly="">
                </div>
                <div class="form-group">
                <label for="exampleInputEmail1">UNGGAH FILE EXCEL</label>
                <input type="file" name="userfile" class="form-control" required>
              </div>

              <button type="submit" class="btn btn-success">UPLOAD</button>
            </form>          
                            </div>
                        </div>
                    </div>                    
                    