<!DOCTYPE html>
<html lang="en">
<style type="text/css">
.count-notif{
  vertical-align:middle;
  margin-right: : 150px;
  margin-bottom: : 30px;
  font-size:13px;
}

.badge-primary{
  padding:1px;
}
.alert-box{
  background: #FFFFFF;
  height:100vh;
  display: none;
  z-index: 1900;
  text-align:  center;
  vertical-align: middle !important;
  overflow: hidden;
  color:  white;
  position: absolute; top: 0; bottom: 0;right: 0;left: 0;
  /*margin-top: 150px;*/
 }
#overlay{
    /*display: flex;*/
    /*align-items: center;*/
   /* display: block; */
   /* width: 100%;*/
   /* heigth: 100%;*/
   /* width: 300px;*/
   /*height: 300px;*/
   /*position: absolute;*/
   /*left: 50%;*/
   /*top: 50%; */
   /*margin-left: -150px;*/
   margin-top: 350px;
}
#pageloader
{
  background: #FFFFFF;
  display: none;
  height: 100%;
  position: fixed;
  width: 100%;
  z-index: 9999;
}

#pageloader img
{
  left: 43%;
  margin-left: -32px;
  margin-top: -32px;
  position: absolute;
  top: 30%;
}
</style>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>TIDAR</title>

    <!-- ================= Favicon ================== -->
    <!-- Standard -->
    <link rel="shortcut icon" href="http://placehold.it/64.png/000/fff">
    <!-- Retina iPad Touch Icon-->
    <link rel="apple-touch-icon" sizes="144x144" href="http://placehold.it/144.png/000/fff">
    <!-- Retina iPhone Touch Icon-->
    <link rel="apple-touch-icon" sizes="114x114" href="http://placehold.it/114.png/000/fff">
    <!-- Standard iPad Touch Icon-->
    <link rel="apple-touch-icon" sizes="72x72" href="http://placehold.it/72.png/000/fff">
    <!-- Standard iPhone Touch Icon-->
    <link rel="apple-touch-icon" sizes="57x57" href="http://placehold.it/57.png/000/fff">

    <!-- Styles -->
    <link href="<?php echo base_url()?>assets/css/lib/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo base_url()?>assets/css/lib/themify-icons.css" rel="stylesheet">
    <link href="<?php echo base_url()?>assets/css/lib/chartist/chartist.min.css" rel="stylesheet">
    <link href="<?php echo base_url()?>assets/css/lib/menubar/sidebar.css" rel="stylesheet">
    <link href="<?php echo base_url()?>assets/css/lib/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url()?>assets/css/bootstrap-datepicker.css" rel="stylesheet">
    <link href="<?php echo base_url()?>assets/css/lib/helper.css" rel="stylesheet">
    <link href="<?php echo base_url()?>assets/css/style.css" rel="stylesheet">    
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/searchpanes/1.3.0/css/searchPanes.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/select/1.3.3/css/select.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.7.1/css/buttons.dataTables.min.css">
</head>

<body>
<!--<div class="alert-box">-->
<!--    <div id="overlay2" data-role="page" data-add-back-btn="false">-->
<!--        <div id="overlay"><img src="<?= site_url('assets/images/loading.gif')?>"/></div>-->
<!--    </div>-->
<!--</div>-->
<div id="pageloader">
   <img src="<?= site_url('assets/images/loading.gif')?>" alt="processing..." />
</div>
        <div class="sidebar sidebar-hide-to-small sidebar-shrink sidebar-gestures">
            <div class="nano">
                <div class="nano-content">
                    <div class="logo"><a href="<?=site_url('Dashboard') ?>"><!-- <img src="<?php echo base_url()?>assets/images/logo.png" alt="" /> --><span>TIDAR</span></a>
                    </div>
                    <ul>
                        <li class="label">Home</li>
                        <?php if($this->session->level == "1" || $this->session->level == "3") { ?>
                            <li><a href="<?=site_url('satker/clear') ?>"><i class="ti-home"></i> Home </a></li>
                        <?php } else { 
                            $notif = ($this->fungsi->countdata() + $this->fungsi->countdata1() + $this->fungsi->countdata4()) ?>
                            <li class="nav-item">
                                <a  href="<?=site_url('Remark')  ?>"><i class="ti-alert"></i>Remark<span class="badge badge-pill badge-primary count-notif" style="float:right;"><?php echo $notif ?></span> <span class="sr-only">(current)</span></a>
                            </li>
                       <?php }?>
                        <li><a href="<?=site_url('dashboard') ?>"><i class="ti-dashboard"></i> Dashboard </a></li>

                        <li class="label">Main</li>
						<li><a class="sidebar-sub-toggle"><i class="ti-user"></i>Kepesertaan<span class="sidebar-collapse-icon ti-angle-down"></span></a>
                            <ul>
                            	<li><a href="<?=site_url('Pengajuan') ?>">Pengajuan</i></a></li>
								<li><a href="<?=site_url('Pegawai') ?>">Pegawai</i></a></li>
                            </ul>
                        </li>
                        <!--<li><a href="<?=site_url('import')  ?>"><i class="ti-import"></i> Import Excel</a></li>-->
                        <!--<li><a href="<?=site_url('import/data')  ?>"><i class="ti-export"></i> Export Excel</a></li>-->
                        <?php if($this->session->level == "1" || $this->session->level == "3") { ?>
                        <li><a href="<?=site_url('BuktiBayar/download')  ?>"><i class="ti-cloud-down"></i> Validasi Bukti Bayar</a></li>
                        <?php } ?>
                        <?php if ($this->session->level == "2"): ?>
                        <li><a href="<?=site_url('BuktiBayar')  ?>"><i class="ti-cloud-up"></i> Upload Bukti Bayar</a></li>
                        <?php endif ?>
                        <li><a href="<?=site_url('sk')  ?>"><i class="ti-files"></i> SK</a></li>
                        <li><a href="<?=site_url('ba')  ?>"><i class="ti-printer"></i> Cetak BA</a></li>
                        <li><a href="<?=site_url('KertasKerja')  ?>"><i class="ti-pencil-alt"></i> Download Kertas Kerja</a></li>
                        <li><a href="<?=site_url('qna')  ?>"><i class="ti-help"></i> QNA</a></li>
                        <li class="label">Menu</li>
                        <li><a class="sidebar-sub-toggle"><i class="ti-layout"></i> Periode <span class="sidebar-collapse-icon ti-angle-down"></span></a>
                            <ul>
                            	<?php $dt = $this->fungsi->periode(); ?>
                            	<?php foreach ($dt->result() as $key => $pd) { ?>
                            		<li><a href="<?=site_url('periode/change/') ?><?= $pd->id_periode; ?>"><?= $pd->tahun; ?></a></li>
                            	<?php } ?>
                            </ul>
                        </li>
                        <?php if ($this->session->level == "2"): ?>
                            <li><a href="<?=site_url('profile')  ?>"><i class="ti-info-alt"></i> Profile</a></li>
                            <li><a href="<?=site_url('changepass')?>"><i class="ti-lock"></i> Change Password</a></li>
                        <?php endif ?>
                        
                        
                        <li><a href="<?=site_url('auth/logout') ?>"><i class="ti-close"></i> Logout</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- /# sidebar -->


    <div class="header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="float-left">
                        <div class="hamburger sidebar-toggle">
                            <span class="line"></span>
                            <span class="line"></span>
                            <span class="line"></span>
                        </div>
                    </div>
                    <div class="float-right">
                        <div class="dropdown dib">
                            <div class="header-icon">
                                <span class="user-avatar">
                        <?php if ($this->session->level =="1") {?>
                        <?php echo  $this->session->satker; ?>||
                        <?php } ?>  <?= $this->fungsi->user_login()->username ?>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <div class="content-wrap">
        <div class="main">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-8 p-r-0 title-margin-right">
                        <div class="page-header">
                            <div class="page-title">
                                <h1>Hello, <?= $this->fungsi->user_login()->nama ?></h1>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                    <div class="col-lg-4 p-l-0 title-margin-left">
                        <div class="page-header">
                            <div class="page-title">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="<?=site_url('dashboard') ?>">Dashboard</a></li>
                                    <li class="breadcrumb-item active"><?php echo $this->session->page; ?></li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                </div>
                <!-- /# row -->
                <section id="main-content">
                	<?php echo $contents; ?>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="footer">
                                <p>2018 © Admin Board. - <a href="#">example.com</a></p>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        </div>
    </div>







    
    <!-- jquery vendor -->
    <script src="<?php echo base_url()?>assets/js/lib/jquery.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/lib/jquery.nanoscroller.min.js"></script>
    <!-- nano scroller -->
    <script src="<?php echo base_url()?>assets/js/lib/menubar/sidebar.js"></script>
    <script src="<?php echo base_url()?>assets/js/lib/preloader/pace.min.js"></script>
    <!-- sidebar -->
    
    <!-- bootstrap -->
    <script src="<?php echo base_url()?>assets/js/lib/bootstrap.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/scripts.js"></script>
    <script src="<?php echo base_url()?>assets/js/bootstrap-datepicker.js"></script>
    <!-- scripit init-->
    <script src="https://cdn.jsdelivr.net/npm/chart.js@3.3.0/dist/chart.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/searchpanes/1.3.0/js/dataTables.searchPanes.min.js"></script>
    <script src="https://cdn.datatables.net/select/1.3.3/js/dataTables.select.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.7.1/js/dataTables.buttons.min.js"></script>
 	<script type="text/javascript">
 		$(document).ready(function() {
	    $('#example').DataTable( {
	        initComplete: function () {
	            this.api().columns().every( function () {
	                var column = this;
	                var select = $('<select><option value=""></option></select>')
	                    .appendTo( $(column.footer()).empty() )
	                    .on( 'change', function () {
	                        var val = $.fn.dataTable.util.escapeRegex(
	                            $(this).val()
	                        );
	 
	                        column
	                            .search( val ? '^'+val+'$' : '', true, false )
	                            .draw();
	                    } );
	 
	                column.data().unique().sort().each( function ( d, j ) {
	                    select.append( '<option value="'+d+'">'+d+'</option>' )
	                } );
	            } );
	        }
	    } );
	} );
 	</script>
        <script type="text/javascript">
        $(document).ready(function() {
        $('#example2').DataTable( {
            initComplete: function () {
                this.api().columns().every( function () {
                    var column = this;
                    var select = $('<select><option value=""></option></select>')
                        .appendTo( $(column.footer()).empty() )
                        .on( 'change', function () {
                            var val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                            );
     
                            column
                                .search( val ? '^'+val+'$' : '', true, false )
                                .draw();
                        } );
     
                    column.data().unique().sort().each( function ( d, j ) {
                        select.append( '<option value="'+d+'">'+d+'</option>' )
                    } );
                } );
            }
        } );
    } );
    </script>
    <script type="text/javascript">
   $(document).ready(function() {
    $('#example3').DataTable({
        buttons: [
            'searchPanes'
        ],
        dom: 'Bfrtip',
        columnDefs: [
            {
                searchPanes: {
                    show: true
                },
                targets: [1]
            },
            {
                searchPanes: {
                    show: false
                },
                targets: [0]
            },
            {
                searchPanes: {
                    show: false
                },
                targets: [2]
            },
            {
                searchPanes: {
                    show: false
                },
                targets: [3]
            },
            {
                searchPanes: {
                    show: false
                },
                targets: [4]
            },
            {
                searchPanes: {
                    show: false
                },
                targets: [5]
            },
            {
                searchPanes: {
                    show: false
                },
                targets: [6]
            },
            {
                searchPanes: {
                    show: false
                },
                targets: [7]
            },
            {
                searchPanes: {
                    show: false
                },
                targets: [8]
            }
        ]
    });
});
    </script>
 		<script>
 			$(document).ready(function() {
	var ctx = document.getElementById('myChart');
    if (ctx!=null) {
	var myChart = new Chart(ctx, {
	    type: 'bar',
	    data: {
	        labels: bln,
	        datasets: [{
	            label: '# of Votes',
	            data: hsl,
	            backgroundColor: [
	                'rgba(255, 99, 132, 0.2)',
	                'rgba(54, 162, 235, 0.2)',
	                'rgba(255, 206, 86, 0.2)',
	                'rgba(75, 192, 192, 0.2)',
	                'rgba(153, 102, 255, 0.2)',
	                'rgba(255, 159, 64, 0.2)'
	            ],
	            borderColor: [
	                'rgba(255, 99, 132, 1)',
	                'rgba(54, 162, 235, 1)',
	                'rgba(255, 206, 86, 1)',
	                'rgba(75, 192, 192, 1)',
	                'rgba(153, 102, 255, 1)',
	                'rgba(255, 159, 64, 1)'
	            ],
	            borderWidth: 1
	        }]
	    },
	    options: {
	        scales: {
	            y: {
	                beginAtZero: true
	            }
	        }
	    }
	});
    }
	} );
	</script>
	<script type="text/javascript">
	function startLoader(){
 		     //$('.alert-box').show();
 		     $("#pageloader").fadeIn();
    }
    function stopLoader(){
 		     //$('.alert-box').hide();
 		     $("#pageloader").fadeOut();
    }
		$('#sandbox-container input').datepicker({
		    format: "yyyy-mm-dd",
		    autoclose: true
		});
	</script>
	<script>
        $(document).ready(function(){
            $(document).on('click', '#tolakempat', function(){
                var id = $(this).data('id-empat');
                $('#idempat').val(id);
        });
            $(document).on('click', '#tolaksatu', function(){
                var id = $(this).data('id-satu');
                $('#idsatu').val(id);
        });
    })
    </script>
    <script type="text/javascript">
		$('#sandbox-container input').datepicker({
		    format: "yyyy-mm-dd",
		    autoclose: true
		});
        $('#datepicker').datepicker({
            viewMode: 'years',
            format: "yyyymm",
            autoclose: true
        });
        $('#datepicker2').datepicker({
            viewMode: 'years',
            format: "yyyymm",
            autoclose: true
        });
         $('.datepicker3').datepicker({
            viewMode: 'years',
            format: "yyyymm",
            autoclose: true
        });
          $('.datepicker4').datepicker({
            viewMode: 'years',
            format: "yyyy-m-d",
            autoclose: true
        });
		$(".js-example-responsive").select2({
            width: 'resolve' // need to override the changed default
            });
	</script>
</body>

</html>
