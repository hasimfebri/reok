<!DOCTYPE html>
<html lang="en">
<style type="text/css">
#pageloader
{
  background: #FFFFFF;
  display: none;
  height: 100%;
  position: fixed;
  width: 100%;
  z-index: 9999;
}

#pageloader img
{
  left: 43%;
  margin-left: -32px;
  margin-top: -32px;
  position: absolute;
  top: 30%;
}
</style>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>TIDAR</title>

    <!-- ================= Favicon ================== -->
    <!-- Standard -->
    <link rel="shortcut icon" href="http://placehold.it/64.png/000/fff">
    <!-- Retina iPad Touch Icon-->
    <link rel="apple-touch-icon" sizes="144x144" href="http://placehold.it/144.png/000/fff">
    <!-- Retina iPhone Touch Icon-->
    <link rel="apple-touch-icon" sizes="114x114" href="http://placehold.it/114.png/000/fff">
    <!-- Standard iPad Touch Icon-->
    <link rel="apple-touch-icon" sizes="72x72" href="http://placehold.it/72.png/000/fff">
    <!-- Standard iPhone Touch Icon-->
    <link rel="apple-touch-icon" sizes="57x57" href="http://placehold.it/57.png/000/fff">

    <!-- Styles -->
    <link href="<?php echo base_url()?>assets/css/lib/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo base_url()?>assets/css/lib/themify-icons.css" rel="stylesheet">
    <link href="<?php echo base_url()?>assets/css/lib/chartist/chartist.min.css" rel="stylesheet">
    <link href="<?php echo base_url()?>assets/css/lib/menubar/sidebar.css" rel="stylesheet">
    <link href="<?php echo base_url()?>assets/css/lib/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url()?>assets/css/bootstrap-datepicker.css" rel="stylesheet">
    <link href="<?php echo base_url()?>assets/css/lib/helper.css" rel="stylesheet">
    <link href="<?php echo base_url()?>assets/css/style.css" rel="stylesheet">    
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.7/css/responsive.dataTables.min.css">
</head>

<body>
<div id="pageloader">
   <img src="<?= site_url('assets/images/loading.gif')?>" alt="processing..." />
</div>
        <div class="sidebar sidebar-hide-to-small sidebar-shrink sidebar-gestures">
            <div class="nano">
                <div class="nano-content">
                    <div class="logo"><a href="index.html"><!-- <img src="<?php echo base_url()?>assets/images/logo.png" alt="" /> --><span>TIDAR</span></a></div>
                    <ul>
                        <li class="label">Home</li>
                        <li><a href="<?=site_url('dashboard') ?>"><i class="ti-dashboard"></i> Dashboard </a></li>

                        <li class="label">Main</li>
                        <?php if ($this->session->level == 3){?>
                        <li><a class="sidebar-sub-toggle"><i class="ti-user"></i> User <span class="sidebar-collapse-icon ti-angle-down"></span></a>
                            <ul>
                            <!--<li><a href="<?=site_url('satker')?>">Satker</a></li>-->
                            <li><a href="<?=site_url('admin')?>">Admin</a></li>
                            </ul>
                        </li>
                        <?php } ?>
                        <li><a class="sidebar-sub-toggle"><i class="ti-target"></i>Satker <span class="sidebar-collapse-icon ti-angle-down"></span></a>
                            <ul>
                            <li><a href="<?=site_url('satker')?>">Satker</a></li>
                            <!--<li><a href="<?=site_url('admin')?>">Admin</a></li>-->
                            </ul>
                        </li>
                        <!-- <li><a class="sidebar-sub-toggle"><i class="ti-file"></i>Cek Upload Satker <span class="sidebar-collapse-icon ti-angle-down"></span></a>
                            <ul>
                            <li><a href="<?=site_url('BelumExport')?>">Data Pegawai</a></li>
                            <li><a href="<?=site_url('BelumBuktiBayar')?>">Bukti Bayar</a></li>
                            </ul>
                        </li> -->
                        <li><a href="<?=site_url('qna')  ?>"><i class="ti-help"></i> QNA</a></li>
                        <!--<li><a class="sidebar-sub-toggle js-example-responsive"><i class="ti-target"></i> Satker <span class="sidebar-collapse-icon ti-angle-down"></span></a>-->
                        <!--    <ul class="js-example-responsive">-->
                        <!--    	<?php $dt = $this->fungsi->satker(); ?>-->
                        <!--    	<?php foreach ($dt->result() as $key => $pd) { ?>-->
                        <!--        <?php if ($pd->nama != "") {?>-->
                        <!--    		<li><a href="<?=site_url('satker/change/') ?><?= $pd->username; ?>"><?= $pd->nama; ?></a></li>-->
                        <!--            <?php } ?>-->
                        <!--    	<?php } ?>-->
                        <!--    </ul>-->
                        <!--</li>-->
                        <li class="label">Menu</li>
                        <li><a href="<?=site_url('logo')  ?>"><i class="ti-info-alt"></i> Logo</a></li>
                        <li><a href="<?=site_url('changepass')?>"><i class="ti-lock"></i> Change Password</a></li>
                        <li><a href="<?=site_url('auth/logout') ?>"><i class="ti-close"></i> Logout</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- /# sidebar -->


    <div class="header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="float-left">
                        <div class="hamburger sidebar-toggle">
                            <span class="line"></span>
                            <span class="line"></span>
                            <span class="line"></span>
                        </div>
                    </div>
                    <div class="float-right">
                        <div class="dropdown dib">
                            <div class="header-icon" data-toggle="dropdown">
                                <span class="user-avatar"><?= $this->fungsi->user_login()->username ?>
                                    <i class="ti-angle-down f-s-10"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <div class="content-wrap">
        <div class="main">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-8 p-r-0 title-margin-right">
                        <div class="page-header">
                            <div class="page-title">
                                <h1>Hello, <?= $this->fungsi->user_login()->nama ?></h1>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                    <div class="col-lg-4 p-l-0 title-margin-left">
                        <div class="page-header">
                            <div class="page-title">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="<?=site_url('dashboard') ?>">Dashboard</a></li>
                                    <li class="breadcrumb-item active"><?php echo $this->session->page; ?></li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                </div>
                <!-- /# row -->
                <section id="main-content">
                	<?php echo $contents; ?>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="footer">
                                <p>2018 © Admin Board. - <a href="#">example.com</a></p>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        </div>
    </div>







    
    <!-- jquery vendor -->
    <script src="<?php echo base_url()?>assets/js/lib/jquery.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/lib/jquery.nanoscroller.min.js"></script>
    <!-- nano scroller -->
    <script src="<?php echo base_url()?>assets/js/lib/menubar/sidebar.js"></script>
    <script src="<?php echo base_url()?>assets/js/lib/preloader/pace.min.js"></script>
    <!-- sidebar -->
    
    <!-- bootstrap -->
    <script src="<?php echo base_url()?>assets/js/lib/bootstrap.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/scripts.js"></script>
    <script src="<?php echo base_url()?>assets/js/bootstrap-datepicker.js"></script>
    <!-- scripit init-->
    <script src="https://cdn.jsdelivr.net/npm/chart.js@3.3.0/dist/chart.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.7/js/dataTables.responsive.min.js"></script>

 	<script type="text/javascript">
 		$(document).ready(function() {
	    $('#example').DataTable( {
	        initComplete: function () {
	            this.api().columns().every( function () {
	                var column = this;
	                var select = $('<select><option value=""></option></select>')
	                    .appendTo( $(column.footer()).empty() )
	                    .on( 'change', function () {
	                        var val = $.fn.dataTable.util.escapeRegex(
	                            $(this).val()
	                        );
	 
	                        column
	                            .search( val ? '^'+val+'$' : '', true, false )
	                            .draw();
	                    } );
	 
	                column.data().unique().sort().each( function ( d, j ) {
	                    select.append( '<option value="'+d+'">'+d+'</option>' )
	                } );
	            } );
	        }
	    } );
	} );
 	</script>
        <script type="text/javascript">
        $(document).ready(function() {
        $('#example2').DataTable( {
            initComplete: function () {
                this.api().columns().every( function () {
                    var column = this;
                    var select = $('<select><option value=""></option></select>')
                        .appendTo( $(column.footer()).empty() )
                        .on( 'change', function () {
                            var val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                            );
     
                            column
                                .search( val ? '^'+val+'$' : '', true, false )
                                .draw();
                        } );
     
                    column.data().unique().sort().each( function ( d, j ) {
                        select.append( '<option value="'+d+'">'+d+'</option>' )
                    } );
                } );
            }
        } );
    } );
    </script>
 		<script>
 			$(document).ready(function() {
	var ctx = document.getElementById('myChart');
    if (ctx!=null) {
	var myChart = new Chart(ctx, {
	    type: 'bar',
	    data: {
	        labels: bln,
	        datasets: [{
	            label: '# of Votes',
	            data: hsl,
	            backgroundColor: [
	                'rgba(255, 99, 132, 0.2)',
	                'rgba(54, 162, 235, 0.2)',
	                'rgba(255, 206, 86, 0.2)',
	                'rgba(75, 192, 192, 0.2)',
	                'rgba(153, 102, 255, 0.2)',
	                'rgba(255, 159, 64, 0.2)'
	            ],
	            borderColor: [
	                'rgba(255, 99, 132, 1)',
	                'rgba(54, 162, 235, 1)',
	                'rgba(255, 206, 86, 1)',
	                'rgba(75, 192, 192, 1)',
	                'rgba(153, 102, 255, 1)',
	                'rgba(255, 159, 64, 1)'
	            ],
	            borderWidth: 1
	        }]
	    },
	    options: {
	        scales: {
	            y: {
	                beginAtZero: true
	            }
	        }
	    }
	});
    }
	} );
	</script>
	<script type="text/javascript">
        function startLoader(){
             //$('.alert-box').show();
             $("#pageloader").fadeIn();
    }
    function stopLoader(){
             //$('.alert-box').hide();
             $("#pageloader").fadeOut();
    }
		$('#sandbox-container input').datepicker({
		    format: "yyyy-mm-dd",
		    autoclose: true
		});
        $('#datepicker').datepicker({
            viewMode: 'years',
            format: "yyyymm",
            autoclose: true
        });
        $('#datepicker2').datepicker({
            viewMode: 'years',
            format: "yyyymm",
            autoclose: true
        });
         $('#datepicker3').datepicker({
            viewMode: 'years',
            format: "yyyymm",
            autoclose: true
        });
          $('#datepicker4').datepicker({
            viewMode: 'years',
            format: "yyyymm",
            autoclose: true
        });
		$(".js-example-responsive").select2({
            width: 'resolve' // need to override the changed default
            });
	</script>
	
</body>

</html>