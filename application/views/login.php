<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Tidar - Login</title>

    <!-- ================= Favicon ================== -->
    <!-- Standard -->
    <link rel="shortcut icon" href="http://placehold.it/64.png/000/fff">
    <!-- Retina iPad Touch Icon-->
    <link rel="apple-touch-icon" sizes="144x144" href="http://placehold.it/144.png/000/fff">
    <!-- Retina iPhone Touch Icon-->
    <link rel="apple-touch-icon" sizes="114x114" href="http://placehold.it/114.png/000/fff">
    <!-- Standard iPad Touch Icon-->
    <link rel="apple-touch-icon" sizes="72x72" href="http://placehold.it/72.png/000/fff">
    <!-- Standard iPhone Touch Icon-->
    <link rel="apple-touch-icon" sizes="57x57" href="http://placehold.it/57.png/000/fff">

    <!-- Styles -->
    <link href="<?php echo base_url()?>assets/css/lib/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo base_url()?>assets/css/lib/themify-icons.css" rel="stylesheet">
    <link href="<?php echo base_url()?>assets/css/lib/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url()?>assets/css/lib/helper.css" rel="stylesheet">
    <link href="<?php echo base_url()?>assets/css/style.css" rel="stylesheet">
</head>

<body class="hold-transition login-page" style='width:100%;height:100%;background-image: url("<?php echo base_url().'assets/images/splash2.jpg'?>");background-position: center;background-repeat: no-repeat;background-size: cover;'>

    <div class="unix-login">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-lg-4">
                    <div class="login-content">
                        <div class="login-form">
                              <img src="<?= site_url('assets/images/logo-bpjs.png')?>" width="50%" class="mx-auto d-block">
                            <br><br>
                                     <form role="form" action="<?=site_url('auth/process')?>" method="post" >
            <fieldset>
              <div class="form-group" style="outline: 0;margin-top: 5px;margin-bottom: 5px;">
                <input class="form-control" placeholder="Username" name="username" style="height:50%;" type="text" autofocus="" autocomplete="off">
              </div>
              <div class="form-group" style="outline: 0;margin-top: 5px;margin-bottom: 5px;">
                <input class="form-control" placeholder="Password" name="password" style="height:50%;" type="password" value="" autocomplete="off">
              </div>
              <div style="outline: 0;margin-top: 5px;margin-bottom: 5px;" class="form-group">
                    <label>Level</label>
                    <select class="form-control" name="level">
                      <option value="2" <?=set_value('level') == 2 ? "Selected": null?> >Satker</option>
                      <option value="1" <?=set_value('level') == 1 ? "Selected": null?>>Petugas BPJS</option>
                    </select>
              </div>
              <div class="form-group" style="outline: 0;margin-top: 5px;margin-bottom: 5px;">
              <label>Please retype the characters</label>
              <?php echo $captchaHtml; ?>
              <br><br>

              <input type="text" class="form-control" name="CaptchaCode" id="CaptchaCode" value="" size="10" style="height:50%;" />
            </div>
              <button style="background-color: #3C8DBC;color: white;outline: 0;margin-top: 5px;margin-bottom: 5px;width:100%;" name="login">Login</button>
          </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</body>

</html>