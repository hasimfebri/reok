<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
	function __construct(){
		parent::__construct();
		check_not_login();
		$this->load->model('basic_m');
		$this->load->model('admin_m');
    $this->load->library('form_validation');
	}

	
	public function index()
	{
		$data['row'] = $this->admin_m->get();
		$this->template->load('templatehome','data/admin_data',$data);
	}
    public function getEdit(){
        $id = $this->input->post('id');
        $data = $this->admin_m->get($id)->row();
        // var_dump($data);
        echo json_encode($data);
    }
    function cekusername(){
        $data = $this->admin_m->get($this->input->post('username'))->num_rows();
        if ($data > 0){
            echo json_encode(array(
                'status'    => true,
                'message'   => 'username sudah dipakai'
            ));
        }else{
            echo json_encode(array(
                'status'    => false,
                'message'   => 'username ready'
            ));
        }
    }
    function getCabang(){
        $data = $this->admin_m->getCabang()->result();
        // var_dump($data);
        // exit();

        foreach ($data as $value) {
            echo "<option value='".$value->id."'>".$value->nama_cabang."</option>";
        }
    }
	public function add()
    {
         $this->form_validation->set_rules('username', 'Username', 'required|is_unique[bpjs.username]');
         $this->form_validation->set_rules('nama', 'Nama', 'required');
         $this->form_validation->set_rules('password', 'Password', 'required');
         $this->form_validation->set_rules('passconf', 'Konfirmasi password', 'required|matches[password]');
         $this->form_validation->set_message('required', '%s Harus diisi!');
         $this->form_validation->set_message('matches', '%s tidak sama');
         $this->form_validation->set_message('is_unique', '%s sudah digunakan');
         $this->form_validation->set_error_delimiters('<span style="color:red">','</span>');
          if ($this->form_validation->run() == FALSE)
            {
                $this->template->load('templatehome','data/admin_add');
            }
            else
            {
                $post = $this->input->post(null,TRUE);
                $this->admin_m->add($post);
                    if($this->db->affected_rows() >0){
                    echo "<script>alert('data berhasil disimpan');
                    window.location='".site_url('admin')."'
                    </script>";
                }
            }
            
    }
    public function del()
    {
		$post = $this->input->post(null,TRUE);
		$id = $post['username'];
		$params['hapus'] = 1;
		$this->db->where('username',$id);
		$this->db->update('bpjs',$params);
		if($this->db->affected_rows() >0){
                    echo "<script>alert('data berhasil disimpan');
                    window.location='".site_url('Admin')."'
                    </script>";
                }
	}
    function addAdmin(){
        $cekData = $this->admin_m->get($this->input->post('addusername'));
        $options = [
            'cost' => 10,
        ];
        if ($cekData->num_rows() < 1){
                $password = 
            $data = array(
                "username"      =>  $this->input->post('addusername'),
                "nama"          =>  $this->input->post('addnama'),
                "cabang"        =>  (int)$this->input->post('addcabang'),
                "password"      =>  password_hash($this->input->post('addpassword'), PASSWORD_DEFAULT, $options),
                "level"         =>  1,
                "hapus"         =>  0,
            );
            $add = $this->admin_m->addadmin($data);
            if($add){
                echo "<script>alert('Data Berhasil Ditambah');
                    window.location='".site_url('Admin')."'
                    </script>";
            }else{
                echo "<script>alert('Data Gagal Ditambah');
                    window.location='".site_url('Admin')."'
                    </script>";
            }
        }else{
          echo "<script>alert('Data Sudah Ada');
                    window.location='".site_url('Admin')."'
                    </script>";  
        }
        
    }
    function Edit(){
        $cekData = $this->admin_m->get($this->input->post('usernameasli'));
        $options = [
            'cost' => 10,
        ];
        if ($cekData->num_rows() > 0){
            if($this->input->post('usernameasli') != ""){
                $password = password_hash($this->input->post('password'), PASSWORD_DEFAULT, $options);
            }else{
                $password = $cekData->row()->password;
            }
            $data = array(
                "nama"          =>  $this->input->post('nama'),
                "cabang"        =>  (int)$this->input->post('cabang'),
                "password"      =>  $password,
                "level"         =>  $cekData->row()->level,
                "hapus"         =>  $cekData->row()->hapus,
            );
            $edit = $this->admin_m->editadmin($data,$this->input->post('usernameasli'));
            if($edit){
                echo "<script>alert('Data Berhasil Dirubah');
                    window.location='".site_url('Admin')."'
                    </script>";
            }else{
                echo "<script>alert('Data Gagal Dirubah');
                    window.location='".site_url('Admin')."'
                    </script>";
            }
        }else{
          echo "<script>alert('Data Tidak Ditemukan');
                    window.location='".site_url('Admin')."'
                    </script>";  
        }
        
    }
	// public function edit($id)
 //    {
 //         // $this->form_validation->set_rules('username', 'Username', 'is_unique[satker.username]');
 //         $this->form_validation->set_rules('nama', 'Nama', 'required');
 //         $this->form_validation->set_message('required', '%s Harus diisi!');
 //         $this->form_validation->set_message('matches', '%s tidak sama');
 //         $this->form_validation->set_message('is_unique', '%s sudah digunakan');
 //         $this->form_validation->set_error_delimiters('<span style="color:red">','</span>');
 //          if ($this->form_validation->run() == FALSE)
 //            {
 //                $query = $this->admin_m->get($id);

 //                if($query->num_rows() > 0){
 //                    $data['row'] = $query;
 //                    $this->template->load('templatehome','data/admin_edit', $data);
 //                }else{
 //                    echo "<script>alert('data tidak ditemukan')</script>";
 //                    redirect('satker');
 //                }
 //            }
 //            else
 //            {
 //                $post = $this->input->post(null,TRUE);
 //                $this->admin_m->editadmin($post);
 //               if ($this->db->trans_status() === FALSE){
 //                    echo "<script>alert('tidak ada data yang diubah');
 //                    window.location='".site_url('admin')."'
 //                    </script>";
 //                }else{
 //                    echo "<script>alert('data berhasil disimpan');
 //                    window.location='".site_url('admin')."'
 //                    </script>";
 //                }
 //            }
 //    }
}
