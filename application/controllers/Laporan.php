<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan extends CI_Controller {
	function __construct(){
		parent::__construct();
		check_not_login();
        $this->load->model('surat_m');
        $this->load->model('klaim_m');
        $this->load->model('fpk_m');
        $this->load->model('layanan_m');
        $this->load->model('fktp_m');
        $this->load->model('laporan_m');
        $this->load->model('basic_m');
        $this->load->library('form_validation');
	}

	public function index(){
		$data['fktp'] = $this->fktp_m->getfktpbyadmin();
		$data['cabang'] = $this->basic_m->get("cabang");
		$data['row'] = null;
		$this->template->load('template','laporan/cetaklaporan',$data);
	}	
	public function getfktp(){
        $id = $this->input->post('kode_cabang');
        $val = $this->basic_m->getById('fktp','kode_cabang_fktp',$id)->result_array();
        echo json_encode(array($val));
    }
    public function cetak(){
    	$post = $this->input->post(null,TRUE);
    	if (isset($post['cetak']))
    		$query = $this->laporan_m->userget($post);
			if($query->num_rows() > 0){
				$data['row'] = $query;
				$data['tgldr'] = $post['tgl_awal'];
				$data['tglsm'] =$post['tgl_akhir'];
			//$html = $this->load->view('laporan/excel',$data,true);
			//$this->template->load('template','laporan/excel',$data);
			$this->load->view('laporan/excel',$data);
	        //$this->fungsi->pdfGenerator($html,'Laporan','A4','landscape');
			}else{
				echo "<script>alert('data tidak ditemukan');
                    window.location='".site_url('laporan')."'
                    </script>";
			}
    	}else if(isset($post['cari'])){
    		$query = $this->laporan_m->userget($post);
			if($query->num_rows() > 0){
				$data['row'] = $query;
				$data['tgldr'] = $post['tgl_awal'];
				$data['tglsm'] =$post['tgl_akhir'];
		    	$data['fktp'] = $this->fktp_m->getfktpbyadmin();
				$data['cabang'] = $this->basic_m->get("cabang");
				$this->template->load('template','laporan/cetaklaporan',$data);
	    	}else{
					echo "<script>alert('data tidak ditemukan');
                    window.location='".site_url('laporan')."'
                    </script>";
				}

		}
	}
    public function cetakadmin(){
    	$post = $this->input->post(null,TRUE);
    	if (isset($post['cetak'])) {
    		$query = $this->laporan_m->adminget($post);
			if($query->num_rows() > 0){
				$data['row'] = $query;
				$data['tgldr'] = $post['tgl_awal'];
				$data['tglsm'] =$post['tgl_akhir'];
			//$html = $this->load->view('laporan/excel',$data,true);
			//$this->template->load('template','laporan/excel',$data);
			$this->load->view('laporan/excel',$data);
	        //$this->fungsi->pdfGenerator($html,'Laporan','A4','landscape');
			}else{
				echo "<script>alert('data tidak ditemukan');
                    window.location='".site_url('laporan')."'
                    </script>";
			}
    	}else if(isset($post['cari'])){
    		$query = $this->laporan_m->adminget($post);
			if($query->num_rows() > 0){
				$data['row'] = $query;
				$data['tgldr'] = $post['tgl_awal'];
				$data['tglsm'] =$post['tgl_akhir'];
		    	$data['fktp'] = $this->fktp_m->getfktpbyadmin();
				$data['cabang'] = $this->basic_m->get("cabang");
				$this->template->load('template','laporan/cetaklaporan',$data);
	    	}else{
					echo "<script>alert('data tidak ditemukan');
                    window.location='".site_url('laporan')."'
                    </script>";
				}

		}
	}	
	public function cetaksuper(){
    	$post = $this->input->post(null,TRUE);
    	if (isset($post['cetak'])) {
    		$query = $this->laporan_m->superget($post);
			if($query->num_rows() > 0){
				$data['row'] = $query;
				$data['tgldr'] = $post['tgl_awal'];
				$data['tglsm'] =$post['tgl_akhir'];
			//$html = $this->load->view('laporan/excel',$data,true);
			//$this->template->load('template','laporan/excel',$data);
			$this->load->view('laporan/excel',$data);
	        //$this->fungsi->pdfGenerator($html,'Laporan','A4','landscape');
			}else{
				echo "<script>alert('data tidak ditemukan');
                    window.location='".site_url('laporan')."'
                    </script>";
			}
    	}else if(isset($post['cari'])){
    		$query = $this->laporan_m->superget($post);
			if($query->num_rows() > 0){
				$data['row'] = $query;
				$data['tgldr'] = $post['tgl_awal'];
				$data['tglsm'] =$post['tgl_akhir'];
		    	$data['fktp'] = $this->fktp_m->getfktpbyadmin();
				$data['cabang'] = $this->basic_m->get("cabang");
				$this->template->load('template','laporan/cetaklaporan',$data);
	    	}else{
					echo "<script>alert('data tidak ditemukan');
                    window.location='".site_url('laporan')."'
                    </script>";
				}

		}
	}			
    public function cetak2(){
		$post = $this->input->post(null,TRUE);
		$id = $post['regis'];
		$data['row'] = $this->surat_m->getcetak($id,"user","surat_username = username")->row() ;
		$data['klaim'] = $this->klaim_m->get();
		$data['layanan'] = $this->layanan_m->get();
		$data['fpkdt'] = $this->fpk_m->getdetail($id);
		$data['fktp'] = $this->fktp_m->get();
		$data['imglogo'] = $_SERVER["DOCUMENT_ROOT"]."\Monas\assets\gambar\logo-bpjs.png";
		$html = $this->load->view('laporan/contoh2',$data,true);
        // $this->template->load('template','laporan/contoh2');
        $this->fungsi->pdfGenerator($html,'Laporan','A4','portrait');        
    }
}
