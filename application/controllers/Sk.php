<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sk extends CI_Controller {
	function __construct(){
		parent::__construct();
		check_not_login();
		$this->load->model('satker_m');
		$this->load->model('sk_m');
        $this->load->library('form_validation');
	}
	public function index()
	{
		$data['row'] = $this->sk_m->get();
		$this->template->load('template','data/sk',$data);
	}
	public function add(){
        $config['upload_path']          = './uploads/sk/';
        $config['allowed_types']        = 'pdf';
        $config['file_name']            = 'Sk1-'.date('ymd').'-'.substr(md5(rand()),0,10);
        $post = $this->input->post(null,TRUE);
        $this->load->library('upload', $config);
        if(@$_FILES['userfile1']['name'] != null){
            if ($this->upload->do_upload('userfile1')){
                $post['userfile1'] = $this->upload->data('file_name');
                $this->sk_m->add($post);
                if($this->db->affected_rows() >0){
                    echo "<script>alert('data berhasil disimpan');
                    window.location='".site_url('sk')."'
                    </script>";
                }
            }else{
                $error = $this->upload->display_errors();
                echo "<script>alert($error);
                window.location='".site_url('sk')."'
                </script>";
            }
        }else{
             $this->sk_m->del();
             $this->sk_m->add($post);
             if($this->db->affected_rows() >0){
                    echo "<script>alert('data berhasil disimpan');
                    window.location='".site_url('sk')."'
                    </script>";
                
         }
        }
    }
		public function edit()
	{
		$data['row'] = $this->satker_m->getId()->row();
		$this->template->load('template','data/edit_profile',$data);
	}
	public function editproce(){
		$post = $this->input->post(null,TRUE);
		$this->satker_m->edit($post);
		if($this->db->affected_rows() >0){
                    echo "<script>alert('data berhasil disimpan');
                    window.location='".site_url('profile')."'
                    </script>";
                }
	}
}
