<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pegawai extends CI_Controller {
	function __construct(){
		parent::__construct();
		check_not_login();
		$this->load->model('basic_m');
        $this->load->model('Pegawai_m');
        $this->load->model('Pengajuan_m');
        $this->load->library('form_validation');
		$this->load->library('encryption');
		date_default_timezone_set("Asia/Bangkok");

	}
	public function index()
	{
		// $data['tanggal'] =date('d');
		// $data['jumlah']  =$this->Pegawai_m->getexist($this->session->idperiod)->num_rows();
		// $data['Pegawai']  =$this->Pegawai_m->getPegawai($this->session->idperiod)->num_rows();
		$this->template->load('template','data/pegawai');
	}
    public function getData()
    {
		$month = $this->input->post('month');
		if($month == ""){
		    $data = $this->Pegawai_m->getData($this->session->idperiod,"")->result();
		}else{
		    $cekData = $this->Pengajuan_m->getPeriode($this->session->idperiod);
		    $bulan = "".$cekData->row()->tahun."-".$this->input->post('month')."-01";
		    $data = $this->Pegawai_m->getData($this->session->idperiod,$bulan)->result();
		}
		$tgl = date('d');
        echo "<table id='example' class='display responsive nowrap' style='width:100%'>
        <thead>
            <tr>
				<th>#</th>
                <th>nama_satker</th>
                <th>nama_pegawai</th>
                <th>nik</th>
                <th>nip</th>
                <th>jml_is</th>
                <th>jml_an</th>
                <th>penghasilan</th>
                <th>tunjangan</th>
                <th>thp</th>
                <th>dpi</th>
                <th>iuran_1</th>
                <th>iuran_4</th>
                <th>total_i</th>
                <th>bulan</th>";
                if ($this->session->level == "2"){
                    echo"<th>Penonaktifan</th>";
                }
                echo"<th>Penambahan</th>
            </tr>
        </thead>
        <tbody>";
        $no=1;
        foreach($data as $key){
            echo "<tr>";
			echo "<td>".$no++."</td>";
			echo "<td>".$key->nama_satker."</td>";
            echo "<td>".$key->nama_pegawai."</td>";
            echo "<td>".substr($this->encryption->decrypt($key->nik),0,12).'*****'."</td>";
            echo "<td>".$key->nip."</td>";
            echo "<td>".$key->jml_is."</td>";
            echo "<td>".$key->jml_an."</td>";
            echo "<td>".$key->penghasilan."</td>";
            echo "<td>".$key->tunjangan."</td>";
            echo "<td>".$key->thp."</td>";
            echo "<td>".$key->dpi."</td>";
            echo "<td>".$key->iuran_1."</td>";
            echo "<td>".$key->iuran_4."</td>";
            echo "<td>".$key->total_i."</td>";
            echo "<td>".$key->bulan."</td>";
            if ($this->session->level == "2"){
                echo "<td>
                        <button onclick='mutasi(`".$key->nik."`,`".$tgl."`,`".substr($this->encryption->decrypt($key->nik),0,12).'*****'."`)' class='btn btn-danger'>Penonaktifan </button>
                    </td>";
            }
            echo "<td>
                <button onclick='penambahan(`".$key->nik."`,`".$tgl."`,`".substr($this->encryption->decrypt($key->nik),0,12).'*****'."`)' class='btn btn-primary'>Keluarga</button>
            </td>
            </tr>";
        }
        echo "</tbody>
        <tfoot>
            <tr>
				<th>#</th>
				<th>nama_satker</th>
				<th>nama_pegawai</th>
				<th>nik</th>
				<th>nip</th>
				<th>jml_is</th>
				<th>jml_an</th>
				<th>penghasilan</th>
				<th>tunjangan</th>
				<th>thp</th>
				<th>dpi</th>
				<th>iuran_1</th>
				<th>iuran_4</th>
				<th>total_i</th>
				<th>bulan</th>";
				if ($this->session->level == "2"){
				    echo "<th>Penonaktifan</th>";
				}
                echo "<th>Penambahan</th>
            </tr>
        </tfoot>
        </table>";
        
    }
    function getMutasi(){
        check_user();
        $nik = $this->input->post('idp');
        $cekData = $this->Pegawai_m->cekData($nik)->num_rows();
        if($cekData > 0){
            	echo "<script>alert('Sudah Ada Pengajuan Mutasi Untuk NIK Tersebut');
				window.location='".site_url('Pengajuan')."'
				</script>";
        }
		$config['upload_path']          = './uploads/pengajuan/';
        $config['allowed_types']        = 'pdf|jpg|png|jpeg';
        $config['file_name']            = 'bukti'.date('ymd').'-'.substr(md5(rand()),0,10);
        $post = $this->input->post(null,TRUE);
		$this->load->library('upload', $config);

		if($this->upload->do_upload('bukti')){
		    $bukti  = $this->upload->data('file_name');
            $nik = $this->input->post('idp');
            $data = $this->Pegawai_m->saveMutasi($nik,$bukti);
            if($data){
                 $pesan = urlencode("Ada Data Pengajaun dari satker ".$this->session->satker." Silahkan cek https://kc-magelang.com/tidar \n\n");
                    $secret_token =  "5349669683:AAE8jV_jT2FkaTxovyUY_nY8RkXQHRSoyoM";
                    $telegram_id = "-742569295";
	                $url = "https://api.telegram.org/bot" . $secret_token . "/sendMessage?parse_mode=markdown&chat_id=-742569295";
	                $url = $url . "&text=".$pesan;
	                $ch = curl_init();
	                $optArray = array(
	                       CURLOPT_URL => $url,
	                       CURLOPT_RETURNTRANSFER => true
	                );
	               curl_setopt_array($ch, $optArray);
	               $send = curl_exec($ch);
	               curl_close($ch);
                	echo "<script>alert('data berhasil disimpan');
				window.location='".site_url('Pengajuan')."'
				</script>";
            }else{
                if($data == 1){
                    	echo "<script>alert('Pegawai tidak terdaftar');
				    window.location='".site_url('Pengajuan')."'
				    </script>";
                }else{
                    	echo "<script>alert('data gagal disimpan');
				    window.location='".site_url('Pengajuan')."'
				    </script>";
                }
            }
		}else{
		    	echo "<script>alert('bukti gagal terupload');
				window.location='".site_url('Pengajuan')."'
				</script>";
		}
    }
//     public function getIuran(){
//         // check_not_login();
//         // include APPPATH.'third_party/PHPExcel/PHPExcel.php';
//         // $opendoc     = base_url('assets/kertaskerja/kertaskerja.xlsx');
       
// 		$objPHPExcel = PHPExcel_IOFactory::load($opendoc);
// 		$objPHPExcel->getActiveSheet(0);
// // 		$objPHPExcel->getActiveSheet()->setCellValue('AE'.$index,"".$row->T10);

		
// 				header('Content-Type: application/vnd.ms-excel'); //mime type
// 		header('Content-Disposition: attachment;filename="Hourly Target - '.$dt.'.xls"'); //tell browser what's the file name
// 		header('Cache-Control: max-age=0'); //no cache
// 		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
// 		$objWriter->save('php://output');$objPHPExcel->getActiveSheet()->setCellValue('AE'.$index,"".$row->T10);

// //         $foo = "100";
// //         $data = number_format((float)$foo, 2, '.', '');
// // 		$html = $this->load->view('laporan/ba2',$data,true);
// //         $this->fungsi->pdfGenerator($html,'Laporan','A4','portrait');
//     }
    function cetakPembayaran(){
        $month = $this->input->post('month');
        if($month == ""){
             $cekData = $this->Pengajuan_m->getPeriode($this->session->idperiod);
             if ($cekData->num_rows() < 1){
	        		echo json_encode(array(
	        			'status'	=> false,
	        			'message'	=> 'periode no found',
	        		));
	        		exit();
	        	}
// 	        	$bulan = "".$cekData->row()->tahun."-".$this->input->post('bulan')."-01";
             $data = $this->Pegawai_m->getIuran($this->session->idperiod)->row();
        }else{
            $cekData = $this->Pengajuan_m->getPeriode($this->session->idperiod);
            $bulan = "".$cekData->row()->tahun."-".$this->input->post('month')."-01";
            $data = $this->Pegawai_m->getIuranMonth($this->session->idperiod,$bulan)->row();
        }
        echo json_encode($data);
    }
    function getIuranData(){
        
        $cekData = $this->Pengajuan_m->getPeriode($this->session->idperiod);
        if ($cekData->num_rows() < 1){
			echo json_encode(array(
				'status'	=> false,
				'message'	=> 'periode no found',
			));
			exit();
		}
		$bulan = "".$cekData->row()->tahun."-".$this->input->post('bulan')."-01";
        $data = $this->Pengawai_m->getIuranMonth($this->session->idperiod,$bulan);
    }
    function getAnggota(){
        		$tgl = date('d');
        $nik = $this->input->post('e');
        $anak = 0;
        $status = '';
        $data = $this->Pegawai_m->getAnggota($nik)->result();
        // $data = $this->Pegawai_m->cekData($nik);
        foreach ($data as $key){
            if($key->status > 2){
                $anak = $key->status - 3;
                $status = "Anak";
            }else{
                $anak = "-";
                if($key->status == 1){
                    $status = "Suami";
                }else{
                   $status = "Istri"; 
                }
            }
          echo "<tr>";
          echo "<td>".substr($this->encryption->decrypt($key->nik),0,12).'*****'."</td>";
          echo "<td>".$key->nama."</td>";
          echo "<td style='text-align: center;'>".$key->tgl_lahir."</td>";
          echo "<td style='text-align: center;'>".$status."</td>";
          echo "<td style='text-align: center;'>".$anak."</td>";
          echo "<td style='text-align: center;'>-</td>";
          if ($this->session->level == "2"){
            echo "<td style='text-align: center;'> <button onclick='delAnggota(`".$key->nik."`,`".$tgl."`,`".substr($this->encryption->decrypt($key->nik),0,12).'*****'."`)' type='button' class='btn btn-danger'>Hapus</button></td>";
          }else{
             echo "<td style='text-align: center;'>-</td>";  
          }
          echo "</tr>";
        }
    }
    function addAnggota(){
        $post = $this->input->post(null,TRUE);
        $jumlah = 0;
        $cekData = $this->Pegawai_m->getAnggota($post['nik_sumber'])->result();
        $no = 0;
        foreach ($cekData as $key){
            if($this->encryption->decrypt($key->nik) == $post['nik_tambah']){
               $jumlah = $no;
            }
            $no++;
        }
        if($jumlah != 0){
            echo "<script>alert('NIK Sudah Terdaftar');
				window.location='".site_url('Pengajuan')."'
				</script>";
        }
		$config['upload_path']          = './uploads/pengajuan/';
        $config['allowed_types']        = 'pdf|jpg|png|jpeg';
        $config['file_name']            = 'bukti'.date('ymd').'-'.substr(md5(rand()),0,10);
		$this->load->library('upload', $config);

		if($this->upload->do_upload('bukti')){
		    $post['bukti']  = $this->upload->data('file_name');
		    $post['nik_encript']  = $this->encryption->encrypt($post['nik_tambah']);
		    if($post['status'] == 3){
		        $post['status'] = $post['status'] +  $post['anak'];
		    }
            $data = $this->Pegawai_m->saveAnggota($post);
            if($data){
                $pesan = urlencode("Ada Data Pengajaun dari satker ".$this->session->satker." Silahkan cek https://kc-magelang.com/tidar \n\n");
                $secret_token =  "5349669683:AAE8jV_jT2FkaTxovyUY_nY8RkXQHRSoyoM";
                $telegram_id = "-742569295";
	            $url = "https://api.telegram.org/bot" . $secret_token . "/sendMessage?parse_mode=markdown&chat_id=-742569295";
	            $url = $url . "&text=".$pesan;
	            $ch = curl_init();
	            $optArray = array(
	                   CURLOPT_URL => $url,
	                   CURLOPT_RETURNTRANSFER => true
	            );
	            curl_setopt_array($ch, $optArray);
	            $send = curl_exec($ch);
	            curl_close($ch);
                	echo "<script>alert('data berhasil disimpan');
				window.location='".site_url('Pengajuan')."'
				</script>";
            }else{
                    	echo "<script>alert('data gagal disimpan');
				    window.location='".site_url('Pengajuan')."'
				    </script>";
            }
		}else{
		    	echo "<script>alert('bukti gagal terupload');
				window.location='".site_url('Pengajuan')."'
				</script>";
		}
    }
    function getKK(){
        $nik = $this->input->post('e');
        $data = $this->Pegawai_m->getKK($nik)->row();
        echo json_encode($data);
    }
    function getPenon(){
        $post = $this->input->post(null,TRUE);
        $array = array();
        $cekData = $this->Pegawai_m->getAnggotaPenon($post['nik_penon']);
        if ($cekData->num_rows() < 1 ){
            echo "<script>alert('Anggota Keluarga Tidak Terdaftar');
				window.location='".site_url('Pengajuan')."'
				</script>";
        }
        $cekPengajuan = $this->Pegawai_m->getAnggotaPenon($post['nik_penon']);
        if ($cekData->num_rows() > 0 ){
            echo "<script>alert('Sudah ada Pengjuan Anggota Keluarga Untuk NIK ini');
				window.location='".site_url('Pengajuan')."'
				</script>";
        }
		$config['upload_path']          = './uploads/pengajuan/';
        $config['allowed_types']        = 'pdf|jpg|png|jpeg';
        $config['file_name']            = 'bukti'.date('ymd').'-'.substr(md5(rand()),0,10);
		$this->load->library('upload', $config);
		if($this->upload->do_upload('bukti')){
		    $post['bukti']  = $this->upload->data('file_name');
		    $data = $this->Pegawai_m->saveAnggotaPenon($post);
		    
		    if($data){
		        $pesan = urlencode("Ada Data Pengajaun dari satker ".$this->session->satker." Silahkan cek https://kc-magelang.com/tidar \n\n");
                $secret_token =  "5349669683:AAE8jV_jT2FkaTxovyUY_nY8RkXQHRSoyoM";
                $telegram_id = "-742569295";
	            $url = "https://api.telegram.org/bot" . $secret_token . "/sendMessage?parse_mode=markdown&chat_id=-742569295";
	            $url = $url . "&text=".$pesan;
	            $ch = curl_init();
	            $optArray = array(
	                   CURLOPT_URL => $url,
	                   CURLOPT_RETURNTRANSFER => true
	            );
	            curl_setopt_array($ch, $optArray);
	            $send = curl_exec($ch);
	            curl_close($ch);
                	echo "<script>alert('data berhasil disimpan');
				window.location='".site_url('Pengajuan')."'
				</script>";
            }else{
                    	echo "<script>alert('data gagal disimpan');
				    window.location='".site_url('Pengajuan')."'
				    </script>";
            }
		}else{
		    	echo "<script>alert('bukti gagal terupload');
				window.location='".site_url('Pengajuan')."'
				</script>";
		}
    }
}
