<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Satker extends CI_Controller {
	function __construct(){
		parent::__construct();
		check_not_login();
		$this->load->model('basic_m');
		$this->load->model('satker_m');
        $this->load->library('form_validation');
	}

	public function change($id)
	{
		$query = $this->satker_m->getById($id)->row();
		$q = $this->basic_m->getById("satker","username",$id)->row();
		$params = array(
						'tahun' => $query->tahun,
						'idperiod' => $query->id_periode,
						'satker' => $q->nama,
						'usersatker' => $id
					);
		$this->session->set_userdata($params);
		redirect('dashboard');
	}
	public function clear(){
		$params = array('tahun','idperiod','satker','usersatker');
		$this->session->unset_userdata($params);
		redirect('dashboard');
	}
	public function index()
	{
    $satker = array(
      'cabang' => "",
      'id'     => "",
    );
		$data['row'] = $this->satker_m->get($satker);
		$this->template->load('templatehome','data/satker_data',$data);
	}
	public function add()
    {
         $this->form_validation->set_rules('username', 'Username', 'required|is_unique[satker.username]');
         $this->form_validation->set_rules('password', 'Password', 'required');
         $this->form_validation->set_rules('passconf', 'Konfirmasi password', 'required|matches[password]');
         $this->form_validation->set_message('required', '%s Harus diisi!');
         $this->form_validation->set_message('matches', '%s tidak sama');
         $this->form_validation->set_message('is_unique', '%s sudah digunakan');
         $this->form_validation->set_error_delimiters('<span style="color:red">','</span>');
          if ($this->form_validation->run() == FALSE)
            {
                $this->template->load('templatehome','data/satker_add');
            }
            else
            {
                $config['upload_path']          = './uploads/logo/';
                $config['allowed_types']        = 'jpg|png|jpeg';
                $config['file_name']            = 'Logo-'.date('ymd').'-'.substr(md5(rand()),0,10);
                $post = $this->input->post(null,TRUE);
                $this->load->library('upload', $config);
                if(@$_FILES['logo']['name'] != null){
                if ($this->upload->do_upload('logo')){
                $post['logo'] = $this->upload->data('file_name');
                $this->satker_m->add($post);
                if($this->db->affected_rows() >0){
                    $result =  new Endroid\QrCode\Builder\Builder();
                    $encode = base64_encode($post['username']); 
                    $link = base_url('ba/buktiqrcode/').$encode;
                    $nameqr = $post['username']."-qr-".substr(md5(rand()),0,10);
                    $a = $result->create();
                    $b = $a->writer(new Endroid\QrCode\Writer\PngWriter())->writerOptions([])->data($link)->encoding(new Endroid\QrCode\Encoding\Encoding('UTF-8'))->errorCorrectionLevel(new Endroid\QrCode\ErrorCorrectionLevel\ErrorCorrectionLevelHigh())->size(300)->margin(10)->roundBlockSizeMode(new Endroid\QrCode\RoundBlockSizeMode\RoundBlockSizeModeMargin())->labelText('TIDAR')->labelFont(new Endroid\QrCode\Label\Font\NotoSans(20))->labelAlignment(new Endroid\QrCode\Label\Alignment\LabelAlignmentCenter())->build();
                    $b->saveToFile('uploads/qrcode/'.$nameqr.'.png');
                    $qrc = $nameqr.'.png';
                    $this->satker_m->updateqr($post,$qrc);
                    if($this->db->affected_rows() >0){
                    echo "<script>alert('data berhasil disimpan');
                    window.location='".site_url('satker')."'
                    </script>";
                }
                }
            }else{
                $error = $this->upload->display_errors();
                echo "<script>alert($error);
                window.location='".site_url('satker')."'
                </script>";
            }
            //
          }else{
            $post['logo'] = null;
            $this->satker_m->add($post);
              if($this->db->affected_rows() >0){
                 echo "<script>alert('data berhasil disimpan');
                    window.location='".site_url('satker')."'
                    </script>";
              }else{
                 echo "<script>alert('data gagal disimpan');
                    window.location='".site_url('satker')."'
                    </script>";
              }
            }
          }
    }
    public function enkripsi(){
         $plaintext = "user4";  
      //Encode plaintext  
      $encode = base64_encode($plaintext);  
      //Decode plaintext  
      $decode = base64_decode($encode);  
      echo "teks = ".$plaintext."<br/>";   
      echo "teks yang diencode = ".$encode."<br/>";  
      echo "teks yang didecode = ".$decode;  
    }
    public function del()
    {
		$post = $this->input->post(null,TRUE);
		$id = $post['username'];
		$params['hapus'] = 1;
		$this->db->where('username',$id);
		$this->db->update('satker',$params);
		if($this->db->affected_rows() >0){
                    echo "<script>alert('data berhasil disimpan');
                    window.location='".site_url('satker')."'
                    </script>";
                }
	}
	public function edit($id)
    {
       $satker = array(
          'cabang' => "",
          'id'     => $id,
        );
         // $this->form_validation->set_rules('username', 'Username', 'is_unique[satker.username]');
         $this->form_validation->set_rules('nama', 'Nama', 'required');
         $this->form_validation->set_rules('alamat', 'Alamat', 'required');
         $this->form_validation->set_message('required', '%s Harus diisi!');
         $this->form_validation->set_message('matches', '%s tidak sama');
         $this->form_validation->set_message('is_unique', '%s sudah digunakan');
         $this->form_validation->set_error_delimiters('<span style="color:red">','</span>');
          if ($this->form_validation->run() == FALSE)
            {
                $query = $this->satker_m->get($satker);

                if($query->num_rows() > 0){
                    $data['row'] = $query;
                    $this->template->load('templatehome','data/satker_edit', $data);
                }else{
                    echo "<script>alert('data tidak ditemukan')</script>";
                    redirect('satker');
                }
            }
            else
            {
                $post = $this->input->post(null,TRUE);
                $this->satker_m->editsatker($post);
                if ($this->db->trans_status() === FALSE){
                    echo "<script>alert('tidak ada data yang diubah');
                    window.location='".site_url('satker')."'
                    </script>";
                }else{
                    echo "<script>alert('data berhasil disimpan');
                    window.location='".site_url('satker')."'
                    </script>";
                }
            }
    }
}
