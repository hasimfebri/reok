<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Import extends CI_Controller {
	function __construct(){
		parent::__construct();
		check_not_login();
		$this->load->model('basic_m');
        $this->load->library('form_validation');
	}
	public function index()
	{
		$this->template->load('template','data/import_add');
	}
    public function data()
    {
        $data['satker'] =$this->basic_m->getImportSatker($this->session->idperiod);
        $data['bpjs'] =$this->basic_m->getImport($this->session->idperiod,"import_bpjs");
        $this->template->load('template','data/downloadimport',$data);
    }
    public function exsatker()
    {
        $post = $this->input->post(null,TRUE);
        $month = $post['xsatker'];
        $tdatae= strtotime($month);
        $data['of'] = "Satker"; 
        $data['bulan'] = date('F', $tdatae);
        $data['satker'] = $this->session->satker;
        $data['row'] =$this->basic_m->getExport($this->session->idperiod,"import_satker",$month);
        $this->load->view('laporan/hasilimport',$data);
    }
    public function exbpjs()
    {
        $post = $this->input->post(null,TRUE);
        $month = $post['xbpjs'];
        $tdatae= strtotime($month);
        $data['of'] = "BPJS";  
        $data['bulan'] = date('F', $tdatae);
        $data['satker'] = $this->session->satker;
        $data['row'] =$this->basic_m->getExport($this->session->idperiod,"import_bpjs",$month);
        $this->load->view('laporan/hasilimport',$data);
    }
	public function upload()
    {
        $post = $this->input->post(null,TRUE);
        // Load plugin PHPExcel nya
        include APPPATH.'third_party/PHPExcel/PHPExcel.php';

        $config['upload_path'] = realpath('excel');
        $config['allowed_types'] = 'xlsx|xlx|csv';
        $config['max_size'] = '10000';
        $config['encrypt_name'] = true;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload()) {

            //upload gagal
            $this->session->set_flashdata('notif', '<div class="alert alert-danger"><b>PROSES IMPORT GAGAL!</b> '.$this->upload->display_errors().'</div>');
            //redirect halaman
            redirect('import/');

        } else {

            $data_upload = $this->upload->data();

            $excelreader     = new PHPExcel_Reader_Excel2007();
            $loadexcel         = $excelreader->load('excel/'.$data_upload['file_name']); // Load file yang telah diupload ke folder excel
            $sheet             = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);

            $data = array();

            $numrow =1;
            $tgl = $post['bulan'].'/01/'.$post['tahun'];
                $time = strtotime($tgl);
                $newformat = date('Y-m-d',$time);
            foreach($sheet as $row){
                            if($numrow >= 6){
                            	if ($row['D']!=null) {
                            		 array_push($data, array(
                                    'nama_satker' => $row['B'],
                                    'nama_pegawai'      => $row['C'],
                                    'nik'      => $row['D'],
                                    'nip' => $row['E'],
                                    'jml_is'      => $row['F'],
                                    'jml_an' => $row['G'],
                                    'penghasilan'      => preg_replace("/[^0-9]/", "", $row['H']),
                                    'tunjangan'      => preg_replace("/[^0-9]/", "", $row['I']),
                                    'thp' => preg_replace("/[^0-9]/", "", $row['J']),
                                    'dpi'      => preg_replace("/[^0-9]/", "", $row['K']),
                                    'iuran_1'      => preg_replace("/[^0-9]/", "", $row['L']),
                                    'iuran_4'      => preg_replace("/[^0-9]/", "", $row['M']),
                                    'total_i' => preg_replace("/[^0-9]/", "", $row['N']),
                                    'bulan'      => $newformat,
                                    'periode_id_periode '      => $this->session->idperiod,
                                ));
                            	}
                               
                    }
                $numrow++;
                
            }
            if ($this->session->level == "2") {
                $cekstatus = $this->basic_m->cekstatus($this->session->idperiod,$newformat);
                if ($cekstatus->num_rows() > 0 && $cekstatus->row()->status == 1 ){
                    $this->session->set_flashdata('notif', '<div class="alert alert-danger"><b>Anda Sudah Tidak Dapat Upload Data di Bulan Tersebut </b> <br>Data Gagal diimport!</div>');
                }else{
                    $this->basic_m->deleteimport($this->session->idperiod,$newformat,"import_satker");
                    $this->basic_m->deletevalidasi($this->session->idperiod,$newformat,"validasi");
                    $this->db->insert_batch('import_satker', $data);
                    $this->session->set_flashdata('notif', '<div class="alert alert-success"><b>PROSES IMPORT BERHASIL!</b> Data berhasil diimport!</div>');
                }
            }else{
                $this->basic_m->deleteimport($this->session->idperiod,$newformat,"import_bpjs");
                $this->db->insert_batch('import_bpjs', $data);
                $this->basic_m->uplastdate($this->session->idperiod);
                $this->session->set_flashdata('notif', '<div class="alert alert-success"><b>PROSES IMPORT BERHASIL!</b> Data berhasil diimport!</div>');
            }
            //delete file from server
            unlink(realpath('excel/'.$data_upload['file_name']));

            //upload success
            
            //redirect halaman
            redirect('import/');

        }
    }

    function setuju(){
        $post = $this->input->post(null,TRUE);
        // var_dump($post);
        $data = array(
            'periode'   => $post['periode'],
            'bulan'     => $post['bulan'],
        );

        $cek = $this->basic_m->cekremark($data);
        if($cek > 0 ){
            $datasave = array(
                'periode'   => $post['periode'],
                'bulan'     => $post['bulan'],
                'remark'    => $post['remark'],
                'status'    => 1,
            );
            $save = $this->basic_m->updateRemark($datasave);
        }else{
            $datasave = array(
                'periode'   => $post['periode'],
                'bulan'     => $post['bulan'],
                'remark'    => $post['remark'],
                'status'    => 1,
            );
            $save = $this->basic_m->saveRemark($datasave);
        }
        if($this->db->affected_rows() >0){
            echo "<script>alert('data berhasil disimpan');
            window.location='".site_url('Import/data')."'
            </script>";
        }else{
            echo "<script>alert('data berhasil disimpan');
            window.location='".site_url('Import/data')."'
            </script>";
        }
    }
    function tolak(){
        $post = $this->input->post(null,TRUE);
        // var_dump($post);
        $data = array(
            'periode'   => $post['periode2'],
            'bulan'     => $post['bulan2'],
        );

        $cek = $this->basic_m->cekremark($data);
        if($cek > 0 ){
            $datasave = array(
                'periode'   => $post['periode2'],
                'bulan'     => $post['bulan2'],
                'remark'    => $post['remark2'],
                'status'    => 2,
            );
            $save = $this->basic_m->updateRemark($datasave);
        }else{
            $datasave = array(
                'periode'   => $post['periode2'],
                'bulan'     => $post['bulan2'],
                'remark'    => $post['remark2'],
                'status'    => 2,
            );
            $save = $this->basic_m->saveRemark($datasave);
        }
        if($this->db->affected_rows() >0){
            echo "<script>alert('data berhasil disimpan');
            window.location='".site_url('Import/data')."'
            </script>";
        }else{
            echo "<script>alert('data berhasil disimpan');
            window.location='".site_url('Import/data')."'
            </script>";
        }
    }
}
