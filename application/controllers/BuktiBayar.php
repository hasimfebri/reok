<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class BuktiBayar extends CI_Controller {
	function __construct(){
		parent::__construct();
		check_not_login();
		$this->load->model('basic_m');
        $this->load->model('satup_m');
        $this->load->model('empatp_m');
        $this->load->library('form_validation');
	}
	public function index()
	{
        $id = $this->session->idperiod;
        if ($this->session->level == "1") {
            $data['row'] = $this->basic_m->getById("fikssatu","periode_id_periode",$id);
            $data['rowe'] = $this->basic_m->getById("fiksempat","periode_id_periode",$id);
            // $data['row'] = $this->basic_m->getbukti1("fikssatu","periode_id_periode",$id);
            // $data['rowe'] = $this->basic_m->getbukti4("fiksempat","periode_id_periode",$id);
        }else{
            $data['row'] = $this->basic_m->getById("satup","periode_id_periode",$id);
            $data['rowe'] = $this->basic_m->getById("empatp","periode_id_periode",$id);
            // $data['row'] = $this->basic_m->getbukti1($id);
            // $data['rowe'] = $this->basic_m->getbukti4($id);
        }
        
		$this->template->load('template','data/bukti_add',$data);
	}
    public function uploadsat(){
        $config['upload_path']          = './uploads/bb/';
        $config['allowed_types']        = 'pdf|jpg|png|jpeg';
        $config['file_name']            = 'Bb1-'.date('ymd').'-'.substr(md5(rand()),0,10);
        $post = $this->input->post(null,TRUE);
        $datacek = array(
            'bulan'                 => $post['tahun1'].'-'.$post['bulan1'].'-01',
            'periode_id_periode'    => $this->session->idperiod, 

        );
        $cekdata = $this->satup_m->cekdata($datacek);
        if($cekdata->num_rows() > 0 && $cekdata->row()->status == 1){
           $this->session->set_flashdata('notif', '<div class="alert alert-danger"><b>Anda Sudah Tidak Dapat Upload Data di Bulan Tersebut </b> <br>Data Gagal diimport!</div>');
           redirect('BuktiBayar/');
       }else{
        if($cekdata->num_rows() > 0){
            $this->satup_m->deletedata($cekdata->row()->id_satu);
        }
        $this->load->library('upload', $config);
            if(@$_FILES['userfile1']['name'] != null){
                if ($this->upload->do_upload('userfile1')){
                    $post['userfile1'] = $this->upload->data('file_name');
                    $this->satup_m->add($post);
                    if($this->db->affected_rows() >0){
                        echo "<script>alert('data berhasil disimpan');
                        window.location='".site_url('BuktiBayar')."'
                        </script>";
                    }
                }else{
                    $error = $this->upload->display_errors();
                    echo "<script>alert($error);
                    window.location='".site_url('BuktiBayar')."'
                    </script>";
                }
            }else{
                 $this->satup_m->add($post);
                 if($this->db->affected_rows() >0){
                        echo "<script>alert('data berhasil disimpan');
                        window.location='".site_url('BuktiBayar')."'
                        </script>";
                    
                }
            }
        }
    }
        public function uploademp(){
        $config['upload_path']          = './uploads/bb/';
        $config['allowed_types']        = 'pdf|jpg|png|jpeg';
        $config['file_name']            = 'Bb4-'.date('ymd').'-'.substr(md5(rand()),0,10);
        $post = $this->input->post(null,TRUE);
        $datacek = array(
            'periode_id_periode'    => $this->session->idperiod, 
            'bulan'                 => $post['tahun2'].'-'.$post['bulan2'].'-01',
        );
        $cekdata = $this->empatp_m->cekdata($datacek);
        if($cekdata->num_rows() > 0 && $cekdata->row()->status == 1){
           $this->session->set_flashdata('notif', '<div class="alert alert-danger"><b>Anda Sudah Tidak Dapat Upload Data di Bulan Tersebut </b> <br>Data Gagal diimport!</div>');
           redirect('BuktiBayar/');
       }else{
        if($cekdata->num_rows() > 0){
            $this->empatp_m->deletedata($cekdata->row()->id_empat);
        }
            $this->load->library('upload', $config);
            if(@$_FILES['userfile2']['name'] != null){
                if ($this->upload->do_upload('userfile2')){
                    $post['userfile2'] = $this->upload->data('file_name');
                    $this->empatp_m->add($post);
                    if($this->db->affected_rows() >0){
                        echo "<script>alert('data berhasil disimpan');
                        window.location='".site_url('BuktiBayar')."'
                        </script>";
                    }
                }else{
                    $error = $this->upload->display_errors();
                    echo "<script>alert($error);
                    window.location='".site_url('BuktiBayar')."'
                    </script>";
                }
            }else{
                 $this->empatp_m->add($post);
                 if($this->db->affected_rows() >0){
                        echo "<script>alert('data berhasil disimpan');
                        window.location='".site_url('BuktiBayar')."'
                        </script>";   
                }
            }
        }
    }
    public function download(){
        $id = $this->session->idperiod;
        $data['row'] = $this->basic_m->getById("satup","periode_id_periode",$id);
        $data['rowe'] = $this->basic_m->getById("empatp","periode_id_periode",$id);
        $this->template->load('template','data/download_bukti',$data);
    }
    public function delsatu(){
        $post = $this->input->post(null,TRUE);
        $id = $post['id1'];
        $this->basic_m->delete("satup","id_satu",$id);
        if($this->db->affected_rows() >0){
            $this->basic_m->delete("fikssatu","id_satu_id",$id);
            if($this->db->affected_rows() >0){
                    echo "<script>alert('data berhasil dihapus');
                    window.location='".site_url('BuktiBayar')."'
                    </script>";
                }
         }
    }
    public function delempat(){
        $post = $this->input->post(null,TRUE);
        $id = $post['id4'];
        $this->basic_m->delete("empatp","id_empat",$id);
        if($this->db->affected_rows() >0){
            $this->basic_m->delete("fiksempat","id_empat_id",$id);
            if($this->db->affected_rows() >0){
                    echo "<script>alert('data berhasil dihapus');
                    window.location='".site_url('BuktiBayar')."'
                    </script>";
                }
         }
    }
    public function validsatu($id){
        $row = $this->basic_m->getById("satup","id_satu",$id)->row();
        $this->satup_m->fsatu($row);
        if($this->db->affected_rows() >0){
            $this->satup_m->upsatu($id);
             if($this->db->affected_rows() >0){
                    echo "<script>alert('data berhasil disimpan');
                    window.location='".site_url('BuktiBayar/download')."'
                    </script>";
                }
                
         }
    }
    public function validempat($id){
        $row = $this->basic_m->getById("empatp","id_empat",$id)->row();
        $this->empatp_m->fempat($row);
        if($this->db->affected_rows() >0){
            $this->empatp_m->upempat($id);
             if($this->db->affected_rows() >0){
                    echo "<script>alert('data berhasil disimpan');
                    window.location='".site_url('BuktiBayar/download')."'
                    </script>";
                }
                
         }
    }
    public function rejecsatu(){
        $post = $this->input->post(null,TRUE);
        $id = $post['idsatu'];
        // $row = $this->basic_m->getById("satup","id_satu",$id)->row();
        $tolak = $this->satup_m->tolak($id,$post);
        if($this->db->affected_rows() >0){
                    echo "<script>alert('data berhasil disimpan');
                    window.location='".site_url('BuktiBayar/download')."'
                    </script>";
        }
    }
    public function rejectempat(){
        $post = $this->input->post(null,TRUE);
        $id = $post['idempat'];
        // $row = $this->basic_m->getById("empatp","id_empat",$id)->row();
        $tolak = $this->empatp_m->tolak($id,$post);
        if($this->db->affected_rows() >0){
                    echo "<script>alert('data berhasil disimpan');
                    window.location='".site_url('BuktiBayar/download')."'
                    </script>";
        }
        // $this->empatp_m->tlkfempat($row,$post);
        // if($this->db->affected_rows() >0){
        //     $this->empatp_m->upempat($id,$post);
        //      if($this->db->affected_rows() >0){
        //             echo "<script>alert('data berhasil disimpan');
        //             window.location='".site_url('BuktiBayar/download')."'
        //             </script>";
        //         }
                
        //  }
    }
}
