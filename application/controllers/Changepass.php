<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Changepass extends CI_Controller {
    function __construct(){
        parent::__construct();
        check_not_login();
        $this->load->model('users_m');
        $this->load->model('basic_m');
    $this->load->library('form_validation');
    }

    public function index()
    {
    $this->form_validation->set_rules('passwordlama', 'Passwordlama', 'min_length[3]|callback_password_check');
    $this->form_validation->set_rules('passwordbaru', 'Passwordbaru', 'callback_valid_password');
    $this->form_validation->set_rules('passconf', 'Konfirmasi password', 'matches[passwordbaru]|required');
    $this->form_validation->set_message('matches', '%s tidak sama');
    $this->form_validation->set_message('required', '%s Harus diisi!');
    $this->form_validation->set_error_delimiters('<span style="color:red">','</span>');
    if ($this->form_validation->run() == FALSE)
        {
        $level = $this->session->level;
                if ($level=="2") {
                 $this->template->load('template','data/user_gantiuser_pass');   
                }else{
                $this->template->load('templatehome','data/user_ganti_pass');
            }
        }
        else{
            $post = $this->input->post(null,TRUE);
            $level = $this->session->level;
            if ($level=="2"){
                $this->users_m->gantipassuser($post);
                if($this->db->affected_rows() >0){
                    echo "<script>alert('data berhasil disimpan');
                    window.location='".site_url('auth/logout')."'
                    </script>";
            }
        }else{
                $this->users_m->gantipass($post);
                if($this->db->affected_rows() >0){
                    echo "<script>alert('data berhasil disimpan');
                    window.location='".site_url('auth/logout')."'
                    </script>";
                }
            }
        }
    }

  function password_check(){
        $post = $this->input->post(null,TRUE);
        if ($this->session->level == "2") {
            $q = $this->basic_m->getById("satker","username",$this->session->username)->row();
        }else{
            $q = $this->basic_m->getById("bpjs","username",$this->session->username)->row();
        }
        
        $pass = $q->password;
        $userpass = $post['passwordlama'];
        if (password_verify($userpass, $pass)){
            return TRUE;
        }else{
        $this->form_validation->set_message('password_check', '{field} salah silahkan check kembali');
        return FALSE;
        }
    }
    public function valid_password()
    {
        $post = $this->input->post(null,TRUE);
        $password = $post['passwordbaru'];
        $regex_lowercase = '/[a-z]/';
        $regex_uppercase = '/[A-Z]/';
        $regex_number = '/[0-9]/';
        $regex_special = '/[!@#$%^&*()\-_=+{};:,<.>§~]/';
        if (empty($password))
        {
            $this->form_validation->set_message('valid_password', 'The {field} field is required.');
            return FALSE;
        }
        if (preg_match_all($regex_lowercase, $password) < 1)
        {
            $this->form_validation->set_message('valid_password', 'The {field} field must be at least one lowercase letter.');
            return FALSE;
        }
        if (preg_match_all($regex_uppercase, $password) < 1)
        {
            $this->form_validation->set_message('valid_password', 'The {field} field must be at least one uppercase letter.');
            return FALSE;
        }
        if (preg_match_all($regex_number, $password) < 1)
        {
            $this->form_validation->set_message('valid_password', 'The {field} field must have at least one number.');
            return FALSE;
        }
        if (preg_match_all($regex_special, $password) < 1)
        {
            $this->form_validation->set_message('valid_password', 'The {field} field must have at least one special character.' . ' ' . htmlentities('!@#$%^&*()\-_=+{};:,<.>§~'));
            return FALSE;
        }
        if (strlen($password) < 8)
        {
            $this->form_validation->set_message('valid_password', 'The {field} field must be at least 8 characters in length.');
            return FALSE;
        }
        return TRUE;
    }    
}
