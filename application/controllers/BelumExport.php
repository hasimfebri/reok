<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class BelumExport extends CI_Controller {
	function __construct(){
		parent::__construct();
		check_not_login();
        date_default_timezone_set("Asia/Jakarta");
		$this->load->model('basic_m');
		$this->load->model('admin_m');
        $this->load->library('form_validation');
	}

	
	public function index()
	{
	   $post = $this->input->post(null,TRUE);
       if(isset($post['cari'])){
            $fromdate = $this->input->post('fromdate');
            $todate = $this->input->post('todate');
            $query = $this->basic_m->BelumExport($fromdate,$todate);
        }else{
            $query = $this->basic_m->BelumExport();
        }
        if($query->num_rows() > 0){
                $data['row'] = $query;
                $this->template->load('templatehome','data/BelumExport',$data);
            }else{
                    echo "<script>alert('data tidak ditemukan');
                    window.location='".site_url('BelumExport')."'
                    </script>";
            }
	}
    function cekSatkerExport(){
        $id     = $this->input->post('id');
        if($this->input->post('date') == null || $this->input->post('date') == ''){
            $date   =  $this->input->post('date');
        }else{
            $date = date('Ym');
        }
        $row = array();
        $data = $this->basic_m->cekSatkerExport($id,$date);
        if($data->num_rows() > 0){
            echo json_encode($data->result());
            exit();
        }else{
            echo json_encode(array(
                'status'    => false,
                'message'   => 'Data Tidak Ditemukan'
            ));
            exit();
        }
    }
}
