<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {
	function __construct(){
		parent::__construct();
		check_not_login();
		$this->load->model('satker_m');
	}
	public function index()
	{
		$data['row'] = $this->satker_m->getId()->row();
		$this->template->load('template','data/profile',$data);
	}
		public function edit()
	{
		$data['row'] = $this->satker_m->getId()->row();
		$this->template->load('template','data/edit_profile',$data);
	}
	public function editproce(){
	 	$config['upload_path']          = './uploads/logo/';
        $config['allowed_types']        = 'jpg|png|jpeg';
        $config['file_name']            = 'Logo-'.date('ymd').'-'.substr(md5(rand()),0,10);
        $post = $this->input->post(null,TRUE);
        $this->load->library('upload', $config);
        if ($this->upload->do_upload('logo')){
        	$post['logo'] = $this->upload->data('file_name');
			$this->satker_m->edit($post);
					if($this->db->affected_rows() >0){
						$result =  new Endroid\QrCode\Builder\Builder();
                    $encode = base64_encode($post['username']); 
                    $link = base_url('ba/buktiqrcode/').$encode;
                    $nameqr = $post['username']."-qr-".substr(md5(rand()),0,10);
                    $a = $result->create();
                    $b = $a->writer(new Endroid\QrCode\Writer\PngWriter())->writerOptions([])->data($link)->encoding(new Endroid\QrCode\Encoding\Encoding('UTF-8'))->errorCorrectionLevel(new Endroid\QrCode\ErrorCorrectionLevel\ErrorCorrectionLevelHigh())->size(300)->margin(10)->roundBlockSizeMode(new Endroid\QrCode\RoundBlockSizeMode\RoundBlockSizeModeMargin())->labelText('TIDAR')->labelFont(new Endroid\QrCode\Label\Font\NotoSans(20))->labelAlignment(new Endroid\QrCode\Label\Alignment\LabelAlignmentCenter())->build();
                    $b->saveToFile('uploads/qrcode/'.$nameqr.'.png');
                    $qrc = $nameqr.'.png';
                    $this->satker_m->updateqr($post,$qrc);
			                    echo "<script>alert('data berhasil disimpan');
			                    window.location='".site_url('profile')."'
			                    </script>";
                }
        }
		
	}
}
