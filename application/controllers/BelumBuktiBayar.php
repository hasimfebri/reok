<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class BelumBuktiBayar extends CI_Controller {
	function __construct(){
		parent::__construct();
		check_not_login();
		$this->load->model('basic_m');
	   	$this->load->model('admin_m');
        $this->load->library('form_validation');
	}

	
	public function index()
	{
		$post = $this->input->post(null,TRUE);
        if(isset($post['cari'])){
            $fromdate   = $this->input->post('fromdate');
            $todate     = $this->input->post('todate');
            $query1 = $this->basic_m->buktiBayar1($fromdate,$todate);
            $query4 = $this->basic_m->buktiBayar4($fromdate,$todate);
        }else{
            $query1 = $this->basic_m->buktiBayar1();
            $query4 = $this->basic_m->buktiBayar4();
        }
        $data['row1'] = $query1;
        $data['row4'] = $query4;
		$this->template->load('templatehome','data/BelumBuktiBayar',$data);
	}
	function cekUpload1(){
        $id     = $this->input->post('id');
        if($this->input->post('date') == null || $this->input->post('date') == ''){
            $date   =  $this->input->post('date');
        }else{
            $date = date('Ym');
        }
        $row = array();
        $data = $this->basic_m->cekUpload1($id,$date);
        if($data->num_rows() > 0){
            echo json_encode($data->result());
            exit();
        }else{
            echo json_encode(array(
                'status'    => false,
                'message'   => 'Data Tidak Ditemukan'
            ));
            exit();
        }
    }
    function cekUpload4(){
        $id     = $this->input->post('id');
        if($this->input->post('date') == null || $this->input->post('date') == ''){
            $date   =  $this->input->post('date');
        }else{
            $date = date('Ym');
        }
        $row = array();
        $data = $this->basic_m->cekUpload4($id,$date);
        if($data->num_rows() > 0){
            echo json_encode($data->result());
            exit();
        }else{
            echo json_encode(array(
                'status'    => false,
                'message'   => 'Data Tidak Ditemukan'
            ));
            exit();
        }
    }
}
