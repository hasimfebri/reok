<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
    // require 'vendor/autoload.php';
    
    // use PhpOffice\PhpSpreadsheet\Spreadsheet;
    // use PhpOffice\PhpSpreadsheet\Reader\Csv;
    // use PhpOffice\PhpSpreadsheet\Reader\Xlsx;

class Remark extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->model('basic_m');
        $this->load->library('form_validation');
    }
    public function index(){
        $template = '';
        if($this->session->level == "1"){
            $template = 'templatehome';
        }else{
            $template = 'template';
        }
        $data['data'] = $this->basic_m->getremark('validasi',$this->session->idperiod,'periode');
        $data['row1'] = $this->basic_m->getremark('satup',$this->session->idperiod,'periode_id_periode');
        $data['row4'] = $this->basic_m->getremark('empatp',$this->session->idperiod,'periode_id_periode');
        $this->template->load($template,'data/remark',$data);
    }

    // public function upload()
    // {
    //     // Load plugin PHPExcel nya
    //     include APPPATH.'third_party/PHPExcel/PHPExcel.php';

    //     $config['upload_path'] = realpath('excel');
    //     $config['allowed_types'] = 'xlsx|xlx|csv';
    //     $config['max_size'] = '10000';
    //     $config['encrypt_name'] = true;

    //     $this->load->library('upload', $config);

    //     if (!$this->upload->do_upload()) {

    //         //upload gagal
    //         $this->session->set_flashdata('notif', '<div class="alert alert-danger"><b>PROSES IMPORT GAGAL!</b> '.$this->upload->display_errors().'</div>');
    //         //redirect halaman
    //         redirect('qna/');

    //     } else {

    //         $data_upload = $this->upload->data();

    //         $excelreader     = new PHPExcel_Reader_Excel2007();
    //         $loadexcel         = $excelreader->load('excel/'.$data_upload['file_name']); // Load file yang telah diupload ke folder excel
    //         $sheet             = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);

    //         $data = array();

    //         $numrow = 1;
    //         foreach($sheet as $row){
    //                         if($numrow > 1){
    //                             array_push($data, array(
    //                                 'tema' => $row['A'],
    //                                 'question'      => $row['B'],
    //                                 'answer'      => $row['C'],
    //                                 'regulasi' => $row['D'],                                    
    //                             ));
    //                 }
    //             $numrow++;
    //         }
    //         $this->db->insert_batch('qna', $data);
    //         //delete file from server
    //         unlink(realpath('excel/'.$data_upload['file_name']));

    //         //upload success
    //         $this->session->set_flashdata('notif', '<div class="alert alert-success"><b>PROSES IMPORT BERHASIL!</b> Data berhasil diimport!</div>');
    //         //redirect halaman
    //         redirect('qna/');

    //     }
    // }

   public function upload()
    {
        $post = $this->input->post(null,TRUE);
        // Load plugin PHPExcel nya
        include APPPATH.'third_party/PHPExcel/PHPExcel.php';

        $config['upload_path'] = realpath('excel');
        $config['allowed_types'] = 'xlsx|xlx|csv';
        $config['max_size'] = '10000';
        $config['encrypt_name'] = true;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload()) {

            //upload gagal
            $this->session->set_flashdata('notif', '<div class="alert alert-danger"><b>PROSES IMPORT GAGAL!</b> '.$this->upload->display_errors().'</div>');
            //redirect halaman
            redirect('qna/');

        }else{

            $data_upload = $this->upload->data();

            $excelreader     = new PHPExcel_Reader_Excel2007();
            $loadexcel         = $excelreader->load('excel/'.$data_upload['file_name']); // Load file yang telah diupload ke folder excel
            $sheet             = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);

            $data = array();

            $numrow =1;
            foreach($sheet as $row){
                            if($numrow > 1){
                                array_push($data, array(
                                    'tema'      => $row['A'],
                                    'question'  => $row['B'],
                                    'answer'    => $row['C'],
                                    'regulasi'  => $row['D'],

                                ));
                               
                    }
                $numrow++;
            }
            $this->db->insert_batch('qna', $data);
             unlink(realpath('excel/'.$data_upload['file_name']));

            //upload success
            $this->session->set_flashdata('notif', '<div class="alert alert-success"><b>PROSES IMPORT BERHASIL!</b> Data berhasil diimport!</div>');
            //redirect halaman
            redirect('qna/');
        }
    }   

    public function del(){
        $id= $this->input->post('id');
        $this->qna_m->del($id);
        if($this->db->affected_rows() >0){
            echo "<script>alert('data berhasil dihapus');
            window.location='".site_url('qna')."'
            </script>";
        }
    }
    public function edit($id){
        $this->form_validation->set_rules('tema', 'Tema', 'required');
        $this->form_validation->set_rules('question', 'Question', 'required');
        $this->form_validation->set_rules('answer', 'Answer', 'required');
        $this->form_validation->set_message('required', '%s Harus diisi!');
        $this->form_validation->set_error_delimiters('<span style="color:red">','</span>');
        if ($this->form_validation->run() == FALSE){
            $query = $this->basic_m->getById("qna","id_qna",$id);
            if($query->num_rows() > 0){
                $data['row'] = $query;
                $this->template->load('template','data/qna_edit', $data);
            }else{
                echo "<script>alert('data tidak ditemukan')</script>";
                redirect('qna');
            }
        }else{
            $post = $this->input->post(null,TRUE);
            $this->qna_m->edit($post);
            if ($this->db->trans_status() === FALSE){
                echo "<script>alert('tidak ada data yang diubah');
                window.location='".site_url('qna')."'
                </script>";
            }else{
                echo "<script>alert('data berhasil disimpan');
                window.location='".site_url('qna')."'
                </script>";
            }
        }
    }   
}