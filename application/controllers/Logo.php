<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Logo extends CI_Controller {
	function __construct(){
		parent::__construct();
		check_not_login();
		$this->load->model('basic_m');
		$this->load->model('satker_m');
        $this->load->library('form_validation');
	}

	public function index()
	{
		$this->template->load('templatehome','data/logo_bpjs');
	}
	public function add()
    {
          $config['upload_path']          = './uploads/logo/';
                $config['allowed_types']        = 'jpg|png|jpeg';
                $config['file_name']            = 'Logo-'.date('ymd').'-'.substr(md5(rand()),0,10);
                $post = $this->input->post(null,TRUE);
                $this->load->library('upload', $config);
                if(@$_FILES['logo']['name'] != null){
                if ($this->upload->do_upload('logo')){
                $post['logo'] = $this->upload->data('file_name');
                $this->satker_m->addlogo($post);
                if($this->db->affected_rows() >0){
                    $result =  new Endroid\QrCode\Builder\Builder();
                    $encode = base64_encode("1"); 
                    $link = base_url('ba/buktiqrcode/').$encode;
                    $nameqr = "bpjs-qr-".substr(md5(rand()),0,10);
                    $a = $result->create();
                    $b = $a->writer(new Endroid\QrCode\Writer\PngWriter())->writerOptions([])->data($link)->encoding(new Endroid\QrCode\Encoding\Encoding('UTF-8'))->errorCorrectionLevel(new Endroid\QrCode\ErrorCorrectionLevel\ErrorCorrectionLevelHigh())->size(300)->margin(10)->roundBlockSizeMode(new Endroid\QrCode\RoundBlockSizeMode\RoundBlockSizeModeMargin())->labelText('TIDAR')->labelFont(new Endroid\QrCode\Label\Font\NotoSans(20))->labelAlignment(new Endroid\QrCode\Label\Alignment\LabelAlignmentCenter())->build();
                    $b->saveToFile('uploads/qrcodebpjs/'.$nameqr.'.png');
                    $qrc = $nameqr.'.png';
                    $this->satker_m->updateqrbpjs($qrc);
                    if($this->db->affected_rows() >0){
                    echo "<script>alert('data berhasil disimpan');
                    window.location='".site_url('satker')."'
                    </script>";
                }
                }
            }else{
                $error = $this->upload->display_errors();
                echo "<script>alert($error);
                window.location='".site_url('satker')."'
                </script>";
            }
    }
  }
    public function enkripsi(){
         $plaintext = "user4";  
      //Encode plaintext  
      $encode = base64_encode($plaintext);  
      //Decode plaintext  
      $decode = base64_decode($encode);  
      echo "teks = ".$plaintext."<br/>";   
      echo "teks yang diencode = ".$encode."<br/>";  
      echo "teks yang didecode = ".$decode;  
    }
}
