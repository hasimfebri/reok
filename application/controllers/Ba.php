<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ba extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('lampiran_m');
		$this->load->model('basic_m');
	}
	public function index()
	{
		check_not_login();
		$id = $this->session->idperiod;
		$satker = $this->session->idperiod;
		$data['total'] = $this->basic_m->getById("iuran_month","periode_id_periode",$id)->num_rows(); 
		$this->template->load('template','data/cetak_ba',$data);
	}
	public function cetakLampiran2(){
		check_not_login();
		$this->load->library("ExcelReport");
		$id_session = $this->session->userdata('idperiod');
		$satker 	= $this->session->satker;
		// $datasakter = $this->lampiran_m->getSatker($id_session)->row();
		$date		= date("d F Y");
			$file 		= "./uploads/lampiran2.xls";
			$objPHPExcel = PHPExcel_IOFactory::load($file);
			$objPHPExcel->getActiveSheet(0);
		$data = $this->lampiran_m->getAlldata($id_session)->result();
		// $objPHPExcel->getActiveSheet()->setCellValue('A1',"BERITA ACARA REKONSILIASI JUMLAH PEKERJA DAN BESARAN IURAN JAMINAN KESEHATAN NASIONAL ANTARA BPJS KESEHATAN KANTOR CABANG MAGELANG TAHUN ".date("Y")." DENGAN ".$satker."");
		// $objPHPExcel->getActiveSheet()->setCellValue('D4',$date);
		// $objPHPExcel->getActiveSheet()->setCellValue('i24',$date);
		$pegawai	= 0;
		$isteri  	= 0;
		$thp 		= 0;
		$dpi		= 0;
		$i1  		= 0;
		$i4 		= 0;
		$a1			= 0;
		$a4  		= 0;
		$k1 		= 0;
		$k4 		= 0;
		foreach ($data as $key){
			$index = (int)$key->bulan + 13;
			$objPHPExcel->getActiveSheet()->setCellValue('E'.$index,"".$key->jumlah_pegawai);
			$objPHPExcel->getActiveSheet()->setCellValue('F'.$index,"".($key->jml_is + $key->jml_an ));
			$objPHPExcel->getActiveSheet()->setCellValue('G'.$index,"Rp.".number_format($key->thp));
			$objPHPExcel->getActiveSheet()->setCellValue('H'.$index,"Rp.".number_format($key->dpi));
			$objPHPExcel->getActiveSheet()->setCellValue('I'.$index,"Rp.".number_format($key->iuran_1, 2, ",", "."));
			$objPHPExcel->getActiveSheet()->setCellValue('J'.$index,"Rp.".number_format($key->iuran_4, 2, ",", "."));
			$objPHPExcel->getActiveSheet()->setCellValue('K'.$index,"Rp.".number_format($key->actual_iuran_1, 2, ",", "."));
			$objPHPExcel->getActiveSheet()->setCellValue('L'.$index,"Rp.".number_format($key->actual_iuran_4, 2, ",", "."));
			$objPHPExcel->getActiveSheet()->setCellValue('M'.$index,"Rp.".number_format($key->kurang1, 2, ",", "."));
			$objPHPExcel->getActiveSheet()->setCellValue('N'.$index,"Rp.".number_format($key->kurang4, 2, ",", "."));
			$pegawai	+= $key->jumlah_pegawai;
			$isteri  	+= $key->jml_is + $key->jml_an;
			$thp 		+= $key->thp;
			$dpi		+= $key->dpi;
			$i1  		+= $key->iuran_1;
			$i4 		+= $key->iuran_4;
			$a1			+= $key->actual_iuran_1;
			$a4  		+= $key->actual_iuran_4;
			$k1 		+= $key->kurang1;
			$k4 		+= $key->kurang4;
		}
		$objPHPExcel->getActiveSheet()->setCellValue('E26',"".$pegawai);
		$objPHPExcel->getActiveSheet()->setCellValue('F26',"".$isteri);		
		$objPHPExcel->getActiveSheet()->setCellValue('G26',"Rp.".number_format($thp, 2, ",", "."));		
		$objPHPExcel->getActiveSheet()->setCellValue('H26',"Rp.".number_format($dpi, 2, ",", "."));		
		$objPHPExcel->getActiveSheet()->setCellValue('I26',"Rp.".number_format($i1, 2, ",", "."));		
		$objPHPExcel->getActiveSheet()->setCellValue('J26',"Rp.".number_format($i4, 2, ",", "."));		
		$objPHPExcel->getActiveSheet()->setCellValue('K26',"Rp.".number_format($a1, 2, ",", "."));		
		$objPHPExcel->getActiveSheet()->setCellValue('L26',"Rp.".number_format($a4, 2, ",", "."));		
		$objPHPExcel->getActiveSheet()->setCellValue('M26',"Rp.".number_format($k1, 2, ",", "."));		
		$objPHPExcel->getActiveSheet()->setCellValue('N26',"Rp.".number_format($k4, 2, ",", "."));	
		$objPHPExcel->getActiveSheet()->setCellValue('I31',"Rp.".number_format($k4 + $k1, 2, ",", "."));
		$objPHPExcel->getActiveSheet()->setCellValue('M37',"".$date);


		header('Content-Type: application/vnd.ms-excel'); //mime type
		header('Content-Disposition: attachment;filename="Lampiran_BA_'.$satker.'.xls"'); //tell browser what's the file name
		header('Cache-Control: max-age=0'); //no cache
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');
	}
	public function cetaklampiran(){
		check_not_login();
		$post = $this->input->post(null,TRUE);
		$data['satker'] = $this->session->satker;
		if ($this->session->level == "1") {
        	$data['kota'] = $this->basic_m->getById("satker","username",$this->session->usersatker)->row()->kota;
        }else{
        	$data['kota'] = $this->basic_m->getById("satker","username",$this->session->username)->row()->kota;
        }
        $data['tgl'] = $this->basic_m->getById('periode','id_periode',$this->session->idperiod)->row()->lastdate;
        $data['tahun'] = $this->session->tahun;
		$data['row'] = $this->lampiran_m->lampimport($post);
		$data['rowsatu'] = $this->lampiran_m->bayarsatu($post);
		$data['rowempat'] = $this->lampiran_m->bayarempat($post);
		$data['rowbpjs'] = $this->lampiran_m->lampimportbpjs($post);
		$data['rowfikssatu'] = $this->lampiran_m->fikssatu($post);
		$data['rowfiksempat'] = $this->lampiran_m->fiksempat($post);
		$this->load->view('laporan/lampiran',$data);
	}
	public function cetakba(){
		check_not_login();
		$post = $this->input->post(null,TRUE);
		$qrbpjs = $this->basic_m->getById("logobpjs","idlogo","1")->row()->qr;
        $data['imglogo'] = $_SERVER["DOCUMENT_ROOT"]."\uploads\qrcodebpjs\\".$qrbpjs;
        $data['imglogo2'] =base_url("uploads/qrcodebpjs/".$qrbpjs);
        if ($this->session->level == "1") {
        	$qr = $this->basic_m->getById("satker","username",$this->session->usersatker)->row()->qr;
        }else{
        	$qr = $this->basic_m->getById("satker","username",$this->session->username)->row()->qr;
        }
        $data['imgsatker2'] = base_url("uploads/qrcode/".$qr);
        $data['imgsatker'] = $_SERVER["DOCUMENT_ROOT"]."\uploads\qrcode\\".$qr;
        // $data['row'] = $this->lampiran_m->batagihan($post);
        if ($this->session->level == "1") {
        	$data['kota'] = $this->basic_m->getById("satker","username",$this->session->usersatker)->row()->kota;
        }else{
        	$data['kota'] = $this->basic_m->getById("satker","username",$this->session->username)->row()->kota;
        }
        $data['tgl'] = $this->basic_m->getById('periode','id_periode',$this->session->idperiod)->row()->lastdate;
        $data['satker'] = $this->session->satker;
        $data['tahun'] = $this->session->tahun;
        $data['row'] = $this->lampiran_m->lampimport($post);
        $data['totaltagihan'] = $this->lampiran_m->totaltagihan($post)->row();
        $data['rowfikssatu'] = $this->lampiran_m->fikssatu($post);
		$data['rowfiksempat'] = $this->lampiran_m->fiksempat($post);
		$data['rowbpjs'] = $this->lampiran_m->lampimportbpjs($post);
		if ($this->lampiran_m->totalsatu($post)->num_rows()>0) {
			$data['bayarsatu'] = $this->lampiran_m->totalsatu($post)->row()->satu;
		}else{
			$data['bayarsatu'] = 0;
		}
		if ($this->lampiran_m->totalempat($post)->num_rows()>0) {
			$data['bayarempat'] = $this->lampiran_m->totalempat($post)->row()->empat;
		}else{
			$data['bayarempat'] = 0;
		}
		$html = $this->load->view('laporan/ba',$data,true);
        $this->fungsi->pdfGenerator($html,'Laporan','A4','portrait'); 
    }
	public function cetakba2(){
		// check_not_login();
		// $post = $this->input->post(null,TRUE);
		// $qrbpjs = $this->basic_m->getById("logobpjs","idlogo","1")->row()->qr;
        // $data['imglogo'] = $_SERVER["DOCUMENT_ROOT"]."\uploads\qrcodebpjs\\".$qrbpjs;
        // $data['imglogo2'] =base_url("uploads/qrcodebpjs/".$qrbpjs);
        check_not_login();
		$this->load->library("ExcelReport");
		$id_session = $this->session->userdata('idperiod');
		$satker 	= $this->session->satker;
		// $datasakter = $this->lampiran_m->getSatker($id_session)->row();
		$date		= date("d F Y");
			$file 		= "./uploads/lampiranba.xls";
			$objPHPExcel = PHPExcel_IOFactory::load($file);
			$objPHPExcel->getActiveSheet(0);
		$data = $this->lampiran_m->getAlldata($id_session)->result();
		// $objPHPExcel->getActiveSheet()->setCellValue('A1',"BERITA ACARA REKONSILIASI JUMLAH PEKERJA DAN BESARAN IURAN JAMINAN KESEHATAN NASIONAL ANTARA BPJS KESEHATAN KANTOR CABANG MAGELANG TAHUN ".date("Y")." DENGAN ".$satker."");
		// $objPHPExcel->getActiveSheet()->setCellValue('D4',$date);
		// $objPHPExcel->getActiveSheet()->setCellValue('i24',$date);
		$i1  		= 0;
		$i4 		= 0;
		$a1			= 0;
		$a4  		= 0;
		$k1 		= 0;
		$k4 		= 0;
		foreach ($data as $key){
			$index = (int)$key->bulan + 14;
			$index2 = (int)$key->bulan + 31;
			$index3 = (int)$key->bulan + 48;
			$objPHPExcel->getActiveSheet()->setCellValue('E'.$index,"Rp.".number_format($key->iuran_1, 2, ",", "."));
			$objPHPExcel->getActiveSheet()->setCellValue('F'.$index,"Rp.".number_format($key->iuran_4, 2, ",", "."));
			$objPHPExcel->getActiveSheet()->setCellValue('E'.$index2,"Rp.".number_format($key->actual_iuran_1, 2, ",", "."));
			$objPHPExcel->getActiveSheet()->setCellValue('F'.$index2,"Rp.".number_format($key->actual_iuran_4, 2, ",", "."));
			$objPHPExcel->getActiveSheet()->setCellValue('E'.$index3,"Rp.".number_format($key->kurang1, 2, ",", "."));
			$objPHPExcel->getActiveSheet()->setCellValue('F'.$index3,"Rp.".number_format($key->kurang4, 2, ",", "."));
			$i1  		+= $key->iuran_1;
			$i4 		+= $key->iuran_4;
			$a1			+= $key->actual_iuran_1;
			$a4  		+= $key->actual_iuran_4;
			$k1 		+= $key->kurang1;
			$k4 		+= $key->kurang4;
		}	
		$objPHPExcel->getActiveSheet()->setCellValue('E27',"Rp.".number_format($i1, 2, ",", "."));		
		$objPHPExcel->getActiveSheet()->setCellValue('F27',"Rp.".number_format($i4, 2, ",", "."));		
		$objPHPExcel->getActiveSheet()->setCellValue('E44',"Rp.".number_format($a1, 2, ",", "."));		
		$objPHPExcel->getActiveSheet()->setCellValue('F44',"Rp.".number_format($a4, 2, ",", "."));	
		$objPHPExcel->getActiveSheet()->setCellValue('E61',"Rp.".number_format($k1, 2, ",", "."));		
		$objPHPExcel->getActiveSheet()->setCellValue('F61',"Rp.".number_format($k4, 2, ",", "."));


		header('Content-Type: application/vnd.ms-excel'); //mime type
		header('Content-Disposition: attachment;filename="Lampiran'.$satker.'.xls"'); //tell browser what's the file name
		header('Cache-Control: max-age=0'); //no cache
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output'); 
    }
    public function buktiqrcode($id=null){
    	if ($id == null) {
    		header("Location:".site_url('auth/logout'));
			die();
    	}
    	$decode = base64_decode($id);
    	if ($decode != "1") {
    		$query = $this->basic_m->getById("satker",'username',$decode)->row();
    	}else{
    		$query = $this->basic_m->getById("logobpjs",'idlogo',$decode)->row();
    	}
    	$data ['image'] = $query->gambar;
    	$this->load->view('laporan/buktiqr',$data);
    }
}
