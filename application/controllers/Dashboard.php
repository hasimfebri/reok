<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	function __construct(){
		parent::__construct();
		check_not_login();
		$this->load->model('Basic_m');
		$this->load->model('lampiran_m');
	}
	public function index()
	{
		$id_session = $this->session->userdata('idperiod');
		if($id_session){
			if ($this->session->level == "1") {
				$data['rows'] = $this->Basic_m->getById("import_bpjs","periode_id_periode",$id_session)->result();
			}else if ($this->session->level == "1"){
				$data['rows'] = $this->Basic_m->getById("import_satker","periode_id_periode",$id_session)->result();
			}else{
				$data['rows'] = $this->Basic_m->getById("import_satker","periode_id_periode",$id_session)->result();
			}
			$data['data'] = $this->lampiran_m->getAlldata($id_session);
			$post['periode'] = $id_session;
			$data['row'] = $this->lampiran_m->lampimport($post);
			$data['rowsatu'] = $this->lampiran_m->bayarsatu($post);
			$data['rowempat'] = $this->lampiran_m->bayarempat($post);
			$data['rowbpjs'] = $this->lampiran_m->lampimportbpjs($post);
			$data['rowfikssatu'] = $this->lampiran_m->fikssatu($post);
			$data['rowfiksempat'] = $this->lampiran_m->fiksempat($post);
			$this->template->load('template','dashboard',$data);
		}else{
			$this->template->load('templatehome','dashboardadmin');
		}
	}
	function sendnotif(){
	    $jenis = $this->input->post('jenis');
	    if($jenis == 1){
	        $data  = $this->Basic_m->BelumExport();
	        $pesan1 = urlencode("Hari ini ".date("d-M-Y").", silahkan untuk melakukan upload data bagi satker yang belum upload data di bulan ".date("F")." , antara lain : \n\n");
	    }else if($jenis == 2){
	        $data  = $this->Basic_m->BuktiBayar1();
	        $pesan1 = urlencode("Hari ini ".date("d-M-Y").", silahkan untuk melakukan upload data bagi satker yang belum upload bukti bayar 1% di bulan ".date("F")." , antara lain : \n\n");
	    }else if($jenis == 3){
	        $data  = $this->Basic_m->BuktiBayar4();
	        $pesan1 = urlencode("Hari ini ".date("d-M-Y").", silahkan untuk melakukan upload data bagi satker yang belum upload bukti bayar 4% di bulan ".date("F")." , antara lain : \n\n");
	    }
	        if($data->num_rows() > 0){
	            $pesan2 = "";
	            $pesan3 = "";
	            $pesan4 = "";
	            $pesan5 = "";
	            $no = 1;
	            $pno = 1;
                foreach ($data->result() as $key => $value){
                    
                    if ($pno == 1){
                        $pesan1 .= urlencode($no.".".$value->nama."\n\n");
                        if(strlen($pesan1) + strlen($value->nama) > 3500){
                            $pno = 2;
                        }
                    }else if ($pno == 2){
                        $pesan2 .= urlencode($no.". ".$value->nama."\n\n");
                        if(strlen($pesan2) + strlen($value->nama) > 3500){
                            $pno = 3;
                        }
                    }else if ($pno == 3){
                        $pesan3 .= urlencode($no.". ".$value->nama."\n\n");
                        if(strlen($pesan3) + strlen($value->nama) > 3500){
                            $pno = 4;
                        }
                    }else if ($pno == 4){
                        $pesan4 .= urlencode($no.". ".$value->nama."\n\n");
                        if(strlen($pesan4) + strlen($value->nama) > 3500){
                            $pno = 5;
                        }
                    }
                    else if ($pno == 5){
                        $pesan5 .= urlencode($no.". ".$value->nama."\n\n");
                        if(strlen($pesan5) + strlen($value->nama) > 3500){
                            $pno = 6;
                        }
                    }
                    $no++;
                }
                $secret_token =  "5349669683:AAE8jV_jT2FkaTxovyUY_nY8RkXQHRSoyoM";
                $telegram_id = "-742569295";
	            $url = "https://api.telegram.org/bot" . $secret_token . "/sendMessage?parse_mode=markdown&chat_id=-742569295";
	            $url = $url . "&text=".$pesan1;
	            $ch = curl_init();
	            $optArray = array(
	                    CURLOPT_URL => $url,
	                    CURLOPT_RETURNTRANSFER => true
	            );
	            curl_setopt_array($ch, $optArray);
	            $result = curl_exec($ch);
	            curl_close($ch);
	            
	            if($pesan2 != ""){
	                $secret_token =  "5349669683:AAE8jV_jT2FkaTxovyUY_nY8RkXQHRSoyoM";
                    $telegram_id = "-742569295";
	                $url = "https://api.telegram.org/bot" . $secret_token . "/sendMessage?parse_mode=markdown&chat_id=-742569295";
	                $url = $url . "&text=".$pesan2;
	                $ch = curl_init();
	                $optArray = array(
	                        CURLOPT_URL => $url,
	                        CURLOPT_RETURNTRANSFER => true
	                        );
	                curl_setopt_array($ch, $optArray);
	                $result = curl_exec($ch);
	                curl_close($ch);
	                    if($pesan3 != ""){
	                        $secret_token =  "5349669683:AAE8jV_jT2FkaTxovyUY_nY8RkXQHRSoyoM";
                            $telegram_id = "-742569295";
	                        $url = "https://api.telegram.org/bot" . $secret_token . "/sendMessage?parse_mode=markdown&chat_id=-742569295";
	                        $url = $url . "&text=".$pesan3;
	                        $ch = curl_init();
	                        $optArray = array(
	                                CURLOPT_URL => $url,
	                                CURLOPT_RETURNTRANSFER => true
	                        );
	                        curl_setopt_array($ch, $optArray);
	                        $result = curl_exec($ch);
	                        curl_close($ch); 
	                        if($pesan4!=""){
	                            $secret_token =  "5349669683:AAE8jV_jT2FkaTxovyUY_nY8RkXQHRSoyoM";
                                $telegram_id = "-742569295";
	                            $url = "https://api.telegram.org/bot" . $secret_token . "/sendMessage?parse_mode=markdown&chat_id=-742569295";
	                            $url = $url . "&text=".$pesan4;
	                            $ch = curl_init();
	                            $optArray = array(
	                                    CURLOPT_URL => $url,
	                                    CURLOPT_RETURNTRANSFER => true
	                            );
	                            curl_setopt_array($ch, $optArray);
	                            $result = curl_exec($ch);
	                            curl_close($ch);
	                            if($pesan5!=""){
	                            $secret_token =  "5349669683:AAE8jV_jT2FkaTxovyUY_nY8RkXQHRSoyoM";
                                $telegram_id = "-742569295";
	                            $url = "https://api.telegram.org/bot" . $secret_token . "/sendMessage?parse_mode=markdown&chat_id=-742569295";
	                            $url = $url . "&text=".$pesan5;
	                            $ch = curl_init();
	                            $optArray = array(
	                                    CURLOPT_URL => $url,
	                                    CURLOPT_RETURNTRANSFER => true
	                            );
	                            curl_setopt_array($ch, $optArray);
	                            $result = curl_exec($ch);
	                            curl_close($ch); 
	                        }
	                        }
	                    }
	            }
	            echo json_encode(array(
	                    'status'    => true,
	                    'message'   => 'Send Notif succes'
	                ));
                // file_get_contents("https://api.telegram.org/bot$token/sendMessage?" . http_build_query($data) );
	           // foreach ($data->result() as $value){
	                
	           // }
	        }
	    
	}
}
