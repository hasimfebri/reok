<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengajuan extends CI_Controller {
	function __construct(){
		parent::__construct();
		check_not_login();
		$this->load->model('basic_m');
        $this->load->model('Pengajuan_m');
        $this->load->library('form_validation');
		$this->load->library('encryption');
		date_default_timezone_set("Asia/Bangkok");
	}
	public function index()
	{
		$data['tanggal'] = 11;
		$data['jumlah']  =$this->Pengajuan_m->getexist($this->session->idperiod)->num_rows();
		$data['pengajuan']  =$this->Pengajuan_m->getPengajuan($this->session->idperiod)->num_rows();
        // if($this->session->level == "1"){
		// 	$this->template->load('templatehome','data/Pengajuan',$data);
		// }else{
			$this->template->load('template','data/pengajuan',$data);
		// }
	}
	public function downloadpengajuan(){
	    $data['row'] = $this->Pengajuan_m->getData($this->session->idperiod);
	    $data['satker'] = $this->session->satker;
	    $data['bulan'] = date('F');
        $this->load->view('laporan/datapengajuan',$data);
	}
	function getAnggota(){
	    $anak = "";
	    $status = "";
	   $data = $this->Pengajuan_m->getAnggota()->result();
	   echo "<table id='exampleanggota' class='display responsive nowrap' style='width:100%'>
        <thead>
            <tr>";
			echo "<th>#</th>";
			if ($this->session->level == "1"){
				echo "<th><input type='checkbox' onchange='checkAll(this,`2`)' name='chk[]'></th>";
			}
			echo"
                <th>Status</th>
				<th>Jenis</th>
                <th>No KK</th>
                <th>NIK </th>
                <th>Nama</th>
                <th>NIK Sumber</th>
                <th>Silsilah</th>
                <th>Tgl Lahir</th>
                <th>Bukti</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>";
        $no=1;
        foreach($data as $key){
            if($key->status > 2){
                $anak = $key->status - 3;
                $status = "Anak ke-".strval($anak);
            }else{
                $anak = "-";
                if($key->status == 1){
                    $status = "Suami";
                }else{
                   $status = "Istri"; 
                }
            }
            echo "<tr>";
			echo "<td>".$no++."</td>";
			if($this->session->level == "1"){
				if($key->status2 == 2){
					echo "<td>-</td>";
				}else{
					echo "<td><input class='ck2' type='checkbox' name='chkbox[]' value='".$key->id_impsatker."'></td>";
				}
			}
            echo "<td onClick='getTolak(`".$key->id_impsatker."`,`2`)'>".(($key->status2 == 0) ? 	'Menunggu' :'Ditolak')."</td>";
            echo "<td>".(($key->jenis == 1) ? 	'Pengajuan' :'Penonaktifan')."</td>";
			echo "<td>".$key->no_kk."</td>";
            echo "<td>".substr($this->encryption->decrypt($key->nik),0,12).'****'."</td>";
            echo "<td>".$key->nama."</td>";
            echo "<td>".substr($this->encryption->decrypt($key->nik_sumber),0,12).'****'."</td>";
            echo "<td>".$status."</td>";
            echo "<td>".$key->tgl_lahir."</td>";
            echo "<td>";
            if($key->bukti != ""){
					echo "<a href='uploads/pengajuan/".$key->bukti."' class='btn btn-success btn-sm m-b-10 m-l-5' target='_blank'>Lihat</a>";
				}else{
				    echo "-";
				}
			echo "</td>";
			echo "<td>";
			if($this->session->level == "2"){
            	if($key->status2==2){
            	    echo "<button onclick='hapusPengajuan(`".$key->id_impsatker."`,`2`)' class='btn btn-danger'>HAPUS</button>";
            	}
            	echo"</td>";
			}else if($this->session->level == "1"){
				if($key->status2 == 2){
					echo "-";
				}else{
					echo "<button onclick='Tolak(`".$key->id_impsatker."`,`2`)' class='btn btn-danger'>TOLAK</button>";
				}
				echo"</td>";
			}
            echo "</tr>";
        }
        echo "</tbody>
        <tfoot>
            <tr>
                <th>#</th>";
				if ($this->session->level == "1"){
					echo "<th></th>";
				}
				echo"
                <th>Status</th>
				<th>Jenis</th>
                <th>No KK</th>
                <th>NIK </th>
                <th>Nama</th>
                <th>NIK Sumber</th>
                <th>Silsilah</th>
                <th>Tgl Lahir</th>
                <th>Bukti</th>
                <th>Action</th>
            </tr>
        </tfoot>
        </table>";
	}
    public function getData()
    {
        $data = $this->Pengajuan_m->getData($this->session->idperiod)->result();
        echo "<table id='example' class='display responsive nowrap' style='width:100%'>
        <thead>
            <tr>";
			echo "<th>#</th>";
			if ($this->session->level == "1"){
				echo "<th><input type='checkbox' onchange='checkAll(this,``)' name='chk[]'></th>";
			}
			echo"
                <th>Status</th>
				<th>Jenis</th>
                <th>nama_satker</th>
                <th>nama_pegawai</th>
                <th>nik</th>
                <th>nip</th>
                <th>jml_is</th>
                <th>jml_an</th>
                <th>penghasilan</th>
                <th>tunjangan</th>
                <th>thp</th>
                <th>dpi</th>
                <th>iuran_1</th>
                <th>iuran_4</th>
                <th>total_i</th>
                <th>bulan</th>
                <th>Action</th>
				<th>Bukti</th>
            </tr>
        </thead>
        <tbody>";
        $no=1;
        foreach($data as $key){
            echo "<tr>";
			echo "<td>".$no++."</td>";
			if($this->session->level == "1"){
				if($key->status == 2){
					echo "<td>-</td>";
				}else{
					echo "<td><input class='ck' type='checkbox' name='chkbox[]' value='".$key->id_impsatker."'></td>";
				}
			}
            echo "<td onClick='getTolak(`".$key->id_impsatker."`,`1`)'>".(($key->status == 0) ? 	'Menunggu' :'Ditolak')."</td>";
            echo "<td>".(($key->jenis == 1) ? 	'Pengajuan' :'Mutasi')."</td>";
			echo "<td>".$key->nama_satker."</td>";
            echo "<td>".$key->nama_pegawai."</td>";
            echo "<td>".substr($this->encryption->decrypt($key->nik),0,12).'****'."</td>";
            // echo "<td>".$key->nik."</td>";
            echo "<td>".$key->nip."</td>";
            echo "<td>".$key->jml_is."</td>";
            echo "<td>".$key->jml_an."</td>";
            echo "<td>".$key->penghasilan."</td>";
            echo "<td>".$key->tunjangan."</td>";
            echo "<td>".$key->thp."</td>";
            echo "<td>".$key->dpi."</td>";
            echo "<td>".$key->iuran_1."</td>";
            echo "<td>".$key->iuran_4."</td>";
            echo "<td>".$key->total_i."</td>";
            echo "<td>".$key->bulan."</td>";
            echo "<td>";
			if($this->session->level == "2"){
            	if($key->status==2){
            	    echo "<button onclick='editPengajuan(".$key->id_impsatker.")' class='btn btn-warning'>EDIT</button>";
            	}else if($key->status==0){
            	    echo "<button onclick='hapusPengajuan(`".$key->id_impsatker."`,`1`)' class='btn btn-danger'>HAPUS</button>";
            	}
            	echo"</td>";
			}else if($this->session->level == "1"){
				if($key->status == 2){
					echo "-";
				}else{
					echo "<button onclick='Tolak(`".$key->id_impsatker."`,`1`)' class='btn btn-danger'>TOLAK</button>";
				}
				echo"</td>";
			}
				echo"<td>";
				if($key->bukti != ""){
					echo "<a href='uploads/pengajuan/".$key->bukti."' class='btn btn-success btn-sm m-b-10 m-l-5' target='_blank'>Lihat</a>";
				}else{
					if($this->session->level == "2"){
						echo "<button onclick='addBukti(".$key->id_impsatker.")' class='btn btn-success'>ADD</button>";
					}else{
						echo "-";
					}
				}
			echo "</td>
            </tr>";
        }
        echo "</tbody>
        <tfoot>
            <tr>
                <th>#</th>";
				if ($this->session->level == "1"){
					echo "<th></th>";
				}
				echo"
                <th>Status</th>
                <th>Jenis</th>
				<th>nama_satker</th>
                <th>nama_pegawai</th>
                <th>nik</th>
                <th>nip</th>
                <th>jml_is</th>
                <th>jml_an</th>
                <th>penghasilan</th>
                <th>tunjangan</th>
                <th>thp</th>
                <th>dpi</th>
                <th>iuran_1</th>
                <th>iuran_4</th>
                <th>total_i</th>
                <th>bulan</th>
                <th>periode_id_periode</th>
				<th>Bukti</th>
            </tr>
        </tfoot>
        </table>";
        
    }
    function addPengajuan()
	{
		check_user();
		$config['upload_path']          = './uploads/pengajuan/';
        $config['allowed_types']        = 'pdf|jpg|png|jpeg';
        $config['file_name']            = 'bukti'.date('ymd').'-'.substr(md5(rand()),0,10);
        $post = $this->input->post(null,TRUE);
		$this->load->library('upload', $config);
		if($this->upload->do_upload('bukti')){
			$bukti  = $this->upload->data('file_name');
		    $cekData = $this->Pengajuan_m->getPeriode($this->session->idperiod);
		    if ($cekData->num_rows() < 1){
		    	echo json_encode(array(
		    		'status'	=> false,
		    		'message'	=> 'periode no found',
		    	));
		    	exit();
		    }
		    $bulan = date('Y-m-d');
		    if($this->input->post('jenis')== '1' || $this->input->post('jenis')== '3'){
		    	$jenis = 1;
		    }else if($this->input->post('jenis')== '2' || $this->input->post('jenis')== '4'){
		    	$jenis = 2;
		    }
		        $data = array(
		    	"nama_satker"	=>  $cekData->row()->nama,
		    	"nama_pegawai"	=> 	$this->input->post('pegawai'),
		    	"nik"			=> 	$this->encryption->encrypt($this->input->post('nik')),
		    	"nip"			=> 	$this->input->post('nip'),
		    	"jml_is"		=> 	$this->input->post('jml_is'),
		    	"jml_an"		=> 	$this->input->post('jml_an'),
		    	"penghasilan"	=> 	$this->input->post('penghasilan'),
		    	"tunjangan"		=> 	$this->input->post('tunjangan'),
		    	"thp"			=> 	$this->input->post('thp'),
		    	"dpi"			=> 	$this->input->post('dpi'),
		    	"iuran_1"		=> 	$this->input->post('iuran_1'),
		    	"iuran_4"		=> 	$this->input->post('iuran_4'),
		    	"total_i"		=> 	$this->input->post('total_i'),
		    	"bulan"			=>  $bulan,
		    	"periode_id_periode" => $this->session->idperiod,
		    	"status"		=> 0,
		    	"jenis"			=> $jenis,
		    	"bukti"			=> $bukti,
		    );
		    if($this->input->post('jenis')== '1' || $this->input->post('jenis')== '2'){
		    	$result = $this->Pengajuan_m->addPengajuan($data);
		    }else if($this->input->post('jenis')== '3' || $this->input->post('jenis')== '4'){
		    	$result = $this->Pengajuan_m->updatePengajuan($data,$this->input->post('idp'));
		    }
		    
		    if($result){
		        $pesan = urlencode("Ada Data Pengajaun dari satker ".$cekData->row()->nama." Silahkan cek https://kc-magelang.com/tidar \n\n");
                    $secret_token =  "5349669683:AAE8jV_jT2FkaTxovyUY_nY8RkXQHRSoyoM";
                    $telegram_id = "-742569295";
	                $url = "https://api.telegram.org/bot" . $secret_token . "/sendMessage?parse_mode=markdown&chat_id=-742569295";
	                $url = $url . "&text=".$pesan;
	                $ch = curl_init();
	                $optArray = array(
	                       CURLOPT_URL => $url,
	                       CURLOPT_RETURNTRANSFER => true
	                );
	               curl_setopt_array($ch, $optArray);
	               $send = curl_exec($ch);
	               curl_close($ch); 
		        
		    	echo "<script>alert('data berhasil disimpan');
				window.location='".site_url('Pengajuan')."'
				</script>";
		    }else{
		    	echo "<script>alert('data gagal disimpan');
				window.location='".site_url('Pengajuan')."'
				</script>";
		    }
		}else{
		    	echo "<script>alert('Bukti Gagal  diUpload');
				window.location='".site_url('Pengajuan')."'
				</script>";
		}
	}
	function addBukti(){
		check_user();
		$config['upload_path']          = './uploads/pengajuan/';
        $config['allowed_types']        = 'pdf|jpg|png|jpeg';
        $config['file_name']            = 'bukti'.date('ymd').'-'.substr(md5(rand()),0,10);
        $post = $this->input->post(null,TRUE);
		$this->load->library('upload', $config);

		if($this->upload->do_upload('bukti')){
			$post['bukti']  = $this->upload->data('file_name');
			$this->Pengajuan_m->addBukti($post);
			if($this->db->affected_rows() >0){
			 //   $pesan = urlencode("Ada Data Penagajaun dari satker ".$cekData->row()->nama." Silahkan cek https://kc-magelang.com/tidar \n\n");
    //                 $secret_token =  "5349669683:AAE8jV_jT2FkaTxovyUY_nY8RkXQHRSoyoM";
    //                 $telegram_id = "-742569295";
	   //             $url = "https://api.telegram.org/bot" . $secret_token . "/sendMessage?parse_mode=markdown&chat_id=-742569295";
	   //             $url = $url . "&text=".$pesan;
	   //             $ch = curl_init();
	   //             $optArray = array(
	   //                    CURLOPT_URL => $url,
	   //                    CURLOPT_RETURNTRANSFER => true
	   //             );
	               //curl_setopt_array($ch, $optArray);
	               //$send = curl_exec($ch);
	               //curl_close($ch);
				echo "<script>alert('data berhasil disimpan');
				window.location='".site_url('Pengajuan')."'
				</script>";
			}
		}else{
			$error = $this->upload->display_errors();
			echo "<script>alert($error);
			window.location='".site_url('Pengajuan')."'
			</script>";
		}
	}
	function cekBukti(){
		check_user();
		$config['upload_path']          = './uploads/pengajuan/';
        $config['allowed_types']        = 'pdf|jpg|png|jpeg';
        $config['file_name']            = 'bukti'.date('ymd').'-'.substr(md5(rand()),0,10);
        $post = $this->input->post(null,TRUE);
		$this->load->library('upload', $config);

		if($this->upload->do_upload('bukti')){
			$post['bukti']  = $this->upload->data('file_name');
			$this->Pengajuan_m->addBukti($post);
// 			if($this->db->affected_rows() >0){
// 				echo "<script>alert('data berhasil disimpan');
// 				window.location='".site_url('Pengajuan')."'
// 				</script>";
// 			}
		}else{
// 			$error = $this->upload->display_errors();
// 			echo "<script>alert($error);
// 			window.location='".site_url('Pengajuan')."'
// 			</script>";
		}
	}
	function cekNik(){
		$nik 	=$this->input->post('nik');
		if(strlen($nik) > 16){
		    $nik = $this->encryption->decrypt($nik);
		}
		$data 	= $this->Pengajuan_m->getNIk($this->session->idperiod);
		$i = 0;
		$array = -1;
		foreach ($data->result() as $key => $value){
		    if($this->encryption->decrypt($value->nik) == $nik){
		        $array = $i;
		    }
		  $i++;
		}
		if($array != -1){
		    	echo json_encode(array(
		       	        "status" => true,
		       	        "data" => $data->result()[$array])
		       	    );
		       	exit();
		}else{
		    	echo json_encode(array(
		       	        "status" => false,
		       	      //  "data" => $data->result()[$i])
		       	    ));
		       	exit();
		}
	}
	function getEdit(){
		check_user();
		$id = $this->input->post('id');
		$data = $this->Pengajuan_m->getEdit($id);
		$en = $this->encryption->decrypt($data->row()->nik);
		$data = $data->result();
		$data['nik_e'] = $en;
		echo json_encode($data);
	}
	function delPengajuan(){
		check_user();
		$del = $this->Pengajuan_m->delPengajuan($this->input->post('id'),$this->input->post('j'));
		if($del){
			echo json_encode(array(
				'status'	=> true,
				'message'	=> 'suksess'
			));
			exit;
		}else{
			echo json_encode(array(
				'status'	=> false,
				'message'	=> 'Failed'
			));
			exit;
		}
	}
	function hapusanggota(){
	    check_user();
		$del = $this->Pengajuan_m->delPengajuan($this->input->post('id'));
		if($del){
			echo json_encode(array(
				'status'	=> true,
				'message'	=> 'suksess'
			));
			exit;
		}else{
			echo json_encode(array(
				'status'	=> false,
				'message'	=> 'Failed'
			));
			exit;
		}
	}
	public function upload()
    {
        check_user();
		$post = $this->input->post(null,TRUE);
        // Load plugin PHPExcel nya
        include APPPATH.'third_party/PHPExcel/PHPExcel.php';

        $config['upload_path'] = realpath('excel');
        $config['allowed_types'] = 'xlsx|xlx|csv';
        $config['max_size'] = '10000';
        $config['encrypt_name'] = true;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload()) {

            //upload gagal
            $this->session->set_flashdata('notif', '<div class="alert alert-danger"><b>PROSES Pengajuan GAGAL!</b> '.$this->upload->display_errors().'</div>');
            //redirect halaman
            redirect('Pengajuan/');

        } else {

            $data_upload = $this->upload->data();

            $excelreader     = new PHPExcel_Reader_Excel2007();
            $loadexcel         = $excelreader->load('excel/'.$data_upload['file_name']); // Load file yang telah diupload ke folder excel
            $sheet             = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);

            $data = array();

            $numrow =1;
                $newformat = date('Y-m').'-01';
            foreach($sheet as $row){
                            if($numrow >= 6){
                            	if ($row['D']!=null) {
                            		 array_push($data, array(
                                    'nama_satker' => $this->session->satker,
                                    'nama_pegawai'      => $row['C'],
                                    'nik'      => $this->encryption->encrypt($row['D']),
                                    'nip' => $row['E'],
                                    'jml_is'      => $row['F'],
                                    'jml_an' => $row['G'],
                                    'penghasilan'      => preg_replace("/[^0-9]/", "", $row['H']),
                                    'tunjangan'      => preg_replace("/[^0-9]/", "", $row['I']),
                                    'thp' => preg_replace("/[^0-9]/", "", $row['J']),
                                    'dpi'      => preg_replace("/[^0-9]/", "", $row['K']),
                                    'iuran_1'      => preg_replace("/[^0-9]/", "", $row['L']),
                                    'iuran_4'      => preg_replace("/[^0-9]/", "", $row['M']),
                                    'total_i' => preg_replace("/[^0-9]/", "", $row['N']),
                                    'bulan'      => $newformat,
                                    'periode_id_periode'      => $this->session->idperiod,
									'jenis'      => 1,
									'bukti'		=> "",
                                ));
                            	}
                               
                    }
                $numrow++;
                
            }
                $upload = $this->db->insert_batch('pengajuan_satker', $data);
                if($upload){
                    $pesan = urlencode("Ada Data Pengajaun dari satker ".$this->session->satker." Silahkan cek https://kc-magelang.com/tidar \n\n");
                    $secret_token =  "5349669683:AAE8jV_jT2FkaTxovyUY_nY8RkXQHRSoyoM";
                    $telegram_id = "-742569295";
	                $url = "https://api.telegram.org/bot" . $secret_token . "/sendMessage?parse_mode=markdown&chat_id=-742569295";
	                $url = $url . "&text=".$pesan;
	                $ch = curl_init();
	                $optArray = array(
	                       CURLOPT_URL => $url,
	                       CURLOPT_RETURNTRANSFER => true
	                );
	               curl_setopt_array($ch, $optArray);
	               $send = curl_exec($ch);
	               curl_close($ch);
					$this->session->set_flashdata('notif', '<div class="alert alert-success"><b>PROSES Pengajuan BERHASIL!</b> Data berhasil diPengajuan!</div>');
				}else{
					$this->session->set_flashdata('notif', '<div class="alert alert-danger"><b>PROSES Pengajuan Gagal!</b> Silahkan Menghubungi Pihak BPJS!</div>');
				}
                
            //delete file from server
            unlink(realpath('excel/'.$data_upload['file_name']));

            //upload success
            
            //redirect halaman
            redirect('Pengajuan');

        }
    }
	function validasiAll(){
		check_admin();
		$failed= array();
			array_push($failed,"");
		$data = explode(",",$_POST["materi"]);
		for ($i = 0; $i < count($data); $i++) {
			$data_p = $this->Pengajuan_m->getbyId($data[$i])->row()->nama_pegawai;
			$valid = $this->Pengajuan_m->validasi($data[$i]);
			if(!$valid){
				array_push($failed,$data_p);
			}
		}
        $pesan = urlencode("Pengajaun Pegawai dari satker ".$this->session->satker." Sudah divalidasi oleh admin \n Silahkan cek https://kc-magelang.com/tidar ");
        $secret_token =  "5349669683:AAE8jV_jT2FkaTxovyUY_nY8RkXQHRSoyoM";
        $telegram_id = "-742569295";
	    $url = "https://api.telegram.org/bot" . $secret_token . "/sendMessage?parse_mode=markdown&chat_id=-742569295";
	    $url = $url . "&text=".$pesan;
	    $ch = curl_init();
	    $optArray = array(
	          CURLOPT_URL => $url,
	          CURLOPT_RETURNTRANSFER => true
	    );
	    curl_setopt_array($ch, $optArray);
	    $send = curl_exec($ch);
	    curl_close($ch);
		echo json_encode($failed);
	}
	function validasiAllAnggota(){
	    check_admin();
		$failed= array();
			array_push($failed,"");
		$data = explode(",",$_POST["materi"]);
		for ($i = 0; $i < count($data); $i++) {
			$valid = $this->Pengajuan_m->validasiAnggota($data[$i]);
		}
		$pesan = urlencode("Pengajaun Tambah Anggota dari satker ".$this->session->satker." Sudah divalidasi oleh admin \n Silahkan cek https://kc-magelang.com/tidar ");
        $secret_token =  "5349669683:AAE8jV_jT2FkaTxovyUY_nY8RkXQHRSoyoM";
        $telegram_id = "-742569295";
	    $url = "https://api.telegram.org/bot" . $secret_token . "/sendMessage?parse_mode=markdown&chat_id=-742569295";
	    $url = $url . "&text=".$pesan;
	    $ch = curl_init();
	    $optArray = array(
	          CURLOPT_URL => $url,
	          CURLOPT_RETURNTRANSFER => true
	    );
	    curl_setopt_array($ch, $optArray);
	    $send = curl_exec($ch);
	    curl_close($ch);
		echo json_encode($failed);
	}
    function tolak(){
        $post = $this->input->post(null,TRUE);
        $saveCatatan = $this->Pengajuan_m->Tolak($post['id_tolak'], $post['text_tolak'],$post['jenis_tolak']);
        if($saveCatatan){
            $pesan = urlencode("Pengajaun dari satker ".$this->session->satker." Sudah divalidasi oleh admin \n Silahkan cek https://kc-magelang.com/tidar ");
            $secret_token =  "5349669683:AAE8jV_jT2FkaTxovyUY_nY8RkXQHRSoyoM";
            $telegram_id = "-742569295";
	        $url = "https://api.telegram.org/bot" . $secret_token . "/sendMessage?parse_mode=markdown&chat_id=-742569295";
	        $url = $url . "&text=".$pesan;
	        $ch = curl_init();
	        $optArray = array(
	              CURLOPT_URL => $url,
	              CURLOPT_RETURNTRANSFER => true
	        );
	        curl_setopt_array($ch, $optArray);
	        $send = curl_exec($ch);
	        curl_close($ch);
            echo "<script>alert('data berhasil disimpan');
            window.location='".site_url('Pengajuan')."'
            </script>";
        }else{
            echo "<script>alert('data gagal disimpan');
            window.location='".site_url('Pengajuan')."'
            </script>";
        }
    }
	function getTolak(){
		$data = $this->Pengajuan_m->getTolak($this->input->post('id'),$this->input->post('j'))->result();
		echo json_encode($data);
	}
}
