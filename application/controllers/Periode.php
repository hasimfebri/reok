<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Periode extends CI_Controller {
	function __construct(){
		parent::__construct();
		check_not_login();
		$this->load->model('basic_m');
	}
	public function change($id)
	{
		$params = array('tahun','idperiod');
		$this->session->unset_userdata($params);
		$q = $this->basic_m->getById("periode","id_periode",$id)->row();
		$qq = $this->basic_m->getSatkerByPeriode($q->satker_username)->row();
		$params = array(
						'tahun' => $q->tahun,
						'idperiod' => $id,
						'satker'    => $qq->nama,
					);
		$this->session->set_userdata($params);
		redirect('dashboard');
	}
}
