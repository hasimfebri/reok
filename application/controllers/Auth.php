<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	public function index()
	{
	    date_default_timezone_set("Asia/Jakarta");
		$tglcek = array('1','2','3','4','5','7','12');
		$daycek = array('saturday','sunday') ;
		$tgl = date('d');
		$tahun = date('Y');
		$day = strtolower(date('l'));
		if (in_array($tgl, $tglcek) && !in_array($day, $daycek)){
		    $this->load->model('basic_m');
			$tanggal = date('Y-m').'-01';
			if(date('m') == '01'){
			   $cekAutoPeriode = $this->basic_m->cekAutoPeriode(date('Y')); 
			}
			$cekAuto = $this->basic_m->cekAuto($tanggal,$tahun);
		}
		check_already_login();
		  $this->load->library('botdetect/BotDetectCaptcha', array(
            'captchaConfig' => 'ExampleCaptcha'
        ));

        // make Captcha Html accessible to View code
        $data['captchaHtml'] = $this->botdetectcaptcha->Html();
		$this->load->view('login',$data);
	}
	public function process()
	{
		 $this->load->library('botdetect/BotDetectCaptcha', array(
            'captchaConfig' => 'ExampleCaptcha'
        ));
		$post = $this->input->post(null,TRUE);
		if ($_POST) {
            // validate the user-entered Captcha code when the form is submitted
            $code = $this->input->post('CaptchaCode');
            $isHuman = $this->botdetectcaptcha->Validate($code);

            if (!$isHuman) {
            	echo "<script>
						alert('CAPTCHA validation failed, please try again.');
						window.location='".site_url('auth')."'
					</script>";
                //$data['captchaValidationMessage'] = 'CAPTCHA validation failed, please try again.';
            } else {
            	if(isset($post['login'])){
					if($post['level']== '1'){
						$userpass = $post['password'];
						$this->load->model('users_m');
						$query = $this->users_m->login($post);
						if ($query->num_rows()>0) {
							$row = $query->row();
							$pas = $row->password;
							if (password_verify($userpass, $pas)){
							$params = array(
								'username' => $row->username,
								'level' => $row->level,
								'cabang' => $row->cabang,
							);
							$this->session->set_userdata($params);
							echo "
							<script>
								alert('Selamat Login Berhasil!');
								window.location='".site_url('dashboard')."'
							</script>";
						}else{
							echo "<script>
								alert('Username/Password Salah');
								window.location='".site_url('auth')."'
							</script>";
						}
						}else{
							echo "<script>
								alert('Username/Password Salah');
								window.location='".site_url('auth')."'
							</script>";
						}
					}else{
						$userpass = $post['password'];
						$this->load->model('users_m');
						$query = $this->users_m->loginuser($post);
						if ($query->num_rows()>0) {
							$row = $query->row();
							$pas = $row->password;
							if (password_verify($userpass, $pas)){
								if (date("Y") != $row->tahun) {
									$this->users_m->addperiod($row->username);
									$query = $this->users_m->loginuser($post);
									$row = $query->row();
								}
							$params = array(
								'username' => $row->username,
								'tahun' => $row->tahun,
								'idperiod' => $row->id_periode,
								'level' => "2",
								'satker' => $row->nama
							);
							$this->session->set_userdata($params);
							echo "<script>
								alert('Selamat Login Berhasil!');
								window.location='".site_url('dashboard')."'
							</script>";
						}else{
							echo "<script>
								alert('Username/Password Salah');
								window.location='".site_url('auth')."'
							</script>";
						}
						}else{
							echo "<script>
								alert('Username/Password Salah');
								window.location='".site_url('auth')."'
							</script>";
						}
					}
				}
            }
        }
		
	}
	public function logout()
	{
		$params = array('username','level','tahun','idperiod','satker','usersatker');
		$this->session->unset_userdata($params);
		redirect('auth');
	}
}
